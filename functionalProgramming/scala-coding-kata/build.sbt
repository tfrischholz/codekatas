val akkaVersion = "2.5.12"
val akkaHttpVersion = "10.1.1"
val alpAkkaVersion = "0.18"

lazy val scalaCodingKata = (project in file(".")).settings(
  name := "scala-coding-kata",
  version := "2.0.0",
  scalaVersion := "2.12.5",
  resolvers += "scala compiler" at "https://mvnrepository.com/artifact/org.scala-lang/scala-compiler",
  resolvers += "Typesafe Repository" at "http://repo.typesafe.com/typesafe/releases/",
  aggregate in update := false,
  libraryDependencies ++= Seq(
    "com.typesafe.akka" %% "akka-actor" % akkaVersion,
    "com.typesafe.akka" %% "akka-http" % akkaHttpVersion,
    "com.typesafe.akka" %% "akka-stream" % akkaVersion,
    "com.typesafe.akka" %% "akka-remote" % akkaVersion,
    "org.scala-lang" % "scala-reflect" % scalaVersion.value,
    "org.scalatest" %% "scalatest" % "3.0.4" % "test")
)


lazy val `predictive-lotto` = (project in file("./lotto")).settings(
  version := "0.0.1",
  scalaVersion := "2.12.5",
  resolvers += "scala compiler" at "https://mvnrepository.com/artifact/org.scala-lang/scala-compiler",
  resolvers += "Typesafe Repository" at "http://repo.typesafe.com/typesafe/releases/",
  aggregate in update := false,
  libraryDependencies ++= Seq(
    "com.typesafe.akka" %% "akka-actor" % akkaVersion,
    "com.typesafe.akka" %% "akka-http" % akkaHttpVersion,
    "com.typesafe.akka" %% "akka-stream" % akkaVersion,
    "com.lightbend.akka" %% "akka-stream-alpakka-csv" % alpAkkaVersion,
    "com.typesafe.akka" %% "akka-remote" % akkaVersion,
    "org.scala-lang" % "scala-reflect" % scalaVersion.value,
    "org.scalatest" %% "scalatest" % "3.0.4" % "test")
)

// Fix for: https://github.com/scala/bug/issues/2712
scalacOptions += "-Ypartial-unification"

fork in run := true
