package model

import scala.collection.immutable.Map

/**
  * @author Freshwood
  * @since 27.06.2018
  * The lotto ziehung
  */
case class Lotto(first: Int,
                 second: Int,
                 third: Int,
                 fourth: Int,
                 fifth: Int,
                 sixth: Int,
                 superNumber: Int)

object Lotto {

  def apply(map: Map[String, String]): Lotto =
    Lotto(map("Zahl1").toInt,
          map("Zahl2").toInt,
          map("Zahl3").toInt,
          map("Zahl4").toInt,
          map("Zahl5").toInt,
          map("Zahl6").toInt,
          map("Super").toInt)

}
