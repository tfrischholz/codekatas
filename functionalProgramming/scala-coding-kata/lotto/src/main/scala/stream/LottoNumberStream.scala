package stream

import java.nio.file.Paths

import akka.NotUsed
import akka.actor.ActorSystem
import akka.stream.alpakka.csv.scaladsl.{CsvParsing, CsvToMap}
import akka.stream.scaladsl.{FileIO, Flow, Sink, Source}
import akka.stream.{IOResult, Materializer}
import akka.util.ByteString
import model.Lotto

import scala.collection.immutable.Map
import scala.concurrent.{ExecutionContext, Future}
import scala.language.postfixOps
import scala.util.Random

/**
  * @author Freshwood
  * @since 27.06.2018
  * The lotto number stream business logic
  */
trait LottoNumberStream {
  implicit def system: ActorSystem
  implicit def materializer: Materializer
  implicit def ec: ExecutionContext
}

trait LottoNumberSource extends LottoNumberStream {
  lazy val source: Source[ByteString, Future[IOResult]] = FileIO
    .fromPath(Paths.get(getClass.getClassLoader.getResource("lotto.csv").toURI))
}

trait LottoNumberFlow extends LottoNumberStream {
  private lazy val lottoFlow: Flow[Map[String, String], Lotto, NotUsed] =
    Flow[Map[String, String]].map(Lotto.apply)

  lazy val flow: Flow[ByteString, Lotto, NotUsed] =
    Flow[ByteString].via(CsvParsing.lineScanner('\t')).via(CsvToMap.toMapAsStrings()).via(lottoFlow)
}

trait LottoNumberSink extends LottoNumberStream {
  lazy val sink: Sink[Lotto, Future[Seq[Lotto]]] = Sink.fold(Seq.empty[Lotto])(_ :+ _)
}

trait LottoNumberService extends LottoNumberSource with LottoNumberFlow with LottoNumberSink {
  lazy val lottoNumbers: Future[Seq[Lotto]] = source.via(flow).runWith(sink)

  def printSuperNumbers(): Unit = lottoNumbers foreach { lottoList =>
    val output: Seq[String] = (0 to 9) map (number =>
      s"Super$number: " + lottoList.count(_.superNumber == number))
    output.foreach(println)
  }

  def bestNumbers: Future[Seq[Int]] = sortedLottoNumbers map (_.take(6))

  def optionalNumbers: Future[Seq[Int]] = sortedLottoNumbers map (_.take(12).takeRight(6))

  def randomLottoNumbers: Future[Set[Int]] = bestNumbers flatMap { best =>
    optionalNumbers map { optionals =>
      val goodNumbers = best ++ optionals

      def randomNumbers(numbers: Set[Int] = Set.empty): Set[Int] = numbers match {
        case x if x.isEmpty   => randomNumbers(Set.empty + Random.nextInt(11) + 1)
        case x if x.size < 6  => randomNumbers(x + Random.nextInt(11) + 1)
        case x if x.size == 6 => x
      }

      randomNumbers() map (randomNumber => goodNumbers(randomNumber))
    }
  }

  private def sortedLottoNumbers: Future[Seq[Int]] = lottoNumbers map { lottoList =>
    val allLottoNumbers = lottoList.flatMap { lotto =>
      Seq(lotto.first, lotto.second, lotto.third, lotto.fourth, lotto.fifth, lotto.sixth)
    }

    val result = (1 to 49) map { number =>
      number -> allLottoNumbers.count(_ == number)
    } toMap

    result.toList.sortWith((f, g) => f._2 > g._2) map (_._1)
  }
}
