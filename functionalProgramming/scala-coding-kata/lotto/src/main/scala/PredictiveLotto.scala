import stream.LottoNumberService
import support.AkkaSupport

import scala.language.postfixOps

/**
  * @author Freshwood
  * @since 26.06.2018
  * A dead simple lotto prediction app coded with akka streams
  */
// Lotto number source from: https://www.gutefrage.net/frage/link-zur-lottodatenbank-6-aus-49-als-txt-rtf
object PredictiveLotto extends App with LottoNumberService with AkkaSupport {

  printSuperNumbers()

  bestNumbers foreach (numbers => println(s"Best Numbers: $numbers"))

  optionalNumbers foreach (numbers => println(s"Optional Numbers: $numbers"))

  randomLottoNumbers foreach (numbers => println(s"Best Randoms1: $numbers"))

  randomLottoNumbers foreach (numbers => println(s"Best Randoms2: $numbers"))
}
