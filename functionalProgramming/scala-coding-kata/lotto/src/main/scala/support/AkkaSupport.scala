package support

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer

import scala.concurrent.ExecutionContextExecutor

/**
  * @author Freshwood
  * @since 27.06.2018
  * The akka relevant implementation to run a materialized stream later on
  */
trait AkkaSupport {
  implicit lazy val system: ActorSystem = ActorSystem("predictive-lotto")

  implicit lazy val materializer: ActorMaterializer = ActorMaterializer()

  implicit lazy val ec: ExecutionContextExecutor = system.dispatcher
}
