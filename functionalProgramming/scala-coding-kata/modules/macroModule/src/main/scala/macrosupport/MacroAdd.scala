package macrosupport

import scala.language.experimental.macros
import scala.reflect.macros.blackbox


/**
	* Created by Freshwood on 24.07.2016.
	*/
object MacroAdd {

	/**Declares a macro definition 'add()'
		*
		* @param num1 first operand
		* @param num2 second operand
		* @return result of addition
		*/
	def add(num1: Int, num2: Int): Int = macro add_impl

	/**Macro implementation of 'add()' method
		*
		* @param c context
		* @param num1 AST of first operand
		* @param num2 AST of second operand
		* @return AST of the resultant of the addition
		*/
	def add_impl(c: blackbox.Context)(num1: c.Expr[Int], num2: c.Expr[Int]): c.Expr[Int] = {
		import c.universe.reify
		reify {
			num1.splice + num2.splice
		}
	}
}