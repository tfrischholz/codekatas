package macrosupport

import scala.language.experimental.macros
import scala.reflect.macros.blackbox


/**
	* Created by Freshwood on 25.09.2016.
	*/
object MacroHelloWorld {

	def sayHello(): Unit = macro macroHelloWorldImpl


	def macroHelloWorldImpl(c: blackbox.Context)(): c.Expr[Unit] = {

		import c.universe._

		println("Compiling macro... " + this.getClass)

		reify {
			println("Hello World")
		}
	}
}
