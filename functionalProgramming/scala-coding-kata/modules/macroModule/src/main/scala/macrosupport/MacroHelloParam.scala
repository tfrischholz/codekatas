package macrosupport

import scala.language.experimental.macros
import scala.reflect.macros.blackbox

/**
	* Created by Freshwood on 25.09.2016.
	*/
object MacroHelloParam {

	def myPrintln(param: Any): Unit = macro macro_myPrintln

	def macro_myPrintln(c: blackbox.Context)(param: c.Expr[Any]): c.Expr[Unit] = {

		import c.universe._

		reify {
			println("At compile time!!!" + param.splice)
		}
	}

}
