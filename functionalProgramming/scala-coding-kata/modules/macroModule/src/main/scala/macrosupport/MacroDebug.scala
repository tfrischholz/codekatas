package macrosupport

import scala.language.experimental.macros
import scala.reflect.macros.blackbox

/**
	* Created by Freshwood on 25.09.2016.
	*/
object MacroDebug {

	def debug(param: Any): Unit = macro debug_macro_imple

	def debug_macro_imple(c: blackbox.Context)(param: c.Expr[Any]): c.Expr[Unit] = {

		import c.universe._

		val tree = show(param.tree)
		val paramRepTree = Literal(Constant(tree))
		val paramRepTreeExpression = c.Expr[String](paramRepTree)

		reify { println(paramRepTreeExpression.splice + " = " + param.splice)}
	}
}
