package macrosupport

import scala.language.experimental.macros
import scala.reflect.macros.blackbox

/**
	* Created by Freshwood on 25.09.2016.
	*/
object MacroDebugEx {

	def debugEx(param: Any): Unit = macro debugEx_macro_impl

	def debugEx_macro_impl(c: blackbox.Context)(param: c.Expr[Any]): c.Expr[Unit] = {

		import c.universe._

		val paramRepresentation = show(param.tree)

		println(paramRepresentation)

		c.Expr[Unit](q"""println($paramRepresentation + " = " + $param)""")
	}

}
