package macrosupport

import scala.language.experimental.macros
import scala.reflect.macros.blackbox


/**
	* Created by Freshwood on 24.07.2016.
	*/
object ClassCheckMacro {

	def ensureClass[T](x: T): T = macro impl

	def impl(c: blackbox.Context)(x: c.Tree) = {
		import c.universe._
		x match {
			case Literal(ClassSymbolTag) => x
			case _ => c.abort(c.enclosingPosition, "not a class type to instantiate")
		}
	}

	def checkClass[T]: T = macro checkClassImpl[T]

	def checkClassImpl[T: c.WeakTypeTag](c: blackbox.Context) = {
		import c.universe._
		val symbol = weakTypeOf[T].typeSymbol
		if (symbol.isAbstract) {
			c.abort(c.enclosingPosition, s"${symbol.fullName} must be a class")
		} else {
			c.Expr(q"()")
		}
	}
}
