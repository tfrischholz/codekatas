package tdd

import org.scalatest.{FlatSpec, Matchers}
import testing.TestDrivenStack

/**
  * @author Freshwood
  * @since 29.07.2018
  * The test driven approach for the [[testing.TestDrivenStack]]
  */
class TestDrivenStackTest extends FlatSpec with Matchers {

  trait EmptyStack {
    val stack: TestDrivenStack[Int] = TestDrivenStack()
  }

  trait FullStack {
    val stack: TestDrivenStack[Int] = TestDrivenStack(1 to 100)
  }

  "A stack" should "be created" in new EmptyStack {
    stack.elements shouldBe empty
  }

  it should "be created with an element list" in new FullStack {
    stack.elements shouldNot be(empty)
  }

  it should "pop an element" in new FullStack {
    val element: Int = stack.pop

    element shouldBe 1

    stack.elements.length should be(99)

    val anotherElement: Int = stack.pop

    anotherElement shouldBe 2

    stack.elements.length should be(98)
  }

  it should "push an element" in new FullStack {
    val pushedStack: TestDrivenStack[Int] = stack.push(1000)

    pushedStack.elements.length shouldBe 101

    val poppedElement: Int = pushedStack.pop

    poppedElement shouldBe 1000
  }

  it should "peek an element without mutate the object" in new FullStack {
    val peek: Int = stack.peek

    peek shouldBe 1

    stack.elements.length shouldBe 100
  }

  it should "throw an exception when the stack is empty on pop" in new EmptyStack {
    intercept[NoSuchElementException](stack.pop)
  }
}
