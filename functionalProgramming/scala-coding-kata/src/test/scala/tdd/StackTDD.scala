package tdd

import org.scalatest.{FlatSpec, Matchers}
import testing.TddStack

import scala.language.postfixOps

class StackTDD extends FlatSpec with Matchers with SharedStackTest {

  val emptyStack: TddStack[Int] = TddStack[Int](Seq.empty)

  val stackWithElements: TddStack[Int] = TddStack[Int](Seq(1, 2, 3))

  /**
    * We can also create a context where we want to run the tests
    */
  trait NormalStack {
    val normalStack: TddStack[Int] = TddStack[Int](1 to 100)
  }

  trait EmptyStack {
    val emptyStack: TddStack[Int] = TddStack[Int](Seq.empty)
  }


  "My own immutable stack" should "instantiate a new instance with the correct type" in {
    type Test = Int
    val stack: TddStack[Test] = TddStack(Seq(1, 2, 3))
    stack shouldBe a[TddStack[Test]]
  }

  it should "push new elements" in {
    val elements: Seq[Int] = 1 to 100

    val stack = TddStack(elements)

    val testStack = stack push 100

    testStack.all.length shouldBe 101
  }

  it should "pop old elements" in {
    val stack = TddStack(Seq(1, 2, 3))

    val poppedStack = stack.pop

    poppedStack.all.length shouldBe 2

    poppedStack.all.last shouldBe 3
  }

  it should "throw a NoSuchElementException when stack is empty and user want to pop" in {
    val stack = TddStack(Seq.empty)

    a[UnsupportedOperationException] should be thrownBy stack.pop
  }

  it should "return all current elements in the stack" in {
    val actual: Seq[Int] = 1 to 100

    val expected = TddStack(actual).all

    actual shouldBe expected
  }

  it should "correct add elements with the same super type" in {
    type ActualType = Int
    type ExpectedType = AnyVal

    val stack: TddStack[ActualType] = TddStack[ActualType](Seq(1, 2, 3))

    val pushedStack: TddStack[ExpectedType] = stack.push[ExpectedType](100)

    pushedStack.all.size shouldBe 4

    pushedStack.all.head shouldBe a[ExpectedType]
  }

  it should behave like withEmptyStack(emptyStack)

  it should behave like withStack(stackWithElements)

  it should "also work within an stack context trait" in new NormalStack {
    val result: TddStack[Int] = normalStack.pop
    result shouldBe a[TddStack[Int]]
  }

  it should "also work within an stack context trait with empty stack" in new EmptyStack {
    val result: TddStack[Int] = emptyStack push 100
    result shouldBe a[TddStack[Int]]
  }
}
