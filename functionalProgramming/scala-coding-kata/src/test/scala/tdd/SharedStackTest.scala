package tdd

import org.scalatest.FlatSpec
import testing.TddStack

trait SharedStackTest {
  this: FlatSpec =>

  def withEmptyStack(newStack: => TddStack[Int]): Unit = {
    it should "push a new element" in {
      newStack push 100
    }
  }

  def withStack(newStack: => TddStack[Int]): Unit = {
    it should "push and pop a new element" in {
      val result = newStack.pop
      assert(result.all.nonEmpty)
    }
  }
}
