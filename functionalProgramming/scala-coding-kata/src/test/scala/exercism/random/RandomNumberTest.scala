package exercism.random

import org.scalatest.{FlatSpec, Matchers}

/**
  * @author Freshwood
  * @since 01.01.2018
  * The tests for the number generator
  */
class RandomNumberTest extends FlatSpec with Matchers {

  val randomizer: Randomizer.RandomNumbers =
    Randomizer.RandomNumbers().generate(100).generate(100).generate(100)
  val tester: List[Int] => Boolean = numbers =>
    numbers.forall(number => numbers.count(_ == number) == 1)

  "The own number generator" should "generate random numbers" in {
    randomizer.last < 100 shouldBe true
  }

  it should "not contain duplicate numbers" in {
    val numbers = randomizer.numbers

    tester(numbers) shouldBe true
    tester(numbers :+ 1 :+ 1) shouldBe false
  }

  it should "not contain a already created random number" in {
    val randoms =
      (1 to 100).foldLeft(Randomizer.RandomNumbers())((x, _) => x.generate(100))

    randoms.numbers.length shouldBe 100
    tester(randoms.numbers) shouldBe true
  }

  it should "abort when the seed is smaller then the actual number size" in {
    intercept[IllegalArgumentException] {
      val randoms =
        (1 to 100).foldLeft(Randomizer.RandomNumbers())(
          (x, _) => x.generate(10))

      randoms.numbers.length shouldBe 10
      tester(randoms.numbers) shouldBe true
    }
  }

}
