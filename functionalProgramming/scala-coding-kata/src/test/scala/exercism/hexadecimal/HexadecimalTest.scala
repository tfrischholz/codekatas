package exercism.hexadecimal

import exercism.hexadecimal.HexadecimalApp.Hexadecimal
import org.scalatest.{FlatSpec, Matchers}

/**
  * Created by Freshwood on 19.06.2016.
  */
class HexadecimalTest extends FlatSpec with Matchers {

  private val hex = new Hexadecimal()


  it should "handle empty" in {
    hex.hexToInt("") should equal(0)
  }

  it should "handle zeros" in {
    hex.hexToInt("00000000") should equal(0)
  }

  it should "handle single digit" in {
    hex.hexToInt("1") should equal(1)
  }

  it should "handle single hex digit" in {
    hex.hexToInt("c") should equal(12)
  }

  it should "handle upper case" in {
    hex.hexToInt("C") should equal(12)
  }

  it should "handle multiple digits" in {
    hex.hexToInt("10") should equal(16)
  }

  it should "handle multiple hex digits" in {
    hex.hexToInt("AF") should equal(175)
  }

  it should "handle complex strings" in {
    hex.hexToInt("19ace") should equal(105166)
    hex.hexToInt("ffffff") should equal(16777215)
    hex.hexToInt("ffff00") should equal(16776960)
  }

  it should "handle invalid strings" in {
    hex.hexToInt("carrot") should equal(0)
    hex.hexToInt("abczcba") should equal(0)
  }
}
