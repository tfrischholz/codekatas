package skill.checkout.naive

import org.scalatest.{FlatSpec, Matchers}
import skill.checkout.naive.CheckoutSystem._

/**
  * @author Freshwood
  * @since 04.07.2019
  */
class CheckoutSystemTest extends FlatSpec with Matchers {

  sealed trait BasicShoppingItems {
    val one = ShoppingItem(1.20)
    val two = ShoppingItem(2.20)
  }

  sealed trait PositiveShoppingItems extends BasicShoppingItems {
    val three = ShoppingItem(4.333333333333333333333333333, 3)
  }

  sealed trait NegativeShoppingItems extends BasicShoppingItems {
    val three = ShoppingItem(-4.333333333333333333333333333, 3)
  }

  "The checkout system" should "correctly handle shopping items" in new PositiveShoppingItems {

    one.total shouldBe 1.20
    two.total shouldBe 2.20
    three.total shouldBe 13.00
  }

  it should "correctly handle orders" in new PositiveShoppingItems {

    val order = Order(Set(one, two, three))

    order.items.size shouldBe 3
    order.total shouldBe 16.40
  }

  it should "correctly handle negative orders" in new NegativeShoppingItems {

    val order = Order(Set(one, two, three))

    order.items.size shouldBe 3
    order.total shouldBe -9.60
  }

  it should "calculate the checkout price without a voucher" in new PositiveShoppingItems {

    val order = Order(Set(one, two, three))

    val account = UserAccount("Hans", order, NoVoucher)

    account.checkout shouldBe 16.40
  }

  it should "well format the checkout price" in new PositiveShoppingItems {

    val order = Order(Set(one, two, three))

    val account = UserAccount("Hans", order, NoVoucher)

    account.checkoutOutput shouldBe "16,40$"
  }

  it should "respect a voucher in the user account" in new PositiveShoppingItems {

    val order = Order(Set(one, two, three))

    val account = UserAccount("Hans", order, WithVoucher(10))

    account.checkout shouldBe 14.76
  }
}
