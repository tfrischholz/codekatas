package skill.checkout.observer

import org.scalatest.{FlatSpec, Matchers}
import skill.checkout.observer.ObserverShoppingSystem._

/**
  * @author Freshwood
  * @since 04.07.2019
  */
class ObserverShoppingSystemTest extends FlatSpec with Matchers {

  sealed trait BasicShoppingItems {
    val one = ShoppingItem(1.20)
    val two = ShoppingItem(2.20)
  }

  sealed trait PositiveShoppingItems extends BasicShoppingItems {
    val three = ShoppingItem(4.333333333333333333333333333, 3)
    val observable: Observable = Observable(Set(one, two, three))
  }

  sealed trait NegativeShoppingItems extends BasicShoppingItems {
    val three = ShoppingItem(-4.333333333333333333333333333, 3)
    val observable: Observable = Observable(Set(one, two, three))
  }

  "The checkout system" should "correctly handle shopping items" in new PositiveShoppingItems {
    one.total shouldBe 1.20
    two.total shouldBe 2.20
    three.total shouldBe 13.00
  }

  it should "correctly handle orders" in new PositiveShoppingItems {

    observable.observers.size shouldBe 3
    observable.total shouldBe 16.40
  }

  it should "correctly handle negative orders" in new NegativeShoppingItems {

    observable.observers.size shouldBe 3
    observable.total shouldBe -9.60
  }

  it should "calculate the checkout price without a voucher" in new PositiveShoppingItems {

    observable.checkout shouldBe 16.40
  }

  it should "well format the checkout price" in new PositiveShoppingItems {

    observable.checkoutOutput shouldBe "16,40$"
  }

  it should "respect a voucher in the calculation" in new PositiveShoppingItems {
    val obs: Observable = observable.withVoucher(10)

    obs.checkout shouldBe 14.76
  }

  it should "notify all observers" in new PositiveShoppingItems {
    observable.notifyObservers("Hello")
  }

}
