package reactive

import akka.NotUsed
import akka.actor.Status.Success
import akka.actor.{Actor, ActorRef, ActorSystem, Props}
import akka.stream.scaladsl.{Flow, Keep, Sink, Source}
import akka.stream.{ActorMaterializer, OverflowStrategy}
import org.reactivestreams.Publisher

/**
  * Created by Freshwood on 21.02.2017.
  */
object AkkaStreamWithPublisher extends App {

  implicit val system = ActorSystem("My-actor-system")
  implicit val materializer = ActorMaterializer()
  implicit val executionContext = system.dispatcher

  private val normalActor = system.actorOf(Props[NormalActor])

  val source: Source[String, ActorRef] = Source.actorRef[String](16, OverflowStrategy.dropTail)

  val ref: ActorRef = Flow[String].to(Sink.actorRef(normalActor, Success(()))).runWith(source)

  ref ! "Test"

  val test = Source.fromIterator(() => (1 to 100).toIterator) map {
    _.toString
  } to Sink.actorRef[String](ref, Success(())) run()


  println("Success Run")


  val (actorRef: ActorRef, publisher: Publisher[String]) = Source.actorRef[String](1000, OverflowStrategy.fail).toMat(Sink.asPublisher(false))(Keep.both).run()
  actorRef ! "External Service1"
  actorRef ! "External Service2"
  actorRef ! "External Service3"

  val sourceFromPublisher: Source[String, NotUsed] = Source.fromPublisher(publisher)

  val newFlow: NotUsed = sourceFromPublisher.to(Sink.foreach(println)).run()

  Thread.sleep(3000)

  (1 to 100).toList foreach (actorRef ! _)
}

class NormalActor extends Actor {
  override def receive: Receive = {
    case Success(()) => println("Success Received")
    case x => println(s"Received: $x")
  }
}