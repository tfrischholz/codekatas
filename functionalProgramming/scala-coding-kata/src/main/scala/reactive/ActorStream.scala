package reactive

import akka.actor.Status.Success
import akka.actor.{Actor, ActorRef, ActorSystem, PoisonPill, Props}
import akka.stream._
import akka.stream.scaladsl.{Flow, GraphDSL, Keep, RunnableGraph, Sink, Source}

import scala.concurrent.duration._
import scala.language.postfixOps

/**
  * Created by Freshwood on 11.02.2017.
  */
object ActorStream extends App {

  implicit val system = ActorSystem("My-actor-system")
  implicit val materializer = ActorMaterializer()
  implicit val executionContext = system.dispatcher

  private val childActor1 = system.actorOf(Props(classOf[SomeActors.ChildActor1]), "childActor1")
  private val childActor2 = system.actorOf(Props(classOf[SomeActors.ChildActor2]), "childActor2")
  private val streamActor = system.actorOf(Props(classOf[SomeActors.StreamActor]), "streamActor")

  private val actorSource: Source[Nothing, ActorRef] = Source.actorRef(16, OverflowStrategy.dropHead)
  private val actorSink = Sink.asPublisher[String](fanout = false)
  lazy val (actor, publisher) = actorSource.toMat(actorSink)(Keep.both).run()

  system.scheduler.schedule(1 second, 1 seconds, childActor1, "First Message")
  system.scheduler.schedule(1 second, 1 seconds, childActor2, "Second Message")

  val graph = GraphDSL.create() { implicit builder =>

    import GraphDSL.Implicits._

    // Source
    val outletSource: Outlet[String] = builder.add(Source.fromPublisher(publisher)).out

    val flowShape: FlowShape[String, String] = builder.add(Flow[String].map(x => x + "Via Stream Processing"))

    val inletSink: Inlet[Any] = builder.add(Sink.actorRef(streamActor, Success(()))).in

    //outletSource ~> flowShape ~> inletSink
    outletSource ~> flowShape ~> inletSink

    ClosedShape
  }

  val runnable = RunnableGraph.fromGraph(graph)

  runnable.run()
}


object SomeActors {

  class ChildActor1 extends Actor {
    override def receive: Receive = {
      case x => println("ChildActor1: " + x)
        ActorStream.actor ! x
    }
  }

  class ChildActor2 extends Actor {
    override def receive: Receive = {
      case x => println("ChildActor2: " + x)
        ActorStream.actor ! x
    }
  }

  class StreamActor extends Actor {
    var count: Long = 0
    override def receive: Receive = {
      case Success(_) => println("Lines Received: " + count)
        self ! PoisonPill
      case x => count = count + 1
        println(s"${x.toString} count: $count")
    }
  }
}