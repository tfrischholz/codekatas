package reactive

import akka.actor.{Actor, ActorSelection}

/**
  * Created by Freshwood on 03.03.2017.
  * TODO: Update this example ;)
  */
object AkkaRemoteExample extends App {

}


class RemoteActor extends Actor {

  val selection: ActorSelection = context.actorSelection("akka.tcp://actorSystemName@10.0.0.1:2552/user/actorName")

  override def receive: Receive = {
    case x => println(x)
  }
}