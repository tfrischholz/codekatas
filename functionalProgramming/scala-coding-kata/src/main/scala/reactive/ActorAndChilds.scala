package reactive

import akka.actor.{Actor, ActorSystem, PoisonPill, Props, Terminated}
import akka.stream.ActorMaterializer
import akka.pattern.ask
import akka.util.Timeout
import reactive.ActorAndChilds.Tick
import scala.concurrent.duration._

import scala.language.postfixOps

/**
  * Created by Freshwood on 12.02.2017.
  */
object ActorAndChilds extends App {

  case class Tick(value: String)

  implicit val system = ActorSystem("ActorSystem")
  implicit val materializer = ActorMaterializer()
  implicit val executionContext = system.dispatcher
  implicit val timeout: Timeout = 1 second

  lazy val firstActor = system.actorOf(Props[MainActor], "MainActorSchnuffel")
  lazy val secondActor = system.actorOf(Props[ChildActor], "ChildActorSchnuffel")

  val result = firstActor ? Tick("Hallo")

  result map println

  firstActor ! "Hallo"
}


private[reactive] class MainActor extends Actor {

  private val secondActor = context.watch(context.actorOf(Props[ChildActor], "ChildActorSchnuffel"))

  override def receive: Receive = {
    case Tick(x) => sender() ! "I ju hu" + x
    case Terminated(x) => println("Actor terminated" + x)
      context.system.terminate()
    case x: String => println(self.path)
      secondActor ! x
  }
}

private[reactive] class ChildActor extends Actor {
  override def receive: Receive = {
    case x => println(self.path)
      self ! PoisonPill
  }
}