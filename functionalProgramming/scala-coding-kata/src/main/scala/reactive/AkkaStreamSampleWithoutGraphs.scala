package reactive

import akka.NotUsed
import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.{Broadcast, Flow, Sink, Source}

import scala.concurrent.Future
import scala.util.Try


/**
  * Created by Freshwood on 13.02.2017.
  */
object AkkaStreamSampleWithoutGraphs extends App {

  implicit val system = ActorSystem("My-actor-system")
  implicit val materializer = ActorMaterializer()
  implicit val executionContext = system.dispatcher

  val source = Source.fromIterator(() => flightDelayLines)

  val firstFlow = csvToFlightEvent

  val secondFlow = filterAndConvert

  val broadcastOfTwo = Broadcast[FlightDelayRecord](2)

  val thirdFlow = averageCarrierDelay

  val sinkIgnore = Sink.ignore

  val sinkPrint = Sink.foreach(averageSink)

  val stream = source.via(firstFlow).via(secondFlow).via(averageCarrierDelay).alsoTo(sinkPrint).to(sinkIgnore).run()

  def averageSink[A](a: A) {
    a match {
      case (a: String, b: Int, c: Int) => println(s"Delays for carrier $a: ${Try(c / b).getOrElse(0)} average mins, $b delayed flights")
      case x => println("no idea what " + x + "is!")
    }
  }

  lazy val flightDelayLines: Iterator[String] = scala.io.Source.fromFile("src/main/resources/2008.csv", "utf-8").getLines()

  // immutable flow step to split apart our csv into a string array and transform each array into a FlightEvent
  lazy val csvToFlightEvent: Flow[String, FlightEvent, NotUsed] = Flow[String]
    .map(_.split(",").map(_.trim)) // we now have our columns split by ","
    .map(stringArrayToFlightEvent) // we convert an array of columns to a FlightEvent

  // string array to FlightEvent
  def stringArrayToFlightEvent(cols: Array[String]): FlightEvent = FlightEvent(cols(0), cols(1), cols(2), cols(3), cols(4), cols(5), cols(6), cols(7), cols(8), cols(9), cols(10), cols(11), cols(12), cols(13), cols(14), cols(15), cols(16), cols(17), cols(18), cols(19), cols(20), cols(21), cols(22), cols(23), cols(24), cols(25), cols(26), cols(27), cols(28))

  // transform FlightEvents to DelayRecords (only for records with a delay)
  lazy val filterAndConvert: Flow[FlightEvent, FlightDelayRecord, NotUsed] =
    Flow[FlightEvent]
      .filter(r => Try(r.arrDelayMins.toInt).getOrElse(-1) > 0) // convert arrival delays to ints, filter out non delays
      .mapAsyncUnordered(parallelism = 2) { r =>
      Future(FlightDelayRecord(r.year, r.month, r.dayOfMonth, r.flightNum, r.uniqueCarrier, r.arrDelayMins))
    } // output a FlightDelayRecord

  lazy val averageCarrierDelay: Flow[FlightDelayRecord, (String, Int, Int), NotUsed] =
    Flow[FlightDelayRecord]
      .groupBy(30, _.uniqueCarrier)
      .fold(("", 0, 0)) {
        (x: (String, Int, Int), y: FlightDelayRecord) =>
          val count = x._2 + 1
          val totalMins = x._3 + Try(y.arrDelayMins.toInt).getOrElse(0)
          (y.uniqueCarrier, count, totalMins)
      }.mergeSubstreams

}