package reactive

import akka.NotUsed
import akka.actor.{Actor, ActorSystem, Props}
import akka.stream._
import akka.stream.scaladsl.{Broadcast, Flow, GraphDSL, RunnableGraph, Sink, Source}

import scala.language.postfixOps

/**
  * Created by Freshwood on 07.02.2017.
  */
object MinimalStream extends App {

  implicit val system = ActorSystem("Minimal-Stream")

  implicit val materializer = ActorMaterializer()

  private val defaultActorSink = Sink.actorRef(system.actorOf(Props(new MyActor())), "Tick")

  val graph: Graph[ClosedShape.type, NotUsed] = GraphDSL.create() { implicit builder =>

    import GraphDSL.Implicits._

    val outletSource: Outlet[Int] = builder.add(Source(1 to 100)).out

    val flowShape: FlowShape[Int, Int] = builder.add(Flow[Int] map { f =>
      f * f
    })

    val fizzBuzzShape: FlowShape[Int, String] = builder.add(Flow[Int] collect {
      case x if x % 15 == 0 => "FizzBuzz:" + x
    })

    val splitBroadcast = builder.add(Broadcast[Int](2))

    val inletSink: Inlet[Any] = builder.add(Sink.foreach(println)).in

    val actorSink: Inlet[Any] = builder.add(defaultActorSink).in

    outletSource ~> flowShape ~> splitBroadcast
    inletSink <~ fizzBuzzShape <~ splitBroadcast
                    actorSink <~ splitBroadcast

    ClosedShape
  }

  val runnableGraph = RunnableGraph.fromGraph(graph)

  runnableGraph.run()

  //system.terminate()

}

object FizzBuzzStream extends App {

  implicit val system = ActorSystem("FizzBuzzStream-System")

  implicit val materializer = ActorMaterializer()

  val graph = GraphDSL.create() { implicit builder =>

    import GraphDSL.Implicits._

    val outletSource: Outlet[Int] = builder.add(Source(1 to 100)).out

    val fizzBuzzFlowShape: FlowShape[Int, String] = builder.add(Flow[Int] collect {
      case x if x % 15 == 0 => "FizzBuzz"
      case x if x % 5 == 0 => "Buzz"
      case x if x % 3 == 0 => "Fizz"
      case _ @ tt => tt.toString
    })

    val inletSink: SinkShape[Any] = builder.add(Sink.foreach(println))

    outletSource ~> fizzBuzzFlowShape ~> inletSink

    ClosedShape
  }

  val runnerRunnableGraph = RunnableGraph.fromGraph(graph)

  runnerRunnableGraph.run()

  system.terminate()
}

class MyActor extends Actor {
  override def receive: Receive = {
    case x if x == "Tick" => context.system.terminate()
    case x => println("Hey " + x)
  }
}