package blog

import scala.language.{existentials, postfixOps}

/**
  * @author Freshwood
  * @since 23.11.2017
  */
trait SemiGroup[A] {

  /**
    * The semi group associative append (concat) function
    */
  def append(a1: A, a2: A): A
}

trait Monoid[A] extends SemiGroup[A] {

  /**
    * The monoid specific identity function
    */
  def unit: A

  /**
    * It is also possible to add out fold function,
    * cause the monoid has an identity and an concat function
    */
  def fold(xs: List[A]): A = xs.fold(unit)(append)
}

object Monoids {

  implicit object StringMonoid extends Monoid[String] {

    /**
      * The monoid specific identity function
      */
    override def unit: String = ""

    /**
      * The semi group associative append (concat) function
      */
    override def append(a1: String, a2: String): String = a1 + a2
  }

  implicit object IntMonoid extends Monoid[Int] {

    /**
      * The monoid specific identity function
      */
    override def unit: Int = 0

    /**
      * The semi group associative append (concat) function
      */
    override def append(a1: Int, a2: Int): Int = a1 + a2
  }

  // given an monoid for B, I can give you a monoid for functions
  // returning B, by running the functions on the input and adding the
  // results
  implicit def functionMonoid[A, B](
      implicit monoid: Monoid[B]): Monoid[A => B] = new Monoid[A => B] {

    /**
      * The monoid specific identity function
      */
    override def unit: A => B = _ => monoid.unit

    /**
      * The semi group associative append (concat) function
      */
    override def append(a1: A => B, a2: A => B): A => B = { a =>
      monoid.append(a1(a), a2(a))
    }
  }

}
object MonoidBlog extends App {

  import Monoids._

  def sum[A](list: List[A])(implicit monoid: Monoid[A]): A = monoid.fold(list)

  val stringList: List[String] = List("Hallo", "Welt", "Cool")

  val intList: List[Int] = (0 to 100).toList

  val fn: Int => String = _ * 2 toString

  val sumFn: Int => String = sum(List(fn, fn, fn))

  println(sum(stringList))

  println(sum(intList))

  println(sumFn(5)) // 101010
}
