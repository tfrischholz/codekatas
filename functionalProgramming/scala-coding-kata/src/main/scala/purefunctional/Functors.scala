package purefunctional

import scala.language.higherKinds

/**
  * @author Freshwood
  * @since 30.08.2017
  * A functor F[_] is a type constructor of kind * -> *.
  * In the most general case, an F[A] represents a recipe that may halt, run forever, or produce 0 or more A's.
  */
object Functors extends App {

  /**
    * Laws
    *   Identity
    *   map(fa)(identity) == fa
    * Composition
    *   map(map(fa)(ab))(bc) == map(fa)(ab.andThen(bc))
    */
  val intFunctor: FunctorTest[Int] = FunctorTest(100)
  val stringFunctor: FunctorTest[String] = FunctorTest("Hallo")

  val test: FunctorTest[String] =
    stringFunctor.map(intFunctor)(f => stringFunctor.value * f)

  println(test)
}

/**
  * Note: Technically, this is a covariant endofunctor,
  * and there are many other types of functors, including invariant and contravariant.
  * Identity
  *   map(fa)(identity) == fa
  * Composition
  *   map(map(fa)(ab))(bc) == map(fa)(ab.andThen(bc))
  * List is a functor, and List[Int] is a trivial description of a computation producing some number of Int's.
  */
trait Functor[F[_]] {
  def map[A, B](fa: F[A])(ab: A => B): F[B]
}

case class FunctorTest[S](value: S) extends Functor[FunctorTest] {
  override def map[A, B](fa: FunctorTest[A])(ab: (A) => B): FunctorTest[B] =
    FunctorTest(ab(fa.value))
}
