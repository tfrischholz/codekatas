package purefunctional

/**
  * @author Freshwood
  * @since 30.08.2017
  * These types are commonly used to describe optionality and partiality.
  */
object OptionEtherValidation extends App {
  val left: Choices.Either[String, Exception] = -\/("")
  val right: Choices.Either[String, Exception] = \/-(
    new Exception("Could not parse"))

  println(left)
  println(right)
}

/**
  * Option type as Maybe type -> like in Haskell
  */
sealed trait Maybe[A]
final case class Just[A](value: A) extends Maybe[A]
final case class Empty[A]() extends Maybe[A]

sealed trait \/[A, B]
final case class -\/[A, B](value: A) extends \/[A, B]
final case class \/-[A, B](value: B) extends \/[A, B]

package object Choices {
  type Either[A, B] = A \/ B
}

sealed trait Validation[A, B]
final case class Failure[A, B](value: A) extends Validation[A, B]
final case class Success[A, B](value: B) extends Validation[A, B]
