package purefunctional

import scala.util.Try

/**
  * @author Freshwood
  * @since 30.08.2017
  * A type class is a bundle of types and operations defined on them.
  * Most type classes have laws that implementations are required to satisfy.
  */
object TypeClasses extends App {

  /**
    * A type class instance, or simply instance, is an implementation of a type class for a given set of types.
    * Such instances are usually made implicit so the compiler can thread them through functions that require them.
    */
  implicit val ShowReadString: ShowRead[String] = new ShowRead[String] {
    def show(v: String): String = v
    def read(v: String): Either[String, String] = Right(v)
  }

  implicit val ShowReadBoolean: ShowRead[Boolean] = new ShowRead[Boolean] {
    override def show(v: Boolean): String = v.toString

    override def read(v: String): Either[String, Boolean] =
      Try {
        Right(v.toBoolean)
      } getOrElse Left("Could not parse boolean")
  }

  implicit val ShowReadInt: ShowRead[Int] = new ShowRead[Int] {
    override def show(v: Int): String = v.toString

    override def read(v: String): Either[String, Int] =
      Try {
        Right(Integer.parseInt(v))
      } getOrElse Left("Could not parse integer")
  }

  println(ShowRead[String].show("foo")) // foo

  // Convenient syntax, sometimes called extension methods,
  // can be added to types to make it easier to use type classes.
  implicit class ShowOps[A: ShowRead](self: A) {
    def show: String = ShowRead[A].show(self)
  }
  implicit class ReadOps(self: String) {
    def read[A: ShowRead]: Either[String, A] = ShowRead[A].read(self)
  }

  println(true.show.read[Boolean]) // Right(true)

}

/**
  * Example: The ShowRead[A] type class defines a way of "showing" a type A by rendering it to a string,
  * and reading it by parsing it from a string (or producing an error message).
  */
trait ShowRead[A] {
  def show(v: A): String
  def read(v: String): Either[String, A]
}
object ShowRead {
  def apply[A](implicit v: ShowRead[A]): ShowRead[A] = v
}
