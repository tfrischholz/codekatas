package purefunctional

/**
  * @author Freshwood
  * @since 30.08.2017
  * Semigroups allows combining two things of the same type into another thing of the same type.
  * For example, addition forms a semigroup over integers.
  * Monoids add the additional property of having an "zero" element,
  * which you can append to a value without changing the value.
  */
object SemigroupMonoids extends App {

  /**
    * Laws
    * Associativity
        append(a1, append(a2, a3)) == append(append(a1, a2), a3)
      Identity
        append(a, zero) == a
    */
  val stringMonoid: Monoid[String] = new Monoid[String] {
    override def zero: String = ""

    override def append(a1: String, a2: String): String = a1 + a2

    override def associativity(a1: String, a2: String, a3: String): Boolean =
      append(a1, append(a2, a3)) == append(append(a1, a2), a3)

    override def identity(a: String): Boolean = append(a, zero) == a
  }

  // Check rights
  println(stringMonoid.associativity("Hallo", "Welt", "Test"))
  println(stringMonoid.identity("Hallo"))

}

trait Semigroup[A] {
  def append(a1: A, a2: A): A
  def associativity(a1: A, a2: A, a3: A): Boolean =
    append(a1, append(a2, a3)) == append(append(a1, a2), a3)
}
trait Monoid[A] extends Semigroup[A] {
  def zero: A
  def identity(a: A): Boolean = append(a, zero) == a
}
