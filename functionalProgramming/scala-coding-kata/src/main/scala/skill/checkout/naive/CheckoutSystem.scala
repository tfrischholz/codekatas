package skill.checkout.naive

import scala.math.BigDecimal.RoundingMode

/**
  * See Readme in containing root folder
  *
  * @author Freshwood
  * @since 30.06.2019
  */
object CheckoutSystem extends App {

  type Currency = BigDecimal

  sealed trait TotalPrice {
    def total: Currency
  }

  sealed trait Voucher {
    def percent: Int
  }

  case object NoVoucher extends Voucher {
    override def percent: Int = 0
  }

  case class WithVoucher(percent: Int = 1) extends Voucher

  private def roundTo2Digits: Currency => Currency = _.setScale(2, RoundingMode.HALF_UP)

  case class ShoppingItem(price: Currency, quantity: Int = 1) extends TotalPrice {
    override def total: Currency = roundTo2Digits {
      price * quantity
    }
  }

  case class Order(items: Set[ShoppingItem]) extends TotalPrice {
    override def total: Currency = roundTo2Digits {
      items.foldLeft(BigDecimal(0))((x, y) => x + y.total)
    }
  }

  case class UserAccount(name: String, order: Order, voucher: Voucher) {
    def checkout: Currency = roundTo2Digits {
      val total = order.total
      total - (total * voucher.percent / 100)
    }

    def checkoutOutput: String = f"$checkout%1.2f$$"
  }
}
