package skill.checkout.observer

import scala.math.BigDecimal.RoundingMode

/**
  * @author Freshwood
  * @since 04.07.2019
  */
object ObserverShoppingSystem extends App {

  implicit class BigDecimalPrecision(value: BigDecimal) {
    def to2Digits: BigDecimal = value.setScale(2, RoundingMode.HALF_UP)
  }

  trait Observer {
    def price: BigDecimal

    def quantity: Int

    def total: BigDecimal = (price * quantity).to2Digits

    def notification(argument: Object): Unit = println(s"Observer was notified with $argument")
  }

  case class ShoppingItem(price: BigDecimal, quantity: Int = 1) extends Observer

  case class Observable(observers: Set[Observer] = Set.empty, voucher: Int = 0) {

    def add(observer: Observer): Observable =
      Observable(observers + observer)

    def withVoucher(voucher: Int): Observable = Observable(observers, voucher)

    def total: BigDecimal =
      observers.foldLeft(BigDecimal(0))((x, y) => x + y.total).to2Digits

    def checkout: BigDecimal = {
      val price = total
      price - (price * voucher / 100)
    }.to2Digits

    def checkoutOutput: String = f"$checkout%1.2f$$"

    def notifyObservers(argument: Object): Unit = observers.foreach(_.notification(argument))
  }
}
