package skill.checkout.messaging

import akka.actor.{Actor, ActorRef, ActorSystem, Props}
import akka.pattern._
import akka.stream.{ActorMaterializer, Materializer}
import akka.util.Timeout
import skill.checkout.messaging.ShoppingActorSystem.ShoppingActor
import skill.checkout.messaging.ShoppingActorSystem.ShoppingEvents._

import scala.concurrent.ExecutionContext
import scala.concurrent.duration._
import scala.language.postfixOps
import scala.math.BigDecimal.RoundingMode

/**
  * @author Freshwood
  * @since 04.07.2019
  */
object MessagingShoppingSystem extends App {

  implicit val system: ActorSystem = ActorSystem("Test-Actor-System")

  implicit val materializer: Materializer = ActorMaterializer()

  implicit val ec: ExecutionContext = system.dispatcher

  implicit val timeout: Timeout = 10.seconds

  val actor: ActorRef = system.actorOf(Props[ShoppingActor])

  actor ! ShoppingItem(1.20)
  actor ! ShoppingItem(2.20)
  actor ! ShoppingItem(4.333333333333333333333333333, 3)

  val size = actor ? RetrieveShoppingItems
  val checkoutWithoutVoucher = actor ? Checkout
  actor ! SetVoucher(10)
  val checkoutWithVoucher = actor ? Checkout
  val total = actor ? Total
  val checkoutOutput = actor ? CheckoutOutput

  size foreach println // 3
  total foreach println // 16.40
  checkoutWithoutVoucher foreach println // 16.40
  checkoutWithVoucher foreach println // 14.76
  checkoutOutput foreach println // 14.76$

}

object ShoppingActorSystem {

  implicit class BigDecimalPrecision(value: BigDecimal) {
    def to2Digits: BigDecimal = value.setScale(2, RoundingMode.HALF_UP)
  }

  object ShoppingEvents {
    case class ShoppingItem(price: BigDecimal, quantity: Int = 1) {
      def total: BigDecimal = (price * quantity).to2Digits
    }
    case object RetrieveShoppingItems
    case object Total
    case class SetVoucher(amount: Int)
    case object Checkout
    case object CheckoutOutput
  }

  class ShoppingActor extends Actor {
    override def receive: Receive = {
      case x =>
        context.become(receiveWithShoppingItems())
        self ! x
    }

    private def receiveWithShoppingItems(items: Set[ShoppingItem] = Set.empty,
                                         voucher: Int = 0): Receive = {

      case x: ShoppingItem =>
        context.become(receiveWithShoppingItems(items + x, voucher))
      case x: SetVoucher =>
        context.become(receiveWithShoppingItems(items, x.amount))
      case RetrieveShoppingItems =>
        sender ! items.size
      case Total =>
        sender ! items.foldLeft(BigDecimal(0))((x, y) => x + y.total).to2Digits
      case Checkout =>
        val total = items.foldLeft(BigDecimal(0))((x, y) => x + y.total)
        sender ! (total - (total * voucher / 100)).to2Digits
      case CheckoutOutput =>
        val total = items.foldLeft(BigDecimal(0))((x, y) => x + y.total)
        val checkout = total - (total * voucher / 100)
        sender ! f"$checkout%1.2f$$"
    }
  }
}
