  ## The checkout kata has the following behaviors:
  * There is an user account which can buy Items
  * The user has the opportunity to set the quantity of an chopping item
  * The Shopping Items should be calculated in the respective order
  * When the user has all items in his order he can checkout
  * Make sure all order items are recognized by the system
  * Output should be correctly rounded to two digits e.g. 23.99$
  * Optional: Introduce a voucher for the whole basket
