package playground

import scala.language.postfixOps

/**
  * Created by Freshwood on 15.11.2016.
  */
object ScalaHierarchy extends App {

  // First generate out hierarchy classes

  class Entity(val id: Int)

  case class Customer(override val id: Int, name: String, number: Long, plants: Seq[Plant]) extends Entity(id)

  case class Plant(override val id: Int, location: String, areas: Seq[Area]) extends Entity(id)

  case class Area(override val id: Int, codeword: String, deviceGroups: Seq[DeviceGroup]) extends Entity(id)

  case class DeviceGroup(override val id: Int, networkAddress: String) extends Entity(id)

  case class TargetModel(customerCount: Int, plantCount: Int, areaCount: Int, dgCount: Int)

  val deviceGroups = Seq(DeviceGroup(1, "Some String"), DeviceGroup(2, "String2"), DeviceGroup(3, "String3"))

  val areas = Seq(Area(1, "Code1", deviceGroups), Area(2, "Code2", deviceGroups), Area(3, "Code3", deviceGroups))

  val plants = Seq(Plant(1, "Weiden1", areas), Plant(2, "Weiden2", areas))

  val customers = Seq(Customer(1, "Tobi", 12233, plants))

  // Now we can flat everything
  val flatPlants = customers flatten (_.plants)

  val flatAreas = flatPlants flatten (_.areas)

  val flatDeviceGroups = flatAreas flatten (_.deviceGroups)

  val overall: Seq[Entity] = customers ++ flatPlants ++ flatAreas ++ flatDeviceGroups

  val overallIds: Seq[Int] = overall map (_.id)

  println(overallIds)
}
