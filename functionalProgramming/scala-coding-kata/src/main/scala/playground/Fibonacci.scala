package playground

/**
  * @author Freshwood
  * @since 30.12.2017
  */
object Fibonacci extends App {

  private def fib: Int => Int = {
    case 0 => 0
    case 1 => 1
    case n => fib(n - 1) + fib(n - 2)
  }

  println(fib(10))

}
