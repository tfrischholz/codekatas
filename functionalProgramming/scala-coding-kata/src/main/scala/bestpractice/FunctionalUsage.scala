package bestpractice

import scala.collection.immutable
import scala.language.{higherKinds, postfixOps}

/**
  * @author Freshwood
  * @since 04.12.2017
  */
object FunctionalUsage extends App {

  val intToInt: Int => Int = input => Calculator.square(input)

  val intToSomeClass: Int => SomeClass = result => SomeClass(result)

  val combined: Int => SomeClass = intToInt andThen intToSomeClass

  val list: immutable.Seq[Int] = (1 to 100) toList

  val result = list map combined

  println(result)

}

object Calculator {
  def square: Int => Int = input => input * input
}

case class SomeClass(input: Int) extends AnyVal

trait Functor[F[_]] {
  def fmap[A, B](fa: F[A])(f: A => B): F[B]
}

trait Monad[M[_]] extends Functor[M] {
  def pure[A](ma: A): M[A]

  def bind[A, B](a: M[A])(fn: A => M[B]): M[B]

  override def fmap[A, B](fa: M[A])(fn: A => B): M[B] =
    bind(fa)(f => pure(fn(f)))
}

case class Car[+S](value: S) extends Monad[Car] {
  override def pure[A](ma: A): Car[A] = Car(ma)

  override def bind[A, B](a: Car[A])(fn: A => Car[B]): Car[B] = fn(a.value)

  def flatMap[B](f: S => Car[B]): Car[B] = bind(this)(f)

  def map[B](f: S => B): Car[B] = Car(f(value))
}

sealed trait Maybe[+M] extends Monad[Maybe] {

  override def pure[A](ma: A): Maybe[A] = Just(ma)

  override def bind[A, B](a: Maybe[A])(fn: A => Maybe[B]): Maybe[B] =
    if (a.isDefined) fn(a.get) else Empty

  def map[B](f: M => B): Maybe[B] = if (isDefined) Just(f(get)) else Empty

  def flatMap[B](f: M => Maybe[B]): Maybe[B] = bind(this)(f)

  def get: M

  def isDefined: Boolean
}

case class Just[+S](value: S) extends Maybe[S] {
  override def get: S = value

  override def isDefined: Boolean = true
}

case object Empty extends Maybe[Nothing] {
  override def isDefined: Boolean = false

  override def get: Nothing = throw new NoSuchElementException("Empty.get")
}

object FunctorAndMonadTest extends App {

  /**
    * Test flatmap and map with Cars
    */
  val car1: Car[Int] = Car(1)
  val car2: Car[Byte] = Car(100)
  val car3: Car[Short] = Car(3)

  val result: Car[Int] = for {
    a <- car1
    b <- car2
    c <- car3
  } yield a + b + c

  println(result)

  /**
    * Test flatmap and map with a real monad type Maybe
    */
  val aValue: Maybe[Int] = Just(100)
  val testValue: Maybe[Int] = Just(3)
  val bValue: Maybe[Int] = Just(5)

  val successTest: Maybe[Int] = for {
    one <- aValue
    two <- testValue
    three <- bValue
  } yield one + two + three

  // A successful result, cause we have some good values here
  println(successTest)

  val aValue2: Maybe[Int] = Just(100)
  val empty: Maybe[Int] = Empty
  val bValue2: Maybe[Int] = Just(5)

  val errorTest: Maybe[Int] = for {
    one <- aValue2
    two <- empty
    three <- bValue2
  } yield one + two + three

  // Here you can see the routine is not further processing when flatmap is not possible
  println(errorTest)
}
