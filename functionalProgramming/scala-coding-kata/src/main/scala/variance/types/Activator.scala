package variance.types

import scala.reflect.runtime.universe._

/**
	* Created by Freshwood on 25.07.2016.
	*/
object Activator {

	val mirror = runtimeMirror(getClass.getClassLoader)

	def createInstance[T: TypeTag]: T = {
		val typeInfo = typeOf[T]
		val classSymbol = typeInfo.typeSymbol.asClass
		//ClassCheckMacro.checkClass[T]

		if (classSymbol.isAbstract) throw new IllegalArgumentException("Provided class is abstract")

		val classMirror = mirror.reflectClass(classSymbol)
		val constructorSymbol = typeInfo.decl(termNames.CONSTRUCTOR).asMethod
		val constructorMirror = classMirror.reflectConstructor(constructorSymbol)
		constructorMirror().asInstanceOf[T]
	}

	def createInstance[T: TypeTag](param: Seq[_]): T = {
		val typeInfo = typeOf[T]
		val mirror = runtimeMirror(getClass.getClassLoader)
		val clsSym = typeInfo.typeSymbol.asClass
		val clsMirror = mirror.reflectClass(clsSym)

		val constructorSymbol = typeInfo.decl(termNames.CONSTRUCTOR)

		val defaultConstructor =
			if (constructorSymbol.isMethod) {
				constructorSymbol.asMethod
			} else {
				val constructors = constructorSymbol.asTerm.alternatives
				constructors.map(_.asMethod).find(_.isPrimaryConstructor).get
			}

		val ctorDefault = clsMirror.reflectConstructor(defaultConstructor)
		ctorDefault(param: _*).asInstanceOf[T]
	}
}
