package variance.types

import variance.Variancing.{Two, base}

import scala.reflect.runtime.universe._


/**
	* Created by Freshwood on 24.07.2016.
	*/
object TypeBuilder extends App {



	def createInstance[T:TypeTag]() : T= {
		createInstance(typeOf[T])
	}


	def createInstance[T : TypeTag](tpe:Type): T = {
		val mirror = runtimeMirror(getClass.getClassLoader)
		val clsSym = tpe.typeSymbol.asClass
		val clsMirror = mirror.reflectClass(clsSym)
		val ctorSym = tpe.decl(termNames.CONSTRUCTOR).asMethod
		val ctorMirror = clsMirror.reflectConstructor(ctorSym)
		val instance = ctorMirror().asInstanceOf[T]

		val typeSymbol = tpe.typeSymbol.asClass

		val isAbstract = typeSymbol.isAbstract

		val contructorSymbol = tpe.decl(termNames.CONSTRUCTOR)

		val defaultConstructor =
			if (contructorSymbol.isMethod) contructorSymbol.asMethod
			else {
				val ctors = contructorSymbol.asTerm.alternatives
				ctors.map (_.asMethod).find(_.isPrimaryConstructor).get
			}

		val ctorDefault = clsMirror.reflectConstructor(defaultConstructor)
		ctorDefault(args: _*).asInstanceOf[T]

		instance
	}

	val one: base = createInstance[Two]()
	one.id = 10

	println(one.id)
}
