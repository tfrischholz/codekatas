package variance.types

import scala.reflect.runtime.universe._


/**
	* Created by Freshwood on 24.07.2016.
	*/
object TypeTagAndElse extends App {

	def paramInfo[T](x: T)(implicit tag: TypeTag[T]): Unit = {
		val targs = tag.tpe match { case TypeRef(_, _, args) => args }
		println(s"type of $x has type arguments $targs")
	}

	def paramInfoEx[T: TypeTag](x: T): Unit = {
		val targs = typeOf[T] match { case TypeRef(_, _, args) => args }
		println(s"type of $x has type arguments $targs")
	}

	def weakParamInfo[T](x: T)(implicit tag: WeakTypeTag[T]): Unit = {
		val targs = tag.tpe match { case TypeRef(_, _, args) => args }
		println(s"type of $x has type arguments $targs")
	}

	paramInfo("Hallo")
	paramInfoEx("Hallo")
	weakParamInfo("Hallo")
}
