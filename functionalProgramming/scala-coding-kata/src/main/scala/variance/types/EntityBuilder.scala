package variance.types

import scala.reflect.runtime.universe._

/**
	* The trait [[CanBuildWithEntityBuilder]] describes how the entity should be build
	*
	*/
trait CanBuildWithEntityBuilder[A] {

	var that: A

	def build: A

	def `with`(f: (A => Unit)*): this.type
}

/**
	* The entity builder which create types by the given type param
	*
	*/
class EntityBuilder[A : TypeTag] extends CanBuildWithEntityBuilder[A] {
	override var that: A = if (that != null) that else Activator.createInstance[A]

	override def build: A = that

	override def `with`(f: (A => Unit)*): EntityBuilder.this.type = {
		f.foreach(_ (that))
		this
	}
}