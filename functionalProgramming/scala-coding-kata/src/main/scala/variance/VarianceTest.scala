package variance

import variance.types.Activator

import scala.reflect.ClassTag

/**
	* Created by Freshwood on 23.06.2016.
	*/
object VarianceTest extends App {

	trait FinalResult

	abstract class base {


		var id: Int = _
	}

	class One extends base {
		val one = 1
	}

	class Two extends One with FinalResult {
		val two = 2
	}

	class Three extends Two with FinalResult {
		val three = 3
	}

	class Four(str: String, number: Int) extends Three {
		val four = 4

		def getStr = str

		def getNumber = number
	}

	class TestVariance[A >: Two <: FinalResult] {

		val retVal: Two = new Four("2", 2)

		def asList: A = retVal
	}


	class Test {
		val id = 10
	}

	implicit class Creator[A <: base](entity: A) {

		def getId = {
			entity.id
		}

		def toThree = {
			new Three
		}
	}

	def createInstance[A <: FinalResult](implicit m: ClassTag[A]): A = m.runtimeClass match {
		//case x if x == classOf[base] => new base().asInstanceOf[A]
		case x if x == classOf[One] => new One().asInstanceOf[A]
		case x if x == classOf[Two] => new Two().asInstanceOf[A]
		case x if x == classOf[Three] => new Three().asInstanceOf[A]
		case _ => throw new IllegalArgumentException("Wrong type provided. Please check type system")
	}

	def upl() = {
		createInstance[Three].getId
	}


	val test = createInstance[Two]

	//val result: Four = Activator.createInstance[Four](Seq("Hallo", 100))

	val result = Activator.createInstance[Test]

	println(result.id)

	println(test + "\n" + upl())
}
