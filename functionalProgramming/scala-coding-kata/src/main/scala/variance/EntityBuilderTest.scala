package variance

import variance.types.EntityBuilder

/**
	* Created by Freshwood on 09.10.2016.
	*/
object EntityBuilderTest extends App {

	class Entity {
		var id = 10
	}

	class SuperEntity extends Entity {
		var name = "SuperEntity"
	}

	class SuperDuperEntity extends SuperEntity {
		var superName = "SuperDuberEntity"
	}


	val test = new EntityBuilder[Entity]
	  .`with`(e => e.id = 10, e => e.id = 111)

	val result = test.build

	println(result.id)
}
