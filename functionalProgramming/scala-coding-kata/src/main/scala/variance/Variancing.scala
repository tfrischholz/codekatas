package variance

import scala.reflect.ClassTag

/**
	* Created by Freshwood on 23.06.2016.
	*/
object Variancing extends App {

	abstract class base {
		var id: Int = _
	}

	abstract class One extends base {
		val one = 1
	}

	class Two extends One {
		val two = 2
	}

	class Three extends Two {
		val three = 3
	}

	class Factory {

		type R >: base

		def createInstance[A <: Two](implicit m: ClassTag[A]): A = m.runtimeClass match {
			case x if x == classOf[Two] => new Two().asInstanceOf[A]
			case x if x == classOf[Three] => new Three().asInstanceOf[A]
			case _ => throw new IllegalArgumentException("Wrong type provided. Please check type system")
		}
	}

	val fac = new Factory().createInstance[Three]

	println(fac.id)
}
