package designpatterns.classic.behaviour

/**
  * @author Freshwood
  * TODO: Recode this shit...
  * @since 20.08.2017
  */
object ObserverPattern extends App {

  val carList: Seq[Car] = Seq(new Car, new Car, new Car)

  var observer = ObserverService(carList.toList)

  // Something has changed notify observer
  observer.notifyObservables()

  // Just add a new car
  val newObserver = observer add new Car

  // update all
  newObserver.notifyObservables()
}

trait Observable {
  def update(): Unit
}

trait Observer {
  def observables: List[Observable]

  def add(observable: Observable): Observer

  def remove(observable: Observable): Observer

  def notifyObservables(): Unit = observables foreach (_.update())
}

class Car extends Observable {
  override def update(): Unit = println("Hey I got an update")
}

case class ObserverService(observables: List[Observable]) extends Observer {

  override def add(observable: Observable) =
    ObserverService(observable :: observables)

  override def remove(observable: Observable) =
    ObserverService(observables filter (_ != observable))
}
