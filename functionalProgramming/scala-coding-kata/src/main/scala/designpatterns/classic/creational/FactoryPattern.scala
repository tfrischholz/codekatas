package designpatterns.classic.creational

import scala.reflect.ClassTag

/**
  * A advanced factory test
  */
object FactoryPattern extends App {

  val factory = new ModelFactory

  val test: Model = new Model {
    override def name: String = "Model"
  }

  val car = factory.get[Car](test)

  val customer = factory.get[Customer](test)

  println(car)
  println(customer)
}

// protocol
private[creational] sealed trait Model {
  def name: String
}

case class Customer(name: String) extends Model

case class Car(name: String) extends Model

private[creational] trait Factory[+A] {
  def get[B](model: Model)(implicit tag: ClassTag[B]): A
}

private[creational] class ModelFactory extends Factory[Model] {
  override def get[B](model: Model)(implicit tag: ClassTag[B]): Model =
    tag.runtimeClass match {
      case x if x == classOf[Car]      => Car(model.name)
      case x if x == classOf[Customer] => Customer(model.name)
      case _ =>
        throw new IllegalArgumentException("Factory can not handle this type")
    }
}
