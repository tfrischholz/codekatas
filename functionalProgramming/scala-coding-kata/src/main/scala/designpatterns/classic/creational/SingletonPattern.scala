package designpatterns.classic.creational

/**
  * Shows the standard usage of the singleton pattern
  * It should only be one instance available
  */
object SingletonPattern extends App {

  object Singleton {
    def name: String = "Hallo Welt"
  }

  println(Singleton.name)
}
