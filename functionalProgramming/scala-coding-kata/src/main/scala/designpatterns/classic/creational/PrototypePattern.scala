package designpatterns.classic.creational

/**
  * @author Freshwood
  * @since 26.06.2018
  * The prototype pattern provides an interface which tells to create a clone of the current object
  * This pattern should be used when an object creation is directly costly
  */
object PrototypePattern extends App {}
