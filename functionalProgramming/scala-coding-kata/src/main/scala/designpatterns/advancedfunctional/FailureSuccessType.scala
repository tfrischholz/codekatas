package designpatterns.advancedfunctional

/** This is a selfmade test via monad's to get a neither type
  *
  * @author Freshwood
  * @since 05.11.2017
  */
object FailureSuccessType extends App {

  val failure: Validation[Int] = Validation(10)

  val success: Validation[Int] = Validation(10)

  println(failure.isFailure)

  println(success.get)

  val mapped = success.map(_.toString)

  println(mapped.get)

  val test = for {
    one <- failure
    two <- success
  } yield one + two

  println(test)

}

sealed trait Validation[+S] { self =>
  def isSuccess: Boolean

  def isFailure: Boolean

  def get: S

  def map[B](f: S => B): Validation[B] =
    if (isSuccess) ValidationSuccess(f(this.get)) else ValidationFailure

  def flatMap[B](f: S => Validation[B]): Validation[B] =
    if (isSuccess) f(this.get) else ValidationFailure
}

object Validation {
  def apply[A](input: A): Validation[A] =
    if (input != null) ValidationSuccess(input) else ValidationFailure
}

case class ValidationSuccess[+S](value: S) extends Validation[S] {
  override def isSuccess = true

  override def isFailure = false

  override def get: S = value
}

case object ValidationFailure extends Validation[Nothing] {
  override def isSuccess = false

  override def isFailure = true

  override def get: Nothing =
    throw new IllegalArgumentException(
      "A failure in validation can not have a value")
}
