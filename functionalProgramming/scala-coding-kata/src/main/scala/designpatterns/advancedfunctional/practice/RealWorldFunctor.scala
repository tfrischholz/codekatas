package designpatterns.advancedfunctional.practice

import scala.language.higherKinds

/**
  * @author Freshwood
  * @since 16.11.2017
  * We have the following business case:
  * A car should be transformed to everything
  */
object RealWorldFunctor extends App {

  // In this example we want to transform a car to something else
  val car: Car[String] = Car(100).map(_.toString)

  println(car.data)

}

case class Car[S](data: S) extends Functor[Car] {

  override def fmap[A, B](fa: Car[A])(f: A => B): Car[B] = Car(f(fa.data))

  def map[B](f: S => B): Car[B] = Car(f(data))
}

case class Destroyed[S](data: S) extends Functor[Destroyed] {
  override def fmap[A, B](fa: Destroyed[A])(f: A => B) = Destroyed(f(fa.data))
}

sealed trait Functor[F[_]] {
  def fmap[A, B](fa: F[A])(f: A => B): F[B]
}

sealed trait NaturalTransformation[-F[_], +G[_]] {
  def apply[A](fa: F[A]): G[A]
}

object NaturalTransformation {
  type ~>[-F[_], +G[_]] = NaturalTransformation[F, G]
}

object Transformations {
  implicit object CarTransformer extends NaturalTransformation[Car, Destroyed] {
    override def apply[A](fa: Car[A]): Destroyed[A] = Destroyed(fa.data)
  }
}
