package designpatterns.advancedfunctional.practice

/**
  * @author Freshwood
  * @since 11.12.2017
  */
object FunctionOfFunctions extends App with TestApiConfiguration {

  // You can defeat this with scala implicits
  // For example when you give the query as last param as implicit value

  val transform: String => String => String = query =>
    url =>
      if (url.startsWith("/dev/")) {
        url
      } else {
        rootUrl + url + s"?$query"
  }

  def process(query: String): String = transform(query)(url)

  println(process("?test"))
}

object FunctionWithImplicits extends App with TestApiConfiguration {

  implicit val query: String = "?test"

  val transform: String => String =
    url =>
      if (url.startsWith("/dev/")) {
        url
      } else {
        rootUrl + url
    }

  private def process(implicit query: String) = transform(url) + query

  println(process)
}

trait TestApiConfiguration {
  val rootUrl: String = "http://test.de"
  val url: String = "/dev/data/some.json"
}
