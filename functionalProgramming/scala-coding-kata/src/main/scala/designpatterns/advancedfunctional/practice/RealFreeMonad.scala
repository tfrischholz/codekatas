package designpatterns.advancedfunctional.practice

import scala.language.higherKinds

/**
  * @author Freshwood
  * @since 29.01.2018
  * This is a self implementation of free monads to get the understanding of this pattern
  * Free of evaluation
  * super to scale
  * Push ops on the heap not on the stak
  */
object RealFreeMonad extends App {

  /**
    * DATA STRUCTURE
    */
  case class DataNumber[S](input: S) extends Monad[DataNumber] {
    override def pure[A](input: A): DataNumber[A] = DataNumber(input)

    override def flatMap[A, B](a: DataNumber[A])(
        fa: A => DataNumber[B]): DataNumber[B] = fa(a.input)
  }
  def add[T](a: DataNumber[T]): Free[DataNumber, T] = Suspend(a)
  def remove[T](a: DataNumber[T]): Free[DataNumber, T] = Suspend(a)
  def result[T](a: DataNumber[T]): Free[DataNumber, T] = Return(a.input)

  val program: Free[DataNumber, Int] = for {
    _ <- add(DataNumber(100))
    _ <- remove(DataNumber(30))
    results <- result(DataNumber(10))
  } yield results

  /**
    * So here is something we want to transform the result
    */
  /*case class Outcome[A](value: A) extends Monad[Outcome]
  object DataToOutcome extends (DataNumber ~> Outcome) {
    override def apply[A](f: DataNumber[A]): Outcome[A] = Outcome(f.input)
  }

  /**
 * Recursive interpretation of the free type
 * Type F is the Free Type aka program
 * Type G is the resulting main type
 * Type A is the resulting value wrapped by G
 */
  def runFree[F[_], G[_]: Monad, A](free: Free[F, A])(transform: F ~> G): G[A] = {

    val G = implicitly[Monad[G]] // uses implicit objects in constructor

    free match {
      case Return(x) => G.pure(x)
      case Suspend(x) => transform.apply(x)
      case FlatMap(Suspend(fa), fn) =>
        G.flatMap(transform(fa)) { a =>
          runFree(fn(a))(transform)
        }
    }
  }*/

}

/**
  * DEFINITION
  * Free is the program
  * S is the language
  * A is the type of a value it will produce (once it is run)
  */
sealed trait Free[S[_], A] {

  def pure[T](a: T): Free[S, T] = Return(a)

  def map[B](fn: A => B): Free[S, B] =
    flatMap(a => pure(fn(a)))

  def flatMap[B](fn: A => Free[S, B]): Free[S, B] =
    FlatMap(this, fn)
}

// Will be used as the result of the free evaluation
case class Return[F[_], A](input: A) extends Free[F, A]

// Just a class which points to the next operation (Suspend (aussetzen))
case class Suspend[F[_], A](fn: F[A]) extends Free[F, A]

// Will be used to flatMap incoming data (Like a tree which we can traverse)
case class FlatMap[F[_], A, B](free: Free[F, A], fn: A => Free[F, B])
    extends Free[F, B]

trait Monad[M[_]] {
  def pure[A](input: A): M[A]
  def flatMap[A, B](a: M[A])(fa: A => M[B]): M[B]
}

// AKA Natural Transformation
trait FunctorTransformer[-F[_], +G[_]] {
  def apply[A](f: F[A]): G[A]
}

object FunctorTransformer {
  type ~>[F[_], G[_]] = FunctorTransformer[F, G]
}
