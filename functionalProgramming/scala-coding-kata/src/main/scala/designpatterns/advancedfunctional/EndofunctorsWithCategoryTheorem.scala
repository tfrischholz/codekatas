package designpatterns.advancedfunctional

import scala.language.higherKinds

/**
  * @author Freshwood
  * @since 25.09.2017
  */
object EndofunctorsWithCategoryTheorem extends App {

  trait Monoid[A] {
    def identity(x: A): A
    def append(x: A, y: A): A
  }

  trait Functor[F[_]] {
    def map[A, B](fa: F[A])(f: A => B): F[B]
  }

  trait Monad[M[_]] extends Functor[M] {
    def pure[A](identity: A): M[A]

    def flatMap[A, B](ma: M[A])(f: A => M[B]): M[B]

    override def map[A, B](fa: M[A])(f: (A) => B): M[B] =
      flatMap(fa)(out => pure(f(out)))
  }

  // MONOID SAMPLES

  implicit object ListMonoid extends Monoid[List[Int]] {
    override def identity(x: List[Int]): List[Int] = List.empty

    override def append(x: List[Int], y: List[Int]): List[Int] = x ++ y
  }

  implicit object MapMonoid extends Monoid[Map[Int, Int]] {
    override def identity(x: Map[Int, Int]): Map[Int, Int] = Map.empty

    override def append(x: Map[Int, Int], y: Map[Int, Int]): Map[Int, Int] =
      x ++ y
  }

  def plus[A](input: A)(implicit monoid: Monoid[A]) =
    monoid.append(input, input)

  println(plus(Map[Int, Int](100 -> 100)))

  // Functor Samples

  implicit object ListFunctor extends Functor[List] {
    override def map[A, B](fa: List[A])(f: (A) => B): List[B] = fa map f
  }

  val listTest: List[Int] = (1 to 100) toList

  val list: List[String] = ListFunctor.map(listTest)(_.toString)

  println(list)

  // Monad Samples

  implicit object OptionMonad extends Monad[Option] {
    override def pure[A](identity: A): Option[A] = Some(identity)

    override def flatMap[A, B](ma: Option[A])(f: (A) => Option[B]): Option[B] =
      if (ma.isDefined) f(ma.get) else None
  }

  def optionTest[A](input: Option[A])(
      implicit monad: Monad[Option]): Option[String] =
    monad.flatMap(input)(f => Option(f toString))

  println(optionTest(Some(1000)))
}
