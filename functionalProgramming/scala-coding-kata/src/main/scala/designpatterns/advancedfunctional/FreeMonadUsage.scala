package designpatterns.advancedfunctional

import scala.language.{existentials, higherKinds, postfixOps, reflectiveCalls}

/**
  * @author Freshwood
  * @since 10.09.2017
  * This example should show a example implementation of a free monad
  * A free monad is an evaluation free (computed free) object model
  */
// Protocol
trait Monoid[A] {
  def identity: A

  def append(a: A, b: A): A
}

trait Functor[F[_]] {
  def fmap[A, B](fa: F[A])(f: A => B): F[B]
}

trait Monad[M[_]] extends Functor[M] {
  def pure[A](a: A): M[A]

  def flatMap[A, B](ma: M[A])(f: A => M[B]): M[B]

  override def fmap[A, B](fa: M[A])(f: A => B): M[B] = flatMap(fa) { monad =>
    val b: B = f(monad)
    val result: M[B] = pure(b)
    result
  }

  def identity[A]: A => M[A] = pure

  def append[A, B, C](m1: A => M[B], m2: B => M[C]): A => M[C] =
    a => flatMap(m1(a))(m2)
}

// AKA Natural Transformation
trait FunctorTransformer[-F[_], +G[_]] {
  def apply[A](f: F[A]): G[A]
}

object FunctorTransformer {
  type ~>[F[_], G[_]] = FunctorTransformer[F, G]
}

/**
  * “free” of interpretation of the contained data.
  */
trait Free[F[_], A] {
  def pure[T](a: T): Free[F, T] = Return(a)
  def flatMap[B](fn: A => Free[F, B]): Free[F, B] = FlatMap(this, fn)
  def map[B](fn: A => B): Free[F, B] = flatMap(a => pure(fn(a)))
}

// Will be used as the result of the free evaluation
case class Return[F[_], A](input: A) extends Free[F, A]

// Just a class which points to the next operation
case class Suspend[F[_], A](fn: F[A]) extends Free[F, A]

// Will be used to flatMap incoming data (Like a tree which we can traverse)
case class FlatMap[F[_], A, B](free: Free[F, A], fn: A => Free[F, B])
    extends Free[F, B]

/**
  * Just a monoid examples with identity and append
  */
object MonoidExamples extends App {

  object Monoids {

    implicit case object IntMonoid extends Monoid[Int] {
      def identity: Int = 0

      def append(a: Int, b: Int): Int = a + b
    }
  }

  println(Monoids.IntMonoid.append(10, 13))

  case class Tester[A](value: A)(implicit monoid: Monoid[A]) {
    def sum: A = monoid append (value, value)
  }

  import Monoids._
  println(Tester(100).sum)
}

/**
  * Monad examples which is only a wrapper over a type
  * Important are flatMap e.g. bind (We have to bind functions)
  * When you see the monad trait flatMap and pure has to be implemented
  * Because this logic is model specific but is necessary for the associativity law
  * Example own Option implementation
  */
object MonadExamples extends App {
  sealed trait Option[+A]
  case class Some[A](a: A) extends Option[A]
  case object None extends Option[Nothing]

  object OptionMonad extends Monad[Option] {
    override def pure[A](a: A): Option[A] = Some(a)

    override def flatMap[A, B](ma: Option[A])(f: (A) => Option[B]): Option[B] =
      ma match {
        case Some(x) => f(x)
        case None    => None
      }
  }

  val test: Option[String] = Some("Hallo Welt")
  val test2: Option[String] = OptionMonad.fmap(test)(f => f + " Jo des stimmt")
  // As you can see we can compute in the object without manipulating the class result
  // We can also use for <- with this
  println(test2)
}

object FunctorWithTransformationExample extends App {

  case class Todo[A](input: A)
  case class Task[A](input: A)

  object Functors {
    import FunctorTransformer._

    object TodoFunctor extends Functor[Todo] {
      override def fmap[A, B](fa: Todo[A])(f: (A) => B): Todo[B] =
        Todo(f(fa.input))
    }

    object TaskFunctor extends Functor[Task] {
      override def fmap[A, B](fa: Task[A])(f: (A) => B): Task[B] =
        Task(f(fa.input))
    }

    object TodoToTask extends FunctorTransformer[Todo, Task] {
      override def apply[A](f: Todo[A]): Task[A] = Task(f.input)
    }

    // This time with the scala special notation (No magic here just a other write combination)
    // ~>[Task, Toodo] would also be possible
    object TaskToTodo extends (Task ~> Todo) {
      override def apply[A](f: Task[A]): Todo[A] = Todo(f.input)
    }
  }

  val intTodo: Todo[Int] = Todo(100)
  // Functor in action
  val stringTodo: Todo[String] =
    Functors.TodoFunctor.fmap(intTodo)(f => f toString)

  val toTask: Task[String] = Functors.TodoToTask(stringTodo)

  println(toTask)

  // Keep in mind this is only for learning here
  // In production we can make the Functors Object implicit
  // We have to write less and have the right objects available
  // Thanks to the functor, monoid and monad laws
}

/**
  * Just an examples how to use free Monads (free of evaluation)
  */
object FreeMonadExamples extends App {

  // Why we need all the different models
  // An Interpreter can handle all the different tasks on its own interpretation
  case class Todo[A](input: A)
  def newTodo[A](todo: A): Free[Todo, A] = Suspend(Todo(todo))

  /**
    * Keep in mind the _ syntax makes an FlatMap Object not an Suspend :)
    */
  val todos: Free[Todo, String] =
    for {
      _ <- newTodo("Go to scala days")
      _ <- newTodo("Write a novel")
      _ <- newTodo("Meet Tina Fey")
      _ <- newTodo("Go to scala days")
      _ <- newTodo("Fuck this shit")
      todos <- newTodo("Done")
    } yield todos

  // make Option a monad so we can use it in our `runFree` code
  implicit val optMonad: Monad[Option] = new Monad[Option] {
    def pure[A](given: A): Option[A] = Some(given)
    def flatMap[A, B](given: Option[A])(fn: A => Option[B]): Option[B] =
      given match {
        case Some(o) => fn(o)
        case None    => None
      }
  }

  import FunctorTransformer._

  case object PrintEvaluator extends FunctorTransformer[Todo, Option] {
    def apply[A](a: Todo[A]): Option[A] = {
      a match {
        case Todo(todo) =>
          println(s"New Todo added: $todo")
          Some(todo)
      }
    }
  }

  def runFree[F[_], G[_]: Monad, A](f: Free[F, A])(transform: F ~> G): G[A] = {
    @annotation.tailrec
    def tailThis(free: Free[F, A]): Free[F, A] = free match {
      case FlatMap(FlatMap(fr, fn1), fn2) =>
        tailThis(fr.flatMap(fn1).flatMap(fn2))
      case FlatMap(Return(a), fn) => tailThis(fn(a))
      case _                      => free
    }

    val G = implicitly[Monad[G]] // uses implicit objects in constructor

    tailThis(f) match {
      case Return(a)   => G.pure(a)
      case Suspend(fa) => transform(fa)
      case FlatMap(Suspend(fa), fn) =>
        G.flatMap(transform(fa)) { a =>
          runFree(fn(a))(transform)
        }
      case _ => throw new AssertionError("Unreachable")
    }
  }

  runFree(todos)(PrintEvaluator)
}

/**
  * This is just an example from my own understanding about this free monad implementation
  */
object ListFreeMonadExample extends App {

  // Business Object to handle
  case class Task[A](input: A)
  case class Output[S](outcome: S)

  case class TaskList[S](tasks: List[Task[S]]) extends Monad[Task] {

    override def pure[A](a: A): Task[A] = Task(a)

    override def flatMap[A, B](ma: Task[A])(f: A => Task[B]): Task[B] =
      f(ma.input)

    def add(task: Task[S]): TaskList[S] = TaskList(task :: tasks)
  }

  import FunctorTransformer._

  object TaskToOutput extends (Task ~> Output) {
    override def apply[A](f: Task[A]): Output[A] = {
      println(s"Got input: ${f.input}")
      Output(f.input)
    }
  }

  class TaskRunner(transformer: Task ~> Output) {
    def run[A](tasks: List[Task[A]]): List[Output[A]] =
      tasks.map(f => transformer(f))
  }

  val task: Task[String] = Task("Make something")
  val taskList: TaskList[String] = TaskList(List(task))
  val list: TaskList[String] = taskList.add(Task("Hallo Welt"))
  val list2: TaskList[String] = list.add(Task("Done"))

  val runner = new TaskRunner(TaskToOutput)

  runner.run(list2.tasks)
}
