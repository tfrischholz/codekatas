package designpatterns

import scala.annotation.implicitNotFound
import scala.language.{higherKinds, postfixOps}

/**
  * @author Freshwood
  * @since 03.09.2017
  *        https://hseeberger.wordpress.com/2010/11/25/introduction-to-category-theory-in-scala/
  */
object CategoryTheorem extends App {

  /**
    * A simple category object which provides an identity function
    * And a composer which can build up a new Category from ->
    * A -> B and B -> C || 1. call f -> the result applied with 2. g -> A -> B -> C
    */
  object Category {
    def id[A]: A => A = a => a

    def compose[A, B, C](g: B => C, f: A => B): A => C = a => g(f(a))
  }

  val id = Category.id(10) // should be 10 cause identity

  println(id)

  // Composer test A: Animal -> B: Cow -> C: Grass
  val AtoB: Animal => Cow = animal => Cow(animal.name)
  val BtoC: Cow => Grass = cow => Grass(cow.name)

  val composed: Animal => Grass = Category.compose(BtoC, AtoB) // A => C

  val testAnimal = Animal("I am a animal")

  println(composed(testAnimal)) // The result should be a Grass
}

sealed trait Thing {
  def name: String
}

sealed case class Animal(name: String) extends Thing

sealed case class Cow(name: String) extends Thing

sealed case class Grass(name: String) extends Thing

/**
  * Another example... this time with maps
  */
object CategoryTheorem2 extends App {

  // ->>[_,_] == Map[A, B]
  trait GenericCategory[->>[_, _]] {
    def id[A]: A ->> A
    def compose[A, B, C](g: B ->> C, f: A ->> B): A ->> C
  }

  /**
    * Function is a typical Scala Function -> Function[-A, +B]
    */
  implicit object Category2 extends GenericCategory[Function] {
    def id[A]: A => A = a => a
    def compose[A, B, C](g: B => C, f: A => B): A => C = a => g(f(a))
  }

  val id = Category2.id(10) // should be 10 cause identity

  println(id)

  // Composer test A: Animal -> B: Cow -> C: Grass
  val AtoB: Animal => Cow = animal => Cow(animal.name)
  val BtoC: Cow => Grass = cow => Grass(cow.name)

  val composed: Animal => Grass = Category2.compose(BtoC, AtoB) // A => C

  val testAnimal = Animal("I am a animal")

  println(composed(testAnimal)) // The result should be a Grass
}

/**
  * Now that we know categories and how to represent certain ones in Scala,
  * let’s look at another important concept of category theory, which is also very important for functional programming.
  * Consider two categories C1 and C2; then a functor F is a structure-preserving mapping between these categories
  */
object FunctorTheorem extends App {

  trait GenericFunctor[->>[_, _], ->>>[_, _], F[_]] {
    def fmap[A, B](f: A ->> B): F[A] ->>> F[B]
  }

  /**
    * This results in a so called endofunctor (same source and target) and looks like the following
    * Please note, that the new fmap method is just for convenience and delegates to the inherited one.
    * Using the higher kinded type for the first parameter list
    * makes it possible to infer the type of A for the function in the second parameter list.
    */
  @implicitNotFound("I could not find that functor shit")
  trait Functor[F[_]] extends GenericFunctor[Function, Function, F] {
    final def fmap[A, B](as: F[A])(f: A => B): F[B] = fmap(f)(as)
  }

  /**
    * In order to code up some examples, we have to specify the type constructor F. Let’s start with good old List:
    */
  object ListFunctor extends Functor[List] {
    override def fmap[A, B](f: A => B): List[A] => List[B] = as => as map f
  }

  object Functor {

    implicit object Function0Functor extends Functor[Function0] {
      def fmap[A, B](f: A => B): Function0[A] => Function0[B] =
        a => () => f(a())
    }

    implicit object OptionFunctor extends Functor[Option] {
      def fmap[A, B](f: A => B): Option[A] => Option[B] =
        o => o map f
    }

    implicit object ListFunctor extends Functor[List] {
      def fmap[A, B](f: A => B): List[A] => List[B] =
        as => as map f
    }
    def fmap[A, B, F[_]](as: F[A])(f: A => B)(
        implicit functor: Functor[F]): F[B] =
      functor.fmap(as)(f)
  }

  import Functor._

  val listTest: List[Int] = fmap(List(1, 2, 3))(x => x + 1)
  val optionTest: Option[Int] = fmap(Option(1))(x => x + 1)

  println(listTest)
  println(optionTest)

  // Function0 example
  val f: String => Int = _.length
  val lifted: () => Int = fmap(() => "abc")(f)

  val testNew: Function0[Int] = () => 10

  println(lifted())
}

object Function0Extender extends App {

  implicit class Function0ExtenderSample[A](input: A) extends Function0[A] {

    def woot(f: A): A = {
      println(f)
      f
    }

    override def apply(): A = {
      println(input)
      input
    }
  }

  val test: Function0[Int] = () => 100

  println(test.woot(100))

}
