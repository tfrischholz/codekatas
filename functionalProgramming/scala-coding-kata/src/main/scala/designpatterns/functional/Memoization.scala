package designpatterns.functional

import java.util.UUID

import scala.collection.mutable
import scala.util.Random

/**
  * Created by Freshwood on 29.04.2017.
  */
object Memoization extends App {

  case class Memo[A, B](f: A => B) extends (A => B) {
    private val cache = mutable.Map.empty[A, B]
    def apply(x: A): B = {
      println(cache)
      cache getOrElseUpdate (x, f(x))
    }
  }

  val fibonacci: Memo[Int, BigInt] = Memo {
    case 0 => 0
    case 1 => 1
    case n => fibonacci(n - 1) + fibonacci(n - 2)
  }

  println(fibonacci(500))
}

object MemoizationExample extends App {

  case class StoreData(one: String, two: String, three: String)

  case class RawData(uniqueId: UUID, one: String, two: String, three: String)

  def id: UUID = UUID.randomUUID()

  val ids: Seq[UUID] = 1 to 100 map (_ => id)

  lazy val testData: Seq[RawData] = ids map (id =>
    RawData(this.id, s"One $id", s"Two $id", s"Three $id"))

  object Store {
    private val cache = mutable.Map.empty[UUID, StoreData]

    private def generateStoreData: StoreData =
      StoreData(Random.nextString(5),
                Random.nextString(5),
                Random.nextString(5))

    def get(key: UUID): StoreData = {
      println(cache)
      cache getOrElseUpdate (key, generateStoreData)
    }
  }

  val firstId = ids.head

  println(Store.get(firstId))

  Thread.sleep(5000)

  println(Store.get(firstId))
}
