package designpatterns.functional

/**
  * Created by Freshwood on 25.04.2017.
  * Make Function to Monoid's like Int => Int
  */
object FunctionalComposition extends App {

  type IntToString = Int => String

  type StringToInt = String => Int

  type IntToInt = Int => Int

  val first: IntToString = input => input.toString

  val second: StringToInt = input => Integer.parseInt(input) + 1

  val result: IntToInt = first andThen second

  println(result(10))

  // Just a sample to compose to a Monoid Type
  val lol: String => String = first.compose(second)

  println(lol("10"))
}

object SpecialFunctionalComposing extends App {

  type Mono = Int => Int

  type Another = Int => Int

  type Asserty = (Mono, Another) => Boolean

  private def assertTypes(first: Mono, second: Another): Boolean =
    first == second

  val test: Int => Int = _ => 10

  println(assertTypes(test, test))

  val asserty: Asserty = (one, second) => one == second

  println(asserty(test, test))
  println(asserty.tupled((test, test)))
  println(asserty.curried(test)(test))

  type Log = String => Unit

  val log: Log = input => println(input)

  val logs: Seq[Log] = Seq(log, log, log, log, log)

  val forAll = logs.foldLeft(1) { (x, y) =>
    y(s"Log $x")
    x + 1
  }
}

object ComposingLearningApp extends App {

  type Mono[S] = S => S

  type INT = Mono[Int]

  type STRING = Mono[String]

  val simpleInt: INT = input => input * 100

  val simpleString: STRING = string => string * 100

  println(simpleInt(11))

  println(simpleString("Hallo "))

  def composing[A, B](a: Mono[A])(b: Mono[B])(callBack: => Unit): Mono[A => B] =
    input => { f =>
      callBack
      input(f)
    }

  val fn = composing(simpleInt)(simpleString)(())

  println(fn(int => int.toString)(100))
}

object ComposingLearning2App extends App {

  def composer[A, B](a: Int => Int)(b: String => String): String => Int =
    input => {
      a(b(input).toInt)
    }

  val result = composer(f => f * 10)(f => f * 3)

  val fn = result("100")

  println(fn)
}

object ComposerForDummies extends App {

  type INT = Int => Int

  type STRING = String => String

  type TOGETHER = Int => String

  val inty: INT = int => int * 3

  val stringy: STRING = string => string * 3

  val together: TOGETHER = to => to.toString + "-Together"

  val what: (Int) => String = inty andThen together

  val composed: (Int) => String = together compose inty

  val back: (Int) => String = together andThen (f => (f + " ") * 100)

  println(what(10))
  println(composed(10))
  println(back(10))
}
