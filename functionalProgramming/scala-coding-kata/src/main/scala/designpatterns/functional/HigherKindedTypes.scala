package designpatterns.functional

import scala.language.higherKinds

/**
  * @author Freshwood
  * @since 27.08.2017
  * Test implementations from http://adriaanm.github.io/files/higher.pdf
  * https://gist.github.com/jdegoes/97459c0045f373f4eaf126998d8f65dc
  */
object HigherKindedTypes extends App {

  object len {
    def apply(s: String): Int = s.length
  }

  val out = len("Hallo Welt")

  println(out)

  val maybe: Maybe[Int] = Maybe(100)

  val maybe2: Maybe[String] = maybe map (f => f.toString + " Welt")

  println(maybe2)
}

trait Functor[F[_]] {
  def fmap[A, B](fa: F[A])(f: A => B): F[B]
}

trait MapLike[S] {
  def map[A](f: S => A): MapLike[A]
}

case class Maybe[T](value: T) extends Functor[Maybe] with MapLike[T] {
  override def fmap[A, B](fa: Maybe[A])(f: (A) => B): Maybe[B] =
    Maybe(f(fa.value))

  override def map[A](f: T => A): Maybe[A] = fmap(this)(f)
}
