package designpatterns.functional

/**
  * Created by Freshwood on 30.04.2017.
  * Look at the Magnet pattern
  * With this you can extend type classes or for example implicit classes
  * Or extend functionality with implicit context
  * For example the play json writer / reads
  */
object PimpMyLibraryPattern extends App {

  trait PimpMyLibraryContainer[S] {
    def manipulate[A](input: A): S
  }

  // The same as the magnet pattern. Use the companion object to have the implicit available
  object PimpMyLibraryContainer {

    implicit object asString extends PimpMyLibraryContainer[String] {
      override def manipulate[A](input: A): String = input.toString.reverse
    }

    implicit object asInt extends PimpMyLibraryContainer[Int] {
      override def manipulate[A](input: A): Int =
        Integer.parseInt(input.toString) * 100
    }

  }

  def sampleMethod[A](input: A)(implicit pimp: PimpMyLibraryContainer[A]): A =
    pimp.manipulate(input)

  println(sampleMethod(100))

  println(sampleMethod("Hallo"))

  // The implicit class does belongs to the pimp my library pattern in some way
}
