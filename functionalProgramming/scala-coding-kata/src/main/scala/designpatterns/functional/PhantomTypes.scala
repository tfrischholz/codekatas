package designpatterns.functional

import scala.annotation.implicitNotFound

/**
  * Created by Freshwood on 10.07.2017.
  * The phantom type is used to check the correct type for further working
  */
object PhantomTypes extends App {

  sealed trait InitializationState
  final class NotInitialized extends InitializationState
  final class Initialized extends InitializationState

  class Builder[S <: InitializationState] {
    val v: Option[String] = None
    def setV(s: String): Builder[Initialized] = {
      new Builder[Initialized] {
        override val v = Some(s)
      }
    }
    def build(implicit evidence: S =:= Initialized): String = {
      v.get
    }
  }

  object Builder {
    def apply() = new Builder[NotInitialized]
  }

  // Builder().build
  // cannot compile:
  // Error:(25, 13) Cannot prove that PhantomTypes.NotInitialized =:= PhantomTypes.Initialized.
  // Builder().build
  //           ^
  // Error:(25, 13) not enough arguments for method build: (implicit evidence: =:=[PhantomTypes.NotInitialized,PhantomTypes.Initialized])String.
  // Unspecified value parameter evidence.
  // Builder().build
  //           ^

  val builderResult = Builder().setV("abc").build

  println(builderResult)
  // that one compile
}

/**
  *  https://blog.codecentric.de/en/2016/02/phantom-types-scala/
  */
object Hacker extends App {

  @implicitNotFound("This hacker is in dire need of coffee!")
  type IsCaffeinated[S] = S =:= State.Caffeinated

  @implicitNotFound(
    "This hacker already had coffee and needs to do some hacking first!")
  type IsDecaffeinated[S] = S =:= State.Decaffeinated

  sealed trait State
  object State {
    sealed trait Caffeinated extends State
    sealed trait Decaffeinated extends State
  }

  def caffeinated: Hacker[State.Caffeinated] = new Hacker
  def decaffeinated: Hacker[State.Decaffeinated] = new Hacker

  val coffeeDrinker: Hacker[State.Caffeinated] = caffeinated

  //println(coffeeDrinker.drinkCoffee) --> Would not work, cause of phantom type bound

  println(coffeeDrinker.hackOn)

  // println(coffeeDrinker.hackOn.hackOn) --> Would also not work :: fucking leet builder code
}

class Hacker[S <: Hacker.State] private {
  import Hacker._

  def hackOn(implicit ev: IsCaffeinated[S]): Hacker[State.Decaffeinated] = {
    println("Hacking, hacking, hacking!")
    new Hacker
  }

  def drinkCoffee(
      implicit ev: IsDecaffeinated[S]): Hacker[State.Caffeinated] = {
    println("Slurp ...")
    new Hacker
  }
}
