package designpatterns.functional

import scala.language.higherKinds

/**
  * Created by Freshwood on 27.04.2017.
  * Shows usage of the well known Functor pattern
  * e.g.: A => B = F[A] => F[B]
  */
object FunctorPattern extends App {

  trait Functor[F[_]] {
    def fmap[A, B](f: A => B): F[A] => F[B]
  }

  case class TestClass[T](value: T) extends Functor[TestClass] {
    override def fmap[A, B](f: A => B): TestClass[A] => TestClass[B] =
      input => TestClass(f(input.value))
  }

  val intClass: TestClass[Int] = TestClass(10)

  val stringClass: TestClass[Int] => TestClass[String] =
    intClass.fmap(_.toString)

  println(stringClass)

  println(stringClass(intClass))
}

/**
  * Would be the following aggregation
  * F[A => B] = F[B]
  */
object ApplicativeFunctor extends App {

  trait ApplicativeFunctor[F[_]] {
    def apply[A, B](input: F[A])(f: F[A => B]): F[B]
  }

  class OptionTest[T](val value: T) extends ApplicativeFunctor[OptionTest] {
    override def apply[A, B](input: OptionTest[A])(
        f: OptionTest[A => B]): OptionTest[B] = {
      val outcome = f.value.apply(input.value)
      new OptionTest[B](outcome)
    }
  }

  val testFun: Int => Double = value => value.toDouble

  val lol: OptionTest[Int => Double] = new OptionTest(testFun)

  val applicative: OptionTest[Double] = lol.apply(new OptionTest(100))(lol)

  // I know it's quiet verbose, but with implicit objects and pimp my library pattern you have a cool functionality

  println(applicative.value)
}

/**
  * Monad Functor would be of type this:
  * e.g.: F[A] + A => F[B] = F[B] ===> aka flatMap
  */
object MonadFunctor extends App {

  trait MonadFunctor[F[_]] {
    def flatMap[A, B](input: F[A])(f: A => F[B]): F[B]
  }

  case class MayBe[T](value: T) extends MonadFunctor[MayBe] {
    override def flatMap[A, B](input: MayBe[A])(f: (A) => MayBe[B]): MayBe[B] =
      f(input.value)
  }

  val test: MayBe[Int] = MayBe(100)

  val string: MayBe[String] = test.flatMap(test)(f => MayBe(f.toString))

  println(string)
}

/**
  * All in one together we have the full power
  */
object RealWorldFunctor extends App {

  trait GenericFunctor[F[_]] {
    def fmap[A, B](f: A => B): F[A] => F[B]
  }

  trait ApplicativeFunctor[F[_]] {
    def apply[A, B](input: F[A])(f: F[A => B]): F[B]
  }

  trait MonadFunctor[F[_]] {
    def flatMap[A, B](input: F[A])(f: A => F[B]): F[B]
  }

  trait Functor[F[_]]
      extends GenericFunctor[F]
      with ApplicativeFunctor[F]
      with MonadFunctor[F] {

    type S

    def identity[A](a: A): A = a

    def map[B](f: S => B): F[B]
  }

  class MayBe[T](val value: T) extends Functor[MayBe] {

    override type S = T

    override def apply[A, B](input: MayBe[A])(f: MayBe[(A) => B]): MayBe[B] = {
      val outcome = f.value.apply(input.value)
      new MayBe[B](outcome)
    }

    override def map[B](f: (T) => B): MayBe[B] = new MayBe(f(value))

    override def flatMap[A, B](input: MayBe[A])(f: (A) => MayBe[B]): MayBe[B] =
      f(input.value)

    override def fmap[A, B](f: (A) => B): MayBe[A] => MayBe[B] =
      input => new MayBe(f(input.value))
  }

  val int: MayBe[Int] = new MayBe(100)

  val string: MayBe[String] = int.map(_.toString)

  println(string.value)
}
