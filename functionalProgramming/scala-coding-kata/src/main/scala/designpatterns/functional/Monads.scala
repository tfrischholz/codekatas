package designpatterns.functional

/**
	* Created by Freshwood on 21.09.2016.
	*/
object Monads extends App {

  val x = 10

  val test = Option(10)

  val test2 = Option(100)

  val result = for {
    t1 <- test
    t2 <- test2
  } yield t1 + t2

  println(result.orElse(test))

  sealed trait Resultable[A] {
    def result: A
  }

  sealed trait MaybeInt[A] extends Resultable[A] {
    def map(f: A => A): MaybeInt[A]

    def flatMap(f: A => MaybeInt[A]): MaybeInt[A]
  }

  case class SomeInt[T](value: T) extends MaybeInt[T] {

    override def map(f: (T) => T): MaybeInt[T] = SomeInt(f(value))

    override def flatMap(f: (T) => MaybeInt[T]): MaybeInt[T] = f(value)

    override def result: T = value
  }

  val ex1 = SomeInt(10)

  val ex2 = SomeInt(20)

  val resultEx = for {
    e1 <- ex1
    e2 <- ex2
  } yield e1 + e2

  println(resultEx.result)

}
