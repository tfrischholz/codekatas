package designpatterns.functional

/**
  * @author Freshwood
  * @since 27.08.2017
  */
object SpecializedTypes extends App {

  /*
  case class Parcel[A](value: A) {
    def something: A = ???
  }

  // specialzation "by hand"
  case class IntParcel(intValue: Int) {
    override def something: Int = /* works on low-level Int, no wrapping! */ ???
  }

  case class LongParcel(intValue: Long) {
    override def something: Long = /* works on low-level Long, no wrapping! */ ???
  }
   */

  // Scala help us here with an annotation for primitives (primitive types)

  case class Parcel[@specialized A](value: A)

  val pi: Parcel[Int] = Parcel(1) // will use `int` specialized methods
  val pl: Parcel[Long] = Parcel(1L) // will use `long` specialized methods
  val pb
    : Parcel[Boolean] = Parcel(false) // will use `boolean` specialized methods
  val po: Parcel[String] = Parcel("pi") // will use `Object` methods
}
