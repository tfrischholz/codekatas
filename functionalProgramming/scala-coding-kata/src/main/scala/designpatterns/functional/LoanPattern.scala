package designpatterns.functional

/**
  * @author Freshwood
  * @since 12.02.2018
  * The loan pattern describes how a runnable program
  * should be catch through a routing
  * See the example
  */
object LoanPattern extends App {

  /**
    * Traditionally you would always wrap the operation with an try catch
    * Or you put this specific try catch inside this function
    */
  object Traditional {

    def devision(input: Int, through: Int): Int = input / through

    try {
      devision(10, 0)
    } catch {
      case ex: ArithmeticException => println(ex.getMessage)
    }

  }

  /**
    * The loan variant provides a way to run a operation on an
    * specific exception handler
    * Normally the loan pattern is used to have a try with resource
    * (a predefined fallback where the user does not have to manage)
    */
  object Loan {

    def loan(operation: => Int): Int = {
      try {
        operation
      } catch {
        case ex: ArithmeticException => throw ex
      }
    }

  }

  println(Loan.loan(Traditional.devision(10, 0)))

}
