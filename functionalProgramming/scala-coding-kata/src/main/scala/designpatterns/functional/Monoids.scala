package designpatterns.functional

import scala.collection.immutable
import scala.util.Random

/**
  * Created by Freshwood on 12.12.2016.
  */
object Monoids extends App {

  // here's our definition of a monoid again
  trait Monoid[A] {
    // an identity element
    def id: A

    // an associative operation
    def op(x: A, y: A): A
  }

  object Monoid {
    //easy to define for strings
    implicit val stringMonoid = new Monoid[String] {
      def id = ""

      def op(x: String, y: String) = x + y
    }

    // here's where things start to get more interesting, this says "I
    // can give you a monoid for any Option[A] if you can give me a
    // monoid for A"
    implicit def optionMonoid[A](implicit am: Monoid[A]): Monoid[Option[A]] =
      new Monoid[Option[A]] {
        def id = None

        def op(x: Option[A], y: Option[A]): Option[A] = (x, y) match {
          case (x, None) => x
          case (None, y) => y
          case (Some(x), Some(y)) =>
            Some(am.op(x, y)) // here we use the A monoid to add two As
        }
      }

    // given an monoid for B, I can give you a monoid for functions
    // returning B, by running the functions on the input and adding the
    // results
    implicit def functionMonoid[A, B](implicit bm: Monoid[B]): Monoid[A => B] =
      new Monoid[A => B] {
        def id = A => bm.id

        def op(x: A => B, y: A => B): A => B = { a =>
          bm.op(x(a), y(a))
        }
      }

    // we can use a monoid to collapse a bunch of values, here we take a
    // list and function that takes us to a value for which we have a
    // Monoid, and we can then collapse the list into a single value.
    implicit def fold[A](la: List[A])(implicit am: Monoid[A]): A =
      la.foldLeft(am.id)(am.op)
  }

  import Monoid._

  // ok, lets use all this to show how we might sole a classic "fizzbuzz" like problem

  // we'll start with some functions
  val fizz: Int => Option[String] = x => if (x % 3 == 0) Some("fizz") else None
  val buzz: Int => Option[String] = x => if (x % 5 == 0) Some("buzz") else None
  val bazz: Int => Option[String] = x => if (x % 7 == 0) Some("bazz") else None

  val funcs = List(fizz, buzz, bazz)

  // we can combine our functions, this works because we can find an
  // option monoid for strings, since we have a monoid for strings,
  // then we can find a monoid for Int => Option[String] since we now
  // have a monoid for Option[String]
  val fizzbuzzbazz = fold(funcs)

  // handle the Nones
  val fbbOrInt: Int => String = { i =>
    (fizzbuzzbazz(i) getOrElse i.toString) + ","
  }

  // map our function on a list
  val strings: List[String] = (1 until 100).toList map fbbOrInt

  // use fold to collapse our strings using the string monoid
  println(fold(strings))

  // 1,2,fizz,4,buzz,fizz,bazz,8,fizz,buzz,11,fizz,13,bazz,fizzbuzz,16,17,fizz,19,buzz,fizzbazz...
}

/**
  * This example shows how Monoid "pattern" is applied to the Fizz Buzz Problem
  *
  * @since 03.05.2017
  */
object MonoidFizzBuzz extends App {

  private val list: immutable.Seq[Int] = (1 to 100).toList

  type FizzBuzz = PartialFunction[Int, String]

  private val fizz: FizzBuzz = { case x if x % 3 == 0      => "Fizz" }
  private val buzz: FizzBuzz = { case x if x % 5 == 0      => "Buzz" }
  private val fizzBuzz: FizzBuzz = { case x if x % 15 == 0 => "FizzBuzz" }
  private val default: FizzBuzz = { case x                 => x.toString }
  private val fizzBuzzed
    : FizzBuzz = fizzBuzz orElse buzz orElse fizz orElse default
  private val matchedNumbers: (Int) => Option[String] =
    (fizzBuzz orElse buzz orElse fizz).lift

  // Now with the functional patterns above you can create new cases with this functionality
  // e.g: Get the number which does not fit

  val result: Seq[Int] = list collect {
    case x if matchedNumbers(x).isEmpty => x
  }

  println(result)

  // Now, you can for example test every number you have if it is "fizzbuzzable muahahaha"

  private val isFizzBuzzable: Int => (Int, Boolean) = input =>
    input -> matchedNumbers(input).isDefined

  private val randomNumber: Int = Random.nextInt + 1

  println(isFizzBuzzable(randomNumber))

  // Now we can map the result above to a scala or OOP conform version... !!!

  trait FizzBuzzService {
    def number: Int
    def isDefined: Boolean
  }

  case class FizzBuzzResult(number: Int) extends FizzBuzzService {
    override def isDefined = true
  }

  case class NoFizzBuzz(number: Int) extends FizzBuzzService {
    override def isDefined = false
  }

  object FizzBuzzService {
    def apply(input: Int): FizzBuzzService = input match {
      case x if matchedNumbers(x).isDefined => FizzBuzzResult(x)
      case y                                => NoFizzBuzz(y)
    }
  }

  private val lol: Int => FizzBuzzService = FizzBuzzService.apply

  println(lol(randomNumber))
}
