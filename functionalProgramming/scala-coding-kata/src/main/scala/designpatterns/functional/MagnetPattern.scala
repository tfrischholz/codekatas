package designpatterns.functional

import scala.language.implicitConversions

/**
  * Created by Freshwood on 24.04.2017.
  */
object MagnetPattern extends App {

  trait MyMagnet {

    type Result

    def getRes: Result
  }

  // With the companion object you don't have to import this object via object._
  object MyMagnet {
    implicit def convertFromInt(myVal: Int): MyMagnet = new MyMagnet {
      override type Result = Int

      override def getRes: Result = myVal
    }

    implicit def convertFromString(myVal: String): MyMagnet = new MyMagnet {
      override type Result = String

      override def getRes: Result = "String type: " + myVal
    }

    implicit def convertFromTwoString(myTuple: (String, String)): MyMagnet =
      new MyMagnet {
        override type Result = String

        override def getRes: Result =
          "String2 type: " + myTuple._1 + ", " + myTuple._2
      }
  }

  def findStickyType(mag: MyMagnet): Unit = {
    println("Finding magnet of some type... " + mag.getRes)
  }

  findStickyType("abc"); // create 'MyMagnet' with String constructor
  findStickyType(123); // create 'MyMagnet' with Int constructor
  findStickyType("xyz", "ijk"); // create 'MyMagnet' with (String, String) tuple constructor

  // Magnet pattern = Instead of method overloading, we use one big pojo with variable amount of fields.
  // Then instead of messy if/else checks on each field of the big pojo to run different init, we use 'implicit' method feature of Scala.
}
