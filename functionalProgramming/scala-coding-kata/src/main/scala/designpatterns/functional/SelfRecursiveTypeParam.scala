package designpatterns.functional

/**
  * @author Freshwood
  * @since 27.08.2017
  */
object SelfRecursiveTypeParam extends App {

  trait Fruit[T <: Fruit[T]] {
    final def compareTo(other: Fruit[T]): Boolean =
      true // impl doesn't matter in our example
  }

  class Apple extends Fruit[Apple]
  class Orange extends Fruit[Orange]
  class AnotherApple extends Apple

  val apple = new Apple
  val orange = new Orange
  val anotherApple = new AnotherApple

  //println(apple compareTo orange) would not compile -> Cause of the self recursive type
  // So now we’re sure we’ll only ever compare apples with apples, and other Fruit with the same kind (sub-class) of Fruit

  println(anotherApple compareTo apple)
}

object SelfTypeExample extends App {

  // With self type it is compile time safety
  trait Doubler[T <: Doubler[T]] { self: T =>
    def double: T
  }

  case class Square(base: Double) extends Doubler[Square] {
    override def double: Square = Square(base * 2)
  }

  // extends Doubler[Square] would be compile error (incompatible type)
  case class Apple(kind: String) extends Doubler[Apple] {
    override def double: Apple = Apple(kind)
  }

  val test: Apple = Apple("Apfel")

  println(test.double)

}
