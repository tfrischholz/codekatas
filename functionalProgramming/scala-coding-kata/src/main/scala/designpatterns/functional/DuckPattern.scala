package designpatterns.functional

import scala.language.reflectiveCalls

/**
  * Created by Freshwood on 30.04.2017.
  * Gives you a compile time possible function reference
  */
object DuckPattern extends App {

  def quacker(duck: { def quack(value: String): String }) {
    println(duck.quack("Quack"))
  }

  object BigDuck {
    def quack(value: String) = {
      value.toUpperCase
    }
  }

  object SmallDuck {
    def quack(value: String) = {
      value.toLowerCase
    }
  }

  object IamNotReallyADuck {
    def quack(value: String) = {
      "prrrrrp"
    }
  }

  quacker(BigDuck)
  quacker(SmallDuck)
  quacker(IamNotReallyADuck)

  /** e.g.:
  * Would not compile
  * object NoQuaker {
  * *
  * }
  * *
  * quacker(NoQuaker)
  */
}

/**
  * Keep in mind structural typing is very expensive
  * Duck / structural typing is using reflection
  */
object DuckRealExampleOrStructuralTyping extends App {

  type woot = { def fuck(): Unit }

  def callSpeak[A <: { def speak(): Unit }](obj: A) = obj.speak()

  def typeSpeak[A <: woot](obj: A) = obj.fuck()

  class Dog {
    def speak() {
      println("woof")
    }
  }

  class Klingon {
    def speak() {
      println("Qapla!")
    }
  }

  class MyTestFuck {
    def fuck(): Unit = {
      println("Fuck")
    }
  }

  callSpeak(new Dog)
  callSpeak(new Klingon)

  //callSpeak(new MyTestFuck) would not compile: does not conform to method...

  typeSpeak(new MyTestFuck)

  //typeSpeak(new Dog) would not compile
}
