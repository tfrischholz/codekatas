package designpatterns.functional

import scala.language.{higherKinds, reflectiveCalls}

/**
  * Created by Freshwood on 01.05.2017.
  */
object TypeLambdas extends App {

  trait Functor[F[_]] {
    def fmap[A, B](f: A => B): F[A] => F[B]
  }

  case class TestClass[T](value: T) extends Functor[TestClass] {
    override def fmap[A, B](f: A => B): TestClass[A] => TestClass[B] =
      input => TestClass(f(input.value))
  }

  val intClass: TestClass[Int] = TestClass(10)

  val stringClass: TestClass[Int] => TestClass[String] =
    intClass.fmap(_.toString)

  println(stringClass)

  println(stringClass(intClass))

  // Now try the same with maps and you will see it is not possible
  type functor = Functor[Option]

  type list = Functor[List]

  //type map = Functor[Map] => this does not work, cause Map needs to type params
  type IntKeyMap[A] = Map[Int, A]

  type F3 = Functor[IntKeyMap] // OK

  // type F4 = Functor[Map[Int, _]] => takes no type parameters, expected: one

  type F5 = Functor[({ type T[A] = Map[Int, A] })#T] // OK
}
