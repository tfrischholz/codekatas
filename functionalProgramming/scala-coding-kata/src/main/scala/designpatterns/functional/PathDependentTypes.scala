package designpatterns.functional

/**
  * @author Freshwood
  * @since 27.08.2017
  */
object PathDependentTypes extends App {

  class Outer {
    class Inner
  }

  val out1 = new Outer
  val out1in = new out1.Inner // concrete instance, created from inside of Outer

  val out2 = new Outer
  val out2in = new out2.Inner // another instance of Inner, with the enclosing instance out2

  // the path dependent type. The "path" is "inside out1".
  type PathDep1 = out1.Inner

  // type checks

  val typeChecksOk: PathDep1 = out1in
  // OK

  //val typeCheckFails: PathDep1 = out2in
  // <console>:27: error: type mismatch;
  // found   : out2.Inner
  // required: PathDep1
  //    (which expands to)  out1.Inner
  //       val typeCheckFails: PathDep1 = out2in

  /*class Parent {
    class Child
  }

  class ChildrenContainer(p: Parent) {
    type ChildOfThisParent = p.Child

    def add(c: ChildOfThisParent) = ???
  }*/

  /**
  * Using the path dependent type we have now encoded in the type system,
  * the logic, that this container should only contain children of this parent - and not "any parent".
  * We’ll see how to require the "child of any parent" Type in the section about Type Projections soon.
  */

}
