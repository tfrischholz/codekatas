package designpatterns

import scala.util.Try

/**
  * @author Freshwood
  * @since 22.03.2018
  *        https://gist.github.com/jdegoes/97459c0045f373f4eaf126998d8f65dc
  *        Samples which correlates to a basic functional usage
  */
object MasteringFunctions extends App {

  // Square function just a simple function
  val square: Int => Int = x => x * x

  println(square(2)) //4

  // High Order functions
  // A higher-order function is a function that accepts or returns a function.
  trait TestList[+A] {
    def filter(f: A => Boolean): List[A]
  }

  case class MyList[+A](list: List[A]) extends TestList[A] {
    override def filter(f: A => Boolean): List[A] = list.filter(f)
  }

  val test = MyList(List(1, 2, 3, 4, 5)).filter(_ > 3)

  println(test) // 4,5

  // Combinators
  // Function combinators are higher-order functions that accept and return functions.
  type Conf[A] = String => A

  def string(name: String): Conf[String] = _ + name

  def both(left: Conf[String], right: Conf[String]): Conf[(String, String)] =
    c => (left(c), right(c))

  println(both(identity, s => s)("Woot"))

  // Polymorphic Functions
  // Example: This emulates a polymorphic function called id,
  // which accepts one type parameter A, and a value of type A, and returns that same value.
  case object id {
    def apply[A](x: A): A = x
  }

  println(id(1D))
  println(id("F"))
}

object MasteringTypes extends App {
  // Product Type
  // Product types are defined by a Cartesian cross product on 2 or more types.
  type Point2D = (Int, Int)

  // Case Classes
  // In Scala, case classes are the idiomatic representation of product types.
  // The terms of a case class are identified by name.
  case class Person(name: String, age: Int)

  // Sum Types
  // Sum types are defined by a disjoint union on 2 or more types.
  //type RequestResult = Either[Error, HttpResponse]

  // Sealed Traits
  // In Scala, sealed traits are the idiomatic representation of sum types (pre-Dotty).
  // The terms of a sum type are identified by constructor / deconstructor (and, incidentally, by subtype).
  sealed trait AddressType

  case object Home extends AddressType

  case object Business extends AddressType

  // Example: An AddressType is either a Home or a Business, but not both.

  // Type Constructors
  // A type constructor is a universally quantified type, which can be used to construct types.
  sealed trait Liste[A]

  case class Nil[A]() extends Liste[A]

  case class Cons[A](head: A, tail: Liste[A]) extends Liste[A]

  // Higher-Kinded Types
  // Type-Level Functions
  // Example: List is a type-level function that accepts one type A (the type of its elements),
  // and returns another type List[A]. If you pass Boolean to List, you get back List[Boolean],
  // the type of lists of boolean values.

  // Skolemization
  // Every existential type can be encoded as a universal type. This process is called skolemization.
  case class ListMap[B, A](list: List[B], mapf: B => A)

  trait ListMapInspector[A, Z] {
    def apply[B](value: ListMap[B, A]): Z
  }

  case class AnyListMap[A]() {
    def apply[Z](value: ListMapInspector[A, Z]): Z = ???
  }

  // Example: Instead of using ListMap directly, we use AnyListMap,
  // which allows us to inspect a ListMap but only if we can handle any type parameter for B.
  println(AnyListMap())

  // Type Lambdas
  // Type lambdas are to type constructors as lambdas are to functions. Type constructors and functions are declarations,
  // while lambdas are expressions (either value expressions, or type expressions).
  type λ[α] = ({ type λ[α] = Either[String, α] })#λ[α]

  val lambda: λ[String] = Left("Hallo")

  println(lambda)
}

/**
  * A type class is a bundle of types and operations defined on them.
  * Most type classes have laws that implementations are required to satisfy.
  */
object MasteringTypeClasses extends App {

  trait ShowRead[A] {
    def show(v: A): String

    def read(v: String): Either[String, A]

    def rightIdentity(v: A): Boolean = read(show(v)) == Right(v)

    def leftIdentity(v: String): Boolean =
      read(v).map(show).fold(_ => true, _ == v)
  }

  object ShowRead {
    def apply[A](implicit v: ShowRead[A]): ShowRead[A] = v
  }

  // Example: The ShowRead[A] type class defines a way of "showing" a type A by rendering it to a string,
  // and reading it by parsing it from a string (or producing an error message).
  implicit val showReadString: ShowRead[String] = new ShowRead[String] {
    override def show(v: String): String = v

    override def read(v: String): Either[String, String] = Right(v)
  }

  println(showReadString.rightIdentity("Hallo"))

  implicit class ShowOps[A: ShowRead](self: A) {
    def show: String = ShowRead[A].show(self)
  }

  implicit class ReadOps(self: String) {
    def read[A: ShowRead]: Either[String, A] = ShowRead[A].read(self)
  }

  println("Fucker".show.read[String]) // Right("Fucker")

}

object MasteringFunctionalPatterns extends App {

//  Option, Either, Validation
//  These types are commonly used to describe optionality and partiality.

  // Option
  sealed trait Maybe[+A]
  case class Just[A](value: A) extends Maybe[A]
  case object Empty extends Maybe[Nothing]

  // Either
  sealed trait \/[A, B]
  final case class \[A, B](value: A) extends \/[A, B]
  final case class /[A, B](value: B) extends \/[A, B]
  type OwnEither[A, B] = Either[A, B]

  // Validation
  sealed trait Validation[A, B]
  final case class Failure[A, B](value: A) extends Validation[A, B]
  final case class Success[A, B](value: B) extends Validation[A, B]
  type OwnValidation[A] = Try[A]

//  Semigroup, Monoid
//  Semigroups allows combining two things of the same type into another thing of the same type.
//  For example, addition forms a semigroup over integers.
//  Monoids add the additional property of having an "zero" element,
//  which you can append to a value without changing the value.

  trait Semigroup[A] {
    def append(a1: A, a2: A): A
    def associativity(a1: A, a2: A, a3: A): Boolean =
      append(a1, append(a2, a3)) == append(append(a1, a2), a3)
  }

  trait Monoid[A] extends Semigroup[A] {
    def zero: A
    def identity(a: A): Boolean = append(a, zero) == a
  }

  // Test
  val intMonoid: Monoid[Int] = new Monoid[Int] {
    override def zero: Int = 0

    override def append(a1: Int, a2: Int): Int = a1 + a2
  }

  println(intMonoid.identity(150))
  println(intMonoid.associativity(3, 4, 80))

}

object SandboxSamples extends App {

  val eitherTest: Either[String, Int] = Right(100)

  //val test = eitherTest.toTry -> would not compile evidence is failing (A should be a throwable)
  val test = eitherTest.toOption

  println(test)

  /* Simple functions test */

  val fn: (Int, Int) => Int = _ + _

  val tupled: ((Int, Int)) => Int = fn.tupled

  val curried: Int => Int => Int = fn.curried

  println(tupled(1 -> 3))

  println(curried(1)(3))

  val sequence: Seq[Int] = 1 to 5

  val liftedSequence: Int => Option[Int] = sequence.lift

  println(liftedSequence(5))

}
