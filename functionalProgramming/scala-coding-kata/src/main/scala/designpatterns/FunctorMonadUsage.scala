package designpatterns

import scala.language.higherKinds

/**
  * @author Freshwood
  * @since 04.09.2017
  * This sample shows a example of the functor usage :)
  */
object FunctorMonadUsage extends App {}

// Protocol
trait DataType extends Any {
  def value: String
}
case class IntDataType(value: String) extends AnyVal with DataType

case class DefaultGenerator(pomDataType: DataType) {
  def generate[A <: DataType](`type`: A): Int = `type` match {
    case IntDataType(_) => 100
  }
}
