package designpatterns

import scala.language.higherKinds

/**
  * @author Freshwood
  * @since 05.09.2017
  * http://blog.krobinson.me/posts/explaining-monads#monads
  * Great article about this monad shit
  */
object Endofunctors extends App {}

sealed trait Monoid[A] {
  def identity: A

  def append(a: A, b: A): A
}

sealed trait Functor[F[_]] {
  def map[A, B](fa: F[A])(fn: A => B): F[B]
}

sealed trait Monad[M[_]] extends Functor[M] /* with Monoid[_ => M[_]] */ {
  def pure[A](a: A): M[A]

  def flatMap[A, B](a: M[A])(fn: A => M[B]): M[B]

  override def map[A, B](fa: M[A])(fn: (A) => B): M[B] =
    flatMap(fa)(f => pure(fn(f)))

  /**
    * From Monoid
    */
  def identity[A]: A => M[A] = a => pure(a)

  /**
    * From Monoid
    */
  def append[A, B, C](f1: A => M[B], f2: B => M[C]): A => M[C] =
    a => flatMap(f1(a))(f2)

}

// How to be free of evaluation e.g. Free Monad --->
sealed trait Free[F[_], A] { self =>
  def flatMap[B](fn: A => Free[F, B]): Free[F, B] =
    FlatMap(self, fn)

  def pure[T](a: T): Free[F, T] = Return(a)

  def map[B](fn: A => B): Free[F, B] =
    flatMap(a => pure(fn(a)))
}

case class Return[F[_], A](given: A) extends Free[F, A]
case class Suspend[F[_], A](fn: F[A]) extends Free[F, A]
case class FlatMap[F[_], A, B](free: Free[F, A], fn: A => Free[F, B])
    extends Free[F, B]
