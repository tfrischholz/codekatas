package testing

/**
  * This class is used as an example for the TDD driven tests
  * We just create an immutable stack object to test it with TDD style!
  */

trait TddStackDefinition[+A] {
  def push[B >: A](element: B): TddStackDefinition[B]

  def pop: TddStackDefinition[A]
}


case class TddStack[A](elements: Seq[A]) extends TddStackDefinition[A] {
  override def push[B >: A](element: B): TddStack[B] = TddStack(element +: elements)

  override def pop: TddStack[A] = TddStack(elements.tail)

  def all: Seq[A] = elements
}
