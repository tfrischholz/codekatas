package testing

/**
  * @author Freshwood
  * @since 29.07.2018
  */
sealed trait TestDrivenStackWrapper[S] {

  /**
    * Pop a single element
    * if stack is empty an exception will be thrown
    */
  def pop: S

  /**
    * Pushed a new element to the stack
    * Returns a new instance
    */
  def push(element: S): TestDrivenStackWrapper[S]

  /**
    * Pop a single element without effecting the stack
    * if stack is empty an exception will be thrown
    */
  def peek: S
}

case class TestDrivenStack[S](var elements: Seq[S] = Seq.empty) extends TestDrivenStackWrapper[S] {

  /**
    * Pop a single element
    * if stack is empty an exception will be thrown
    */
  override def pop: S = {
    val retVal = elements.head
    elements = elements.filter(_ != retVal)
    retVal
  }

  /**
    * Pushed a new element to the stack
    * Returns a new instance
    */
  override def push(element: S): TestDrivenStack[S] = TestDrivenStack(element +: elements)

  /**
    * Pop a single element without effecting the stack
    * if stack is empty an exception will be thrown
    */
  override def peek: S = elements.head
}
