package exercism.series

import scala.language.postfixOps
import scala.util.Try

/**
  * @author Freshwood
  * @since 19.07.2018
  *        Given a string of digits, calculate the largest product for a contiguous substring of digits of length n.
  *
  *        For example, for the input '1027839564', the largest product for a series of 3 digits is 270 (9 * 5 * 6), and the largest product for a series of 5 digits is 7560 (7 * 8 * 3 * 9 * 5).
  *
  *        Note that these series are only required to occupy adjacent positions in the input; the digits need not be numerically consecutive.
  *
  *        For the input '73167176531330624919225119674426574742355349194934', the largest product for a series of 6 digits is 23520.
  */
object Series extends App {
  def largestProduct(range: Int, digits: String): Option[Int] =
    Try(digits.map(_.toString.toInt)) map {
      case _ if range == 0          => 1
      case xs if range <= xs.length => xs.sliding(range).map(_.product).max
    } toOption
}
