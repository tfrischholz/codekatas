package exercism.wordwrap

/**
	* This is a simple implementation of the word wrap dojo
	* In this example you have to only implement a function with the following features:
	* Wrap a given String on the given maximum number of line size.
	*/
object DojoWordWrap extends App {

	lazy val maximum_line_number = 25

	lazy val space_width = 1

	val sampleText = "Hallo das ist ein super geiler Sample Text. Der Text soll extrem...extrem....extrem... und super lang sein!!!! Oh yeah"

	 def wordWrapText(text: String, maximumLineLength: Int) = {
		 val words = text.split(" ")

		 var spaceLeft = maximumLineLength

		 def splitWord(sibling: String) = {
			 if ((sibling.length + space_width) > spaceLeft) {
				 print("\n" + sibling + " ")
				 spaceLeft = maximumLineLength - sibling.length
			 } else {
				 print(sibling + " ")
				 spaceLeft -= sibling.length + space_width
			 }
		 }

		 def splitTooLongWord(word: String): Unit = {
			 val tokens = word.splitAt(maximumLineLength)

			 if (tokens._1.length > maximumLineLength) splitTooLongWord(tokens._1) else splitWord(tokens._1)
			 if (tokens._2.length > maximumLineLength) splitTooLongWord(tokens._2) else splitWord(tokens._2)
		 }

		 for (w <- words) if (w.length > maximumLineLength) splitTooLongWord(w) else splitWord(w)
	 }

	wordWrapText(sampleText, maximum_line_number)

}
