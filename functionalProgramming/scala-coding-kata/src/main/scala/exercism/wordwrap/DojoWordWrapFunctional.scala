package exercism.wordwrap

/**
	* This is a sample implementation of the word wrap dojo
	* In this example you have to only implement a function with the following features:
	* Wrap a given String on the given maximum number of line size.
	*/
object DojoWordWrapFunctional extends App {

	type B = String => Boolean

	type U = String => Unit

	lazy val maximum_line_number = 5

	lazy val space_string = " "

	lazy val space_width = space_string.length

	val sampleText = "Hallo das ist ein super geiler Sample Text. Der Text soll extrem...extrem....extrem... und super lang sein!!!! Oh yeah"

	 def wordWrapText(text: String, maximumLineLength: Int) = {

		 lazy val wordFlow = (word: String, f: B, tooLong: U, split: U) => if (f(word)) tooLong(word) else split(word)

		 val words = text.split(space_string)

		 var spaceLeft = maximumLineLength

		 def splitWord(sibling: String) = {
			 if ((sibling.length + space_width) > spaceLeft) {
				 print("\n" + sibling + space_string)
				 spaceLeft = maximumLineLength - sibling.length
			 } else {
				 print(sibling + space_string)
				 spaceLeft -= sibling.length + space_width
			 }
		 }

		 def splitTooLongWord(word: String): Unit = {
			 word.splitAt(maximumLineLength).productIterator.foreach {
				 f => wordFlow(f.toString, w => w.length > maximumLineLength, splitTooLongWord, splitWord)
			 }
		 }

		 for (w <- words) wordFlow(w, w => w.length > maximumLineLength, splitTooLongWord, splitWord)
	 }

	wordWrapText(sampleText, maximum_line_number)
}
