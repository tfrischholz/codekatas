package exercism.lotto

import scala.collection.SortedSet
import scala.util.Random

/**
	* Write a program which is simulating the lotto
	* We need 6 numbers from 1 to 49
	* Make sure the result in console is sorted ASC
	*/
object LottoDojo extends App {

  private type A = SortedSet[Int]

  private type B = Set[Int] => Int

  private final val maximalLottoNumbers = 6

  private final val lottoNumbers = (1 to 49).toSet

  private val randomLottoNumber: B = numbers => Random.nextInt(numbers.size) + 1

  def lotto(x: A = SortedSet[Int]()): A = x match {
    case y if y.size >= lottoNumbers.size => x
    case y if y.size < maximalLottoNumbers => lotto(y + randomLottoNumber(lottoNumbers))
    case y if y.size >= maximalLottoNumbers => y
    case _ => x
  }

  println(lotto())
}
