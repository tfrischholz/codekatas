package exercism

/**
  * @author Freshwood
  * @since 09.10.2017
  * 1 -> „I“
    2 -> „II“
    4 -> „IV“
    5 -> „V“
    9 -> „IX“
    10 -> „X“
    42 -> „XLII“
    99 -> „XCIX“
    2013 -> „MMXIII“
  */
object RomanNumeralKata extends App {

  def toRoman(input: Int): String = {
    var remainingNumber = input
    RomanUnit.romanNumerals.foldLeft("") { (out, unit) =>
      {
        val times = remainingNumber / unit.value
        remainingNumber -= unit.value * times
        out + (unit.key * times)
      }
    }
  }

  println(toRoman(1653))

}

sealed case class RomanUnit(value: Int, key: String)

object RomanUnit {

  val romanNumerals = List(
    RomanUnit(1000, "M"),
    RomanUnit(900, "CM"),
    RomanUnit(500, "D"),
    RomanUnit(400, "CD"),
    RomanUnit(100, "C"),
    RomanUnit(90, "XC"),
    RomanUnit(50, "L"),
    RomanUnit(40, "XL"),
    RomanUnit(10, "X"),
    RomanUnit(9, "IX"),
    RomanUnit(5, "V"),
    RomanUnit(4, "IV"),
    RomanUnit(1, "I")
  )
}

object NumeralRomanKata extends App {

  // Here is a another version that does a simple running sum:
  def fromRoman(s: String): Int = {
    val numerals = Map('I' -> 1,
                       'V' -> 5,
                       'X' -> 10,
                       'L' -> 50,
                       'C' -> 100,
                       'D' -> 500,
                       'M' -> 1000)

    s.toUpperCase
      .map(numerals)
      .foldLeft((0, 0)) {
        case ((sum, last), curr) =>
          val sum2 = sum + curr
          val number = if (last < curr) -2 * last else 0

          (sum2 + number, curr)
      }
      ._1
  }

// A small test
  def test(roman: String) = println(roman + " => " + fromRoman(roman))

  test("MCMXC")
  test("MMVIII")
  test("MDCLXVI")
}
