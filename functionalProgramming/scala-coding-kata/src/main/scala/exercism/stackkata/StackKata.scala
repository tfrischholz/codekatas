package exercism.stackkata

/**
  * Created by Freshwood on 05.03.2017.
  * Showcase for a mutable stack
  */
object MutableStackKata extends App {

  trait TestStack[A] {

    var dataHolder: List[A]

    def pop: A

    def push(element: A): TestStack[A]
  }

  class MutableStack[S](var dataHolder: List[S] = List.empty) extends TestStack[S] {

    override def pop: S = {
      val retVal = dataHolder.head
      dataHolder = dataHolder.tail
      retVal
    }

    override def push(element: S): TestStack[S] = {
      dataHolder = element :: dataHolder
      this
    }
  }

  val ownStack: TestStack[String] = new MutableStack[String](List("Hallo", "Welt"))

  println(ownStack.pop)
  println(ownStack.pop)
}

object ImmutableStackKata extends App {

  val ownStack: TestStack[String] = new ImmutableStack[String](List("Hallo", "Welt"))

  val firstStack = ownStack.pop

  println(ownStack)
  println(firstStack)


  trait TestStack[+A] {

    def pop: TestStack[A]

    def push[B >: A](element: B): TestStack[B]
  }

  class ImmutableStack[S](val elements: List[S]) extends TestStack[S] {
    override def pop: TestStack[S] = new ImmutableStack[S](elements.tail)

    override def push[B >: S](element: B): TestStack[B] = new ImmutableStack[B](element :: elements)

    override def toString: String = elements.mkString("Stack(", ", ", ")")
  }

}


