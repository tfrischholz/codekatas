package exercism.change

import scala.collection.immutable
import scala.language.postfixOps

/**
  * @author Freshwood
  * @since 18.07.2018
  *        Correctly determine the fewest number of coins to be given to a customer such that the sum of the coins' value would equal the correct amount of change.
  *
  *        For example
  *        An input of 15 with [1, 5, 10, 25, 100] should return one nickel (5) and one dime (10) or [0, 1, 1, 0, 0]
  *        An input of 40 with [1, 5, 10, 25, 100] should return one nickel (5) and one dime (10) and one quarter (25) or [0, 1, 1, 1, 0]
  *        Edge cases
  *        Does your algorithm work for any given set of coins?
  *        Can you ask for negative change?
  *        Can you ask for a change value smaller than the smallest coin value?
  */
object Change extends App {

  def findFewestCoins(amount: Int, coins: Seq[Int]): Option[Seq[Int]] = {
    findFewestCoins2(amount, coins.sorted(Ordering.Int.reverse)).map(_.sorted)
  }

  private def findFewestCoins2(amount: Int, coins: Seq[Int]): Option[Seq[Int]] = {
    if (amount == 0) Some(Nil)
    else if (coins.isEmpty || amount < 0) None
    else {
      val coin = coins.head
      val options = ((amount / coin) to 1 by -1).toStream

      val validResults = options.flatMap { count =>
        spendCoins(amount, coin, count, coins.tail)
      }.headOption
      val validResultsWithoutThisCoin = findFewestCoins2(amount, coins.tail).toSeq

      (validResults.toSeq ++ validResultsWithoutThisCoin)
        .sortBy(_.length)
        .headOption
    }
  }

  private def spendCoins(amount: Int, coin: Int, count: Int, rest: Seq[Int]) = {
    val spent: immutable.Seq[Int] = (1 to count).map(_ => coin)
    findFewestCoins2(amount - (count * coin), rest).map(rest => spent ++ rest)
  }
}
