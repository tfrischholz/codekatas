package exercism.random

import scala.language.postfixOps
import scala.util.Random

/**
  * @author Freshwood
  * @since 31.12.2017
  * Self created kata
  * Just write an app which should generate random numbers
  * Make sure that the previous number is not the next number
  */
object RandomNumberGenerator extends App {

  val howMuchNumbers: Int = 100

  // When you run this you will se there some times numbers which comes two or more times between the number
  // e.g.: 2,98,78,44,44,12 -> 44 is not good ;)
  lazy val testFailure: Unit = (1 to howMuchNumbers).toList foreach (_ =>
    println(Random.nextInt(100)))

  // Now, the same test with the immutable object variant
  val randomizer: Randomizer.RandomNumbers =
    Randomizer.RandomNumbers().generate(100).generate(500).generate(250)

  println(randomizer)
}

object Randomizer {

  case class RandomNumbers(numbers: List[Int] = List.empty) { self =>
    def generate(seed: Int): RandomNumbers =
      Random.nextInt(seed) match {
        case _ if numbers.lengthCompare(seed) >= 0 =>
          throw new IllegalArgumentException("Seed is bigger then the numbers")
        case random if numbers.contains(random) => self.generate(seed)
        case random                             => RandomNumbers(numbers :+ random)
      }

    def last: Int = numbers.lastOption.getOrElse(0)
  }
}
