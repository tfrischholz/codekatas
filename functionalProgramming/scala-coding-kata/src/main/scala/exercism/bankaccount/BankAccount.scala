package exercism.bankaccount

/**
	*
	* Bank accounts can be accessed in different ways at the same time.
	* A bank account can be accessed in multiple ways. Clients can make deposits and withdrawals using the internet, mobile phones, etc. Shops can charge against the account.
	* Create an account that can be accessed from multiple threads/processes (terminology depends on your programming language).
	* It should be possible to close an account; operations against a closed account must fail.
	*/
trait BankAccount {

	def closeAccount(): Unit

	def getBalance: Option[Int]

	def incrementBalance(increment: Int): Option[Int]
}

protected case class Account(var balance: Option[Int] = Some(0)) extends BankAccount {

	private def runThreadSafe[A](block: => A): A = this.synchronized(block)

	override def closeAccount(): Unit = runThreadSafe(balance = None)

	override def getBalance: Option[Int] = runThreadSafe(balance)

	override def incrementBalance(increment: Int): Option[Int] = runThreadSafe {
		balance flatMap { amount =>
			balance = Some(amount + increment)
			balance
		}
	}
}

object BankAccount {
	def apply(): BankAccount = Account()
}
