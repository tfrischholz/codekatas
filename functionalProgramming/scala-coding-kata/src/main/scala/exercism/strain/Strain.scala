package exercism.strain

/**
  * @author Freshwood
  * @since 19.11.2018
  * Implement the keep and discard operation on collections. Given a collection and a predicate on the collection's elements, keep returns a new collection containing those elements where the predicate is true, while discard returns a new collection containing those elements where the predicate is false.
  *
  * For example, given the collection of numbers:
  *
  * 1, 2, 3, 4, 5
  * And the predicate:
  *
  * is the number even?
  * Then your keep operation should produce:
  *
  * 2, 4
  * While your discard operation should produce:
  *
  * 1, 3, 5
  * Note that the union of keep and discard is all the elements.
  *
  * The functions may be called keep and discard, or they may need different names in order to not clash with existing functions or concepts in your language.
  *
  *
  */
object Strain extends App {

  def keep[A](xs: Seq[A], predicate: A => Boolean): Seq[A] = xs match {
    case Nil                       => xs
    case x :: Nil if predicate(x)  => xs
    case x :: Nil if !predicate(x) => List.empty
    case x :: y if predicate(x)    => x +: keep(y, predicate)
    case x :: y if !predicate(x)   => keep(y, predicate)
  }

  def discard[A](xs: Seq[A], predicate: A => Boolean): Seq[A] = xs match {
    case Nil                       => xs
    case x :: Nil if !predicate(x) => xs
    case x :: Nil if predicate(x)  => List.empty
    case x :: y if !predicate(x)   => x +: discard(y, predicate)
    case x :: y if predicate(x)    => discard(y, predicate)
  }

  val seq: Seq[Int] = Seq(1, 2, 3, 4, 5)

  val result: Seq[Int] = discard[Int](seq, element => element % 2 == 0)

  println(result)

}
