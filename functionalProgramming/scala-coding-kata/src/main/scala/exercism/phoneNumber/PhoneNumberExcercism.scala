package exercism

//Phone Number
//Write a program that cleans up user-entered phone numbers so that they can be sent SMS messages.
//
//The rules are as follows:
//
//If the phone number is less than 10 digits assume that it is bad number
//If the phone number is 10 digits assume that it is good
//If the phone number is 11 digits and the first number is 1, trim the 1 and use the last 10 digits
//If the phone number is 11 digits and the first number is not 1, then it is a bad number
//If the phone number is more than 11 digits assume that it is a bad number
//We've provided tests, now make them pass.
//
//Hint: Only make one test pass at a time. Disable the others, then flip each on in turn after you get the current failing one to pass.

object PhoneNumberExcercism extends App {

  class PhoneNumber(phoneNumber: String) {

	  val hasValidLength = (x: Int) => x > 9 && x < 11

	  val areaCode = phoneNumber.substring(3)

	  def number: String = phoneNumber match {
		  case x => if (hasValidLength(x.length)) x else "0000000000"
	  }

  }

  val app = new PhoneNumber("(123) 456-7890")

	println(app.number)
}
