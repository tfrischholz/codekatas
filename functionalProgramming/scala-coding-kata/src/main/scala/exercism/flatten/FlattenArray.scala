package exercism.flatten

/**
  * @author Freshwood
  * @since 18.07.2018
  *
  *        Take a nested list and return a single flattened list with all values except nil/null.
  *
  *        The challenge is to write a function that accepts an arbitrarily-deep nested list-like structure and returns a flattened structure without any nil/null values.
  *
  *        For Example
  *
  *        input: [1,[2,3,null,4],[null],5]
  *
  *        output: [1,2,3,4,5]
  */
object FlattenArray extends App {

  def flatten(list: List[Any]): List[Any] = list match {
    case (x :: y) :: z       => flatten(x :: y) ::: flatten(z)
    case x :: y if x != null => x :: flatten(y)
    case _                   => List.empty
  }

  def flattenAlternative(list: List[Any]): List[Int] = list flatMap {
    case int: Int        => List(int)
    case list: List[Any] => flattenAlternative(list)
    case _               => List.empty
  }
}
