package functional

/**
  * Created by Freshwood on 01.06.2016.
  */
object functional15PatternMatching extends App {

  (1 until 100).map(i => (i % 3, i % 5) match {
    case (0, 0) => "FizzBuzz"
    case (0, _) => "Fizz"
    case (_, 0) => "Buzz"
    case _ => i
  }).foreach(println)

  // Further examples

  def matcher(x: Int): String = x match {
    case 5 => "Fünf"
    case _ => "I don't know that"
  }

  println(matcher(50))

  case class PatternMatch(x: Int) {

    // This is mapping on the object itself
    def matcher(x: Int): String = this match {
      case y if y.x == 10 => "Hallo"
      case _ => "Nothing special"
    }
  }

  val test = PatternMatch(11)

  println(test.matcher(10))



}
