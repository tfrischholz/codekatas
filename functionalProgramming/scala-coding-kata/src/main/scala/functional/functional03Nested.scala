package functional

/**
  * This demonstrates the power of nested functions
  * Example: FizzBuzz
  */
object functional03Nested extends App {

  /**
    * As Example we take the FizzBuzz Example
    * Display Fizz when the number is dividable through 3
    * Display Fizz when the number is dividable through 5
    * Display FizzBuzz when the number is dividable through 3 and 5
    */
  def fizzBuzzExample() = {

    val startValue = 100

    def isFizz(x: Int): Boolean = x % 3 == 0

    def isBuzz(x: Int): Boolean = x % 5 == 0

    def fizzBuzzString(x: Int): String = {

      if (isFizz(x) && isBuzz(x)) {
        return "FizzBuzz"
      }

      if (isFizz(x)) return "Fizz"

      if (isBuzz(x)) return "Buzz"

      x.toString
    }

    for (i <- 1 to startValue) {

      println(fizzBuzzString(i))

    }
  }

  fizzBuzzExample()

}
