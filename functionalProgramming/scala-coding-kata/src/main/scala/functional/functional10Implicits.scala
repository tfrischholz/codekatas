package functional

/**
  * Created by Freshwood on 01.06.2016.
  */
object functional10Implicits extends App {

  implicit val multiplier = 2

  def multiply(implicit by: Int) = 10 * by

  println(multiply)
}
