package functional

/**
	* Created by Freshwood on 12.09.2016.
	*/
object functional18TypeAliases extends App {

	case class User(id: Long)
	case class Product(id: Long)

	//code from Eric's article
	type Tagged[U] = { type Tag = U }
	type @@[T, U] = T with Tagged[U]

	trait Design
	trait Usability
	trait Durability

	type DesignScore = Int @@ Design
	type UsabilityScore = Int @@ Usability
	type DurabilityScore = Int @@ Durability

	case class Scoring(user: User, product: Product,
	                   design: DesignScore,
	                   usability: UsabilityScore,
	                   durability: DurabilityScore)

	implicit class TaggedInt(val i: Int) extends AnyVal {
		def design = i.asInstanceOf[DesignScore]
		def usability = i.asInstanceOf[UsabilityScore]
		def durability = i.asInstanceOf[DurabilityScore]
	}

	val test = 10.design

	println(test)

	def requireTest(x: String) = {
		require(x.length > 10, "The given string is too short")
		x
	}

	println(requireTest("Hallo ich bin so cool"))

}
