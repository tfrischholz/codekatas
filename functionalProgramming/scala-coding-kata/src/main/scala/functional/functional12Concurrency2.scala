package functional

import akka.actor.ActorSystem

import scala.concurrent.{ExecutionContext, Future}

/**
	* Created by Freshwood on 12.09.2016.
	*/
object functional12Concurrency2 extends App {

	val system = ActorSystem("Actor-System")

	implicit val context: ExecutionContext = system.dispatcher

	def futureProviderA = Future {
		100
	}

	def futureProviderB = Future {
		" Hallo Welt"
	}

	def futureProviderSum = Future {
		futureProviderA.→(futureProviderB)
	}

	def test = {
		for {
			(t1, t2) <- futureProviderSum
			inty <- t1
			stringy <- t2
		} yield inty + stringy
	}

	println(test)

	system.terminate()
}
