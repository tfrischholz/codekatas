package functional

import java.io.{File, FileReader, PrintWriter}

/**
  * Created by Freshwood on 12.05.2016.
  */
object FunctionalControlStructure extends App{

  def withPrintWriter(file: File)(op: PrintWriter => Unit): PrintWriter => Unit = {

    val writer = new PrintWriter(file)

    try {
      op(writer)
      op
    } finally {
       writer.close()
    }
  }

  def withFileReader(file: File, op: FileReader => Int) = {
    val reader = new FileReader(file)

    try {
      println(op(reader))
    } finally {
      reader.close()
    }
  }

  val result = withPrintWriter(new File("date.txt")){
    f => f.println(new java.util.Date())
  }

  println(result)

  withFileReader(new File("date.txt"), reader => reader.read())

  /**
    * Another Example here
    * First we are generating some names
    * Then we have a high order function which is proofing for names
    * The function itself get a function which is doing the check
    */

  val rsTeam = List("Peter", "Tobias", "Christian", "Boris", "Marc", "Thomas")

  val proofTeamMember = (x: String, y: String) => x == "Fuck" || y == "Thomas"

  private def proofing(string: String, optional: String) = {
    string == "Fuck" || optional == "Thomas"
  }

  private def isTeamMember(listOfName: List[String], proofNames: (String, String) => Boolean): Boolean = {
    val newTeamMember = "Thomas"
    listOfName.exists(element => proofNames(element, newTeamMember))
  }

  println(isTeamMember(rsTeam, proofing))
}
