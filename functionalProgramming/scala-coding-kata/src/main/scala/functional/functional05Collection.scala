package functional

/**
  * All about scala collections!
  */
object functional05Collection extends App {

  val fiveList = 1 to 5

  val concatList = 6 :: 7 :: 8 :: 9 :: 10 :: Nil

  val list = fiveList ++ concatList

  val newList = 0 +: list :+ 11

  println(list)

  println(newList)

  // WATCH OUT FOR RIGHT ASSOCIATED FUNCTIONS!!!
  // Add number 100 at the beginning of a list
  // val test = concatList +: 100 => does not compile why?
  // The compiler does this => val test = 100 +: concatList but 100 is not LIST!!!
  // Every operator with ends with an ':' is right associated
  // Right way =>
  val test = 100 +: concatList

  println(test)

  println(concatList)

  // Operations

  val bigList = List(19) ::: concatList ::: fiveList.toList

  println(bigList)

  // How to sort a list??? Don`t use sortBy on "primitive" Types ->
  // sortBy is used for attributes like users age!
}
