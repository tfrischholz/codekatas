package functional

/**
  * All about Tuple => Info: It is possible to create a max Tuple with 22 params
  * Maps are always Tuple!
  */
object functional06Tuple extends App {

  val tuple2 = (1 -> "Eins", 2 -> "Zwei", 3 -> "Drei")

  println(tuple2)

  // Function as tuple
  def squareAndCubic(x: Int): (Int, Int) = {
    val squareMeter = x * x
    val cubicMeter = x * x * x
    (squareMeter, cubicMeter)
  }

  println(squareAndCubic(10))

  val map = Map(1 -> "Eins", 2 -> "Zwei", 3 -> "Drei")

  println(map)

  // Todo: Add more Map tests

  // Task: Make a list of the alphabet from a - z and add an incremental number to each char to
  // the list, so that we have a map at the end

  val alphabet = 'a' to 'z'

  val alphabetWithNumbers = alphabet.zip(1 to 100)

  println(alphabetWithNumbers)
}
