package functional

/**
  * Learn how scala types are working
  */
object functional01Types extends App {

  /**
    * Traits are some kind of interfaces
    * It is also possible to pre implement some stuff
    */
  trait IDisplayName {

    def displayFullName()

    def displayMagicNumber() = println(10)
  }

  /**
    * This is what every OO language does!
    */
  class MutableClass(lastName: String) extends IDisplayName{

    var name = "Franz"

    setFullName()

    private def setFullName(): Unit = {
      name += " " + lastName
    }

    def displayFullName() = {
      println(name)
    }
  }

  /**
    * This is the functional way!
    * The class is immutable and can not be changed!!!
    */
  class ImmutableClass(lastName: String) extends IDisplayName{

    val name = "Franz"

    val fullName = name + " " + lastName

    def displayFullName() = {
      println(fullName)
    }

    /**
      * Yep it is also possible to override pre implemented stuff
      */
    override def displayMagicNumber() = println(100)
  }

  val mutable = new MutableClass("Schnuffel")

  mutable.displayFullName()

  val immutable = new ImmutableClass("Schnuffel")

  immutable.displayFullName()

  /**
    * Wtf, yep that's real type inference!
    */
  mutable.displayMagicNumber()

  immutable.displayMagicNumber()
}
