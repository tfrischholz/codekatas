package functional

import akka.actor.ActorSystem

import scala.concurrent.{ExecutionContext, Future}

/**
	* Created by Freshwood on 12.09.2016.
	*/
object functional18TypeAliases2 extends App {

	val system = ActorSystem("Actor-System")

	implicit val context: ExecutionContext = system.dispatcher


	abstract class TestTypes[T] {

		type A = Int

		type S = String

		def futureProviderA: Future[A] = Future {
			100
		}

		def futureProviderB: Future[S] = Future {
			" Hallo Welt"
		}

		def futureProviderSum: Future[S] = {
			futureProviderA flatMap { int =>
				futureProviderB map { string =>
					int + string
				}
			}
		}

		def retTest: T
	}

	class TestTypes2 extends TestTypes[Int] {
		override def retTest: Int = {
			1000
		}
	}

	val test = new TestTypes2

	println(test.futureProviderSum)

	println(test.retTest)

	system.terminate()
}
