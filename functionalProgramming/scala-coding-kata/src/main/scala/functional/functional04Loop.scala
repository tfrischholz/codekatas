package functional

/**
  * All about looping through the code
  */
object functional04Loop extends App {

  /**
    * Create a sample list to test the looping here
    */
  val sampleList: List[Int] = (1 to 10).toList

  val sampleList2: List[Int] = (1 to 10).toList

  val sampleList3: List[Int] = (1 to 3).toList
  /**
    * Scala's foreach loop
    */
  for(element <- sampleList) {
    println(element)
  }

  val enhancedLoop = for {
    e <- sampleList
    if (e % 2) == 0 || e == 1
  } yield e -> e

  println("Enhanced Loop " + enhancedLoop)

  /**
    * Duplicates the content of the given list
    * @param list The list to duplicate the content
    * @return A duplicated list
    */
  def duplicateList(list: List[Int]): List[Int] = {
    for (x <- list) yield x * 2
  }

  /**
    * Get all even numbers
    * @param list The list to proof
    * @return A list with even numbers
    */
  def evenNumbersFromList(list: List[Int]): List[Int] = {
    for (x <- list if x % 2 == 0) yield x
  }

  println(duplicateList(sampleList))

  println(evenNumbersFromList(sampleList))

  val result = for {
    v3 <- sampleList3
    v2 <- sampleList2
    v1 <- sampleList
  } yield v1 -> v2 -> v3

  val summed = result map { v =>
    v._1._1 + v._1._2 + v._2
  }

  println(result)

  println(summed)
}
