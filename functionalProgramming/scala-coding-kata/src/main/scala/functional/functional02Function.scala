package functional

/**
  * All about scala functions...
  */
object functional02Function extends App {

  /**
    * Remember: Everything is a function/object
    * In a functional language every entity has a return value!
    */
  class ScalaFunctionExample {

    val x = 10

    val test = x * x

    println(test)

    def testFunction() = {
      x * x
    }

    def testFunction(x: Int, y: Int) = {
      x * y
    }

    def testFunctionExtendedParameterList(x: Int)(y: Int) = {
      x * y
    }

    def recursive(x: Int): Int = {
      if (x >= 1) recursive(x - 1) + (x * x) else 0
    }
  }

  val ex = new ScalaFunctionExample

  println(ex.testFunction())

  println(ex.testFunction(5, 5))

  println(ex.testFunctionExtendedParameterList(25)(25))

  println(ex.recursive(5))
}
