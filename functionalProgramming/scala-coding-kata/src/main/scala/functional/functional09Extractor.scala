package functional

/**
  * Extractors
  */
object functional09Extractor extends App {

	trait User {
		def name: String
		def score: Int
	}
	class FreeUser(val name: String, val score: Int, val upgradeProbability: Double) extends User

	class PremiumUser(val name: String, val score: Int) extends User

	class SuperUser(val name: String, val score: Int, isAdmin: Boolean) extends User

	object SuperUser {
		def unapply(arg: SuperUser): Option[(String, Int, Boolean)] = None
	}

	object FreeUser {
		def unapply(user: FreeUser): Option[(String, Int, Double)] =
			if(user.score > 1000) {
				None
			} else {
				Some((user.name, user.score, user.upgradeProbability))
			}
	}
	object PremiumUser {
		def unapply(user: PremiumUser): Option[(String, Int)] = Some((user.name, user.score))
	}

	val user: User = new FreeUser("Daniel", 3000, 0.7d)

	user match {
		case FreeUser(name, _, p) =>
			if (p > 0.75) name + ", what can we do for you today?" else "Hello " + name
			println(p)

		case PremiumUser(name, _) => "Welcome back, dear " + name
			println(name)

		case _ => println("None of them was a user")
	}

	val admin: SuperUser = new SuperUser("King", 10000000, isAdmin = true)

	admin match {
		case SuperUser(n, _, _) => println(n)
		case _ => println("Fuck you bastard")
	}

	/**
		* Also a nice feature is to tag the desired class and make another case class check
		* This only works for case classes or companion objects which have an unapply function
		*/
	val anyPerson = new FreeUser("Hans", 100, 1.0)

	anyPerson match {
		// Normal behaviour
		case FreeUser("Hanser", _, _) => // Look at here we have not the complete object in scope
			print("Nothing here")

		case user @ FreeUser("Hans", _, _) => // Now we can get the complete user class
			println(user.name)

		case x @_ => println(x.score)
	}
}
