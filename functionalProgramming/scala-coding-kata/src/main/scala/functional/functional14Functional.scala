package functional

import scala.beans.BeanProperty

/**
  * Created by Freshwood on 01.06.2016.
  */
object functional14Functional extends App {

  class User {

    @BeanProperty
    var name: String = _
    var orders: List[Order] = Nil
  }

  class Order {
    var id: Int = _
    var products: List[Product] = Nil
  }

  class Product {
    var id: Int = _
    var category: String = _
  }

  val u = new User()

  u.setName("Hallo")

  println(u.getName)
}
