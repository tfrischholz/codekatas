package functional

/**
  * Created by Freshwood on 01.06.2016.
  */
object functional11Variance extends App {

  class InvariantStack[T] {

    var elements: List[T] = Nil

    def push(element: T) = elements = element :: elements

    def top: T = elements.head

    def pop: List[T] = elements.tail

    override def toString = elements.toString()
  }

  val stacky = new InvariantStack[String]

  stacky.push("Welt")

  stacky.push("Hallo")

  println(stacky.toString)
}
