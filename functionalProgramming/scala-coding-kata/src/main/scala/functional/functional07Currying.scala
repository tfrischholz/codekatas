package functional

/**
  * Functional currying examples
  */
object functional07Currying extends App {

  def buildString(x: String, y: String) = {
    x + " " + y
  }

  println(buildString("Hallo", "Welt"))

  def buildCurryString(x: String)(y: String) = {
    x + " " + y
  }

  println(buildCurryString("Hallo")("Welt"))

  // Control flow coding
  println(buildCurryString("Hallo") {
    "Welt"
  })
}
