package functional

/**
  * Created by Freshwood on 01.06.2016.
  */
object functional13Closures extends App {

  var minAge = 18

  val isAdult = ( age :Int ) => age >= minAge

  val isGermanAdult = isAdult(20)

  minAge = 21

  val isUsAdult = isAdult(20)

  println(isGermanAdult + "\n" + isUsAdult)
}
