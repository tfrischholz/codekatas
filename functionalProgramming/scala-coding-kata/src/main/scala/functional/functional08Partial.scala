package functional

import java.io.{File, PrintWriter}

/**
  * All about partial functions
  */
object functional08Partial extends App {

  // Short version
  val curry = (x: String) => (y: String) => x + " " + y

  println(curry("Hallo")("Welt"))

  //  f(2)(3)         // Same as x => {y => x + y}
  //  |
  //  {y => 2 + y}(3) // The x in f gets replaced by 2
  //  |
  //  2 + 3           // The y gets replaced by 3
  //  |
  //  5

  // So for what is it good?
  // Fox example you can control the execution of parameters?

  /**
    * In this example we write something out and automatically close the handle to the file!
    *
    * @param file The provided file to write
    * @param op The curried print writer function
    */
  def withPrintWriter(file: File)(op: PrintWriter => Unit) = {
    val writer = new PrintWriter(file)

    try {
      op(writer)
    } finally {
      writer.close()
    }
  }

  // So lets call that bitch
  withPrintWriter(new File("date.txt")) {
    writer => writer.println(new java.util.Date())
  }

  // A more advanced user example in pure functional code...

  // First define a high order function which takes functions as arguments
  def func2Partial[A, R](f : A => R) : PartialFunction[A, R] = { case x => f(x) }

  // Define here your first class functions which is working with the high order functions
  val partialInt: PartialFunction[Int, String] = {
    case 1 => "one"
    case 2 => "two"
  }

  // Call partial function or call function <==> reactive pattern
  val f = partialInt orElse func2Partial { _: Int => "default" }

  println(f(1))

  println(f(2))

  println(f(10))
}
