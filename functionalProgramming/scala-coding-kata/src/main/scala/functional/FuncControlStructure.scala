package functional

/**
	* Created by Freshwood on 24.07.2016.
	*/
object FuncControlStructure extends App {

	val data = List(1 to 10)

	def test(input: String => Int => String) = {
		println(s"Got $input")
		input(('a' to 'z').toString())(5)
	}

	def multiplyString(input: String, multiplier: String => Int) = {
		input * multiplier("Hallo")
	}

	val result = test(f => x => f * x)

	val product = multiplyString("Hallo", f => f.map(x => x.asDigit).sum)

	println(result)

	println(product)
}
