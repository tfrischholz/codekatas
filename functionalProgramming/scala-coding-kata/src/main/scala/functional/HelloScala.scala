package functional

/**
  * Created by TFrischholz on 03.06.2016.
  */
object HelloScala {

  val f = println("Hallo Welt")

  var i: Int = 10

  val s: String = i.toString

  val square = i * i

  val fun = (x: Int) => x * x

  val x = fun

	val existList = (1 to 100).toList

	val existFunction = (x: Int) => x > 0

	val test = existList.forall(existFunction)

	println(test)

  def main(args: Array[String]) = {
    i = 11
    println(x(100))
  }

}
