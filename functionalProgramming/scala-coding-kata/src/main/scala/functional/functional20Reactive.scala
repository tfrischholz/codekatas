package functional

import akka.actor.{Actor, ActorSystem, Props}

/**
  * Created by Freshwood on 01.06.2016.
  */
object functional20Reactive extends App {

  class HelloActor(myName: String) extends Actor {
    def receive = {

      case "hello" => println("hello from %s".format(myName))
      case _ => println("'huh?', said %s".format(myName))
    }
  }

  val system = ActorSystem("HelloSystem")

  val helloActor = system.actorOf(Props(new HelloActor("Fred")), name = "helloactor")

  helloActor ! "hello"
  helloActor ! "buenos dias"

}
