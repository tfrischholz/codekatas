package school
import java.util.UUID

import akka.NotUsed
import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.{Flow, Sink, Source}

import scala.collection.immutable
import scala.concurrent.{ExecutionContextExecutor, Future, Promise}
import scala.language.postfixOps
import scala.util.{Failure, Success}

sealed trait AkkaSupport {
  implicit val system: ActorSystem = ActorSystem("TestSystem")

  implicit val materializer: ActorMaterializer = ActorMaterializer()

  implicit val ec: ExecutionContextExecutor = system.dispatcher
}

/**
  * @author Freshwood
  * @since 25.06.2018
  * Come back to scala and learn basic things :)
  * Lession: Make a word counter e.g.: Words ~> (Word -> Size)
  */
object Coming_25062018 extends App {

  val text =
    """Developing a Remote Services IOT platform.Working as a Full Stack Developer I was responsible for the frontend and the backend.
               |The backend was build with the Spring Boot Framework with an Artemis message queue.
               |The frontend SPA with the Play Framework in conjunction with VueJS.Lead Developer for the platform security, user management system and the whole frontend.
               |I heavily used Spring Boot, Scala, Akka and the Play Framework to create a distributed system.Also worked as a Scrum Master for a couple of months."""

  val words = text.split(" ").toList

  val wordMap: Map[String, Int] =
    words.groupBy(w => w).mapValues(value => value.length)

  val sorted: immutable.Seq[(String, Int)] =
    wordMap.toList.sortWith((f, g) => f._2 > g._2)

  println(sorted)
}

object Coming_25062018_Akka_Streams extends App with AkkaSupport {

  val text =
    """Developing a Remote Services IOT platform.Working as a Full Stack Developer I was responsible for the frontend and the backend.
      |The backend was build with the Spring Boot Framework with an Artemis message queue.
      |The frontend SPA with the Play Framework in conjunction with VueJS.Lead Developer for the platform security, user management system and the whole frontend.
      |I heavily used Spring Boot, Scala, Akka and the Play Framework to create a distributed system.Also worked as a Scrum Master for a couple of months."""

  val source: Source[String, NotUsed] =
    Source.fromIterator(() => text.split(" ").toIterator)

  val countFlow: Flow[String, (String, Int), NotUsed] =
    Flow[String].map(word => (word, word.length))

  source.via(countFlow).to(Sink.foreach(println)).run()

  // Keep in mind the sorting as a action where we need the whole stream information (This would blow the border here)
}

/**
  * Just generate 100 UUID's and find a specific letter in it (A) like Adolf :)
  */
object Coming_25062018_General extends App {

  val specificLetter: Char = 'A'

  val uuids = (1 to 100) map (_ => UUID.randomUUID())

  val toUpperCase: String => String = _.toUpperCase()

  val filterFn: String => Boolean = word => word.contains(specificLetter)

  val result = uuids map (_.toString) map toUpperCase filter filterFn

  println(result)
}

object Coming_25062018_General_Akka_Stream extends App with AkkaSupport {

  final val specificLetter: String = "AE"

  val source = Source.fromIterator(() => (1 to 100) map (_ => UUID.randomUUID()) toIterator)

  val toStringFlow = Flow[UUID].map(_.toString)

  val toUpperCaseFlow = Flow[String].map(_.toUpperCase)

  val filterFlow = Flow[String].filter(_.contains(specificLetter))

  val runnableGraph = source via toStringFlow via toUpperCaseFlow via filterFlow to Sink
    .foreach(println)

  runnableGraph.run()
}

object Coming_25062018_Futures extends App with AkkaSupport {

  val future = Future(100)

  val secondFuture = Future {
    throw new ArithmeticException("woot")
    200
  }

  val result = future flatMap (value => secondFuture.map(int => value + int))

  result onComplete {
    case Success(value) => println(value)
    case Failure(ex)    => println(ex)
  }
}

object Coming_25062018_Promises extends App with AkkaSupport {

  val promise = Promise[Int]

  def doSomething(input: Int) = {
    Thread.sleep(5000)
    promise.success(input)
  }

  promise.future.onComplete {
    case Success(value) => println(value)
    case Failure(ex)    => println(ex)
  }

  // You will see the result in 5 seconds
  doSomething(1337)
}
