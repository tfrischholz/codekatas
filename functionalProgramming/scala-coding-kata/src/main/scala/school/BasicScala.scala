package school

import scala.language.{higherKinds, reflectiveCalls}
import scala.util.Random

/**
  * @author Freshwood
  * @since 12.04.2018
  */
object BasicScala extends App {

  // Variable declaration
  val x = 10
  var y = 10

  // lazy evaluation
  lazy val z = {
    Thread.sleep(5000)
    100
  }

  println(x + y)

  println(z)

  def random: Int = Random.nextInt(100)

  (0 to 10) foreach (_ => println(random))
}

object ScalaStructure extends App {

  case class Apple(name: String)

  println(Apple("Apfel"))

  case class Fruit(fn: { def name: String }) {
    println(fn.name)
  }

  object NameObject {
    def name: String = "Fruit"
  }

  println(Fruit(NameObject))
}

object ScalaFunctions extends App {

  def function(): Unit = println("Hallo iCorr Team")

  function()

}

case class Car[S](data: S) extends Functor[Car] {

  override def fmap[A, B](fa: Car[A])(f: A => B): Car[B] = Car(f(fa.data))

  def map[B](f: S => B): Car[B] = Car(f(data))
}

case class Destroyed[S](data: S) extends Functor[Destroyed] {
  override def fmap[A, B](fa: Destroyed[A])(f: A => B) = Destroyed(f(fa.data))
}

sealed trait Functor[F[_]] {
  def fmap[A, B](fa: F[A])(f: A => B): F[B]
}

sealed trait NaturalTransformation[-F[_], +G[_]] {
  def apply[A](fa: F[A]): G[A]
}

object NaturalTransformation {
  type ~>[-F[_], +G[_]] = NaturalTransformation[F, G]
}

object Transformations {
  implicit object CarTransformer extends NaturalTransformation[Car, Destroyed] {
    override def apply[A](fa: Car[A]): Destroyed[A] = Destroyed(fa.data)
  }
}
