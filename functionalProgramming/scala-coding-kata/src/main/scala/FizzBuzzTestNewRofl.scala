/**
  * Created by Freshwood on 01.02.2017.
  */
object FizzBuzzTestNewRofl extends App {

  type F = Int => Option[String]

  val list = (1 to 100).toList

  private val fizz: F = x => if (x % 3 == 0) Some("Fizz") else None
  private val buzz: F = x => if (x % 5 == 0) Some("Buzz") else None
  private val fizzBuzz: F = x => if (x % 15 == 0) Some("FizzBuzz") else None

  val comby: Int => String = number =>
    fizzBuzz(number) orElse buzz(number) orElse fizz(number) getOrElse number.toString

  list foreach { x =>
    println(comby(x))
  }
}
