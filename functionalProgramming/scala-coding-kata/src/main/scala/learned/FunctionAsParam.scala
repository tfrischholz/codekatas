package learned

/**
	* Created by Freshwood on 30.07.2016.
	*/
object FunctionAsParam extends App {

	val sum = (x: Int) => x + x

	val multiply = (x: Int) => x * x

	def callArithmicFunction(param: Int, arithmic: Int => Int) = {
		println(s"Got as param [$param] and apply function $arithmic")
		val result = arithmic(param)
		println(s"Got result [$result]")
	}

	// Testing above val's
	println(sum(5))
	println(multiply(5))

	// run arithmic function
	callArithmicFunction(5, sum)
	callArithmicFunction(5, multiply)

	def IntToString(input: Int): String => Int => String = {
		val squared = input * input
		someString => withid => plusString(s"Previously calculated [$squared] | " + someString + withid)
	}

	def plusString(input: String): String = input + input

	val fTest = IntToString(10)

	println(fTest("My Test String")(543))

}
