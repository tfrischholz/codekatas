package learned

import scala.language.higherKinds

/**
  * @author Freshwood
  * @since 18.07.2018
  *
  */
object CategoryTheoremSemiGroup extends App {

  sealed trait Semigroup[A] {
    def append(x: A, y: A): A
  }

  implicit case object IntSemiGroup extends Semigroup[Int] {
    override def append(x: Int, y: Int): Int = x + y
  }

  def sum[S](list: List[S])(implicit sg: Semigroup[S]): S = list reduce sg.append

  println(sum(List(1, 2, 3, 4, 5)))

  // The same principle works with objects, too

  case class Car(power: Int)

  implicit case object CarSemiGroup extends Semigroup[Car] {
    override def append(x: Car, y: Car): Car = Car(x.power + y.power)
  }

  println(sum(List(Car(100), Car(200), Car(300))))
}

object CategoryTheoremMonoid extends App {

  // Monoid is same as semi group but has an identity / empty function
  sealed trait Monoid[A] {
    def empty: A

    def append(x: A, y: A): A
  }

  implicit object IntMonoid extends Monoid[Int] {
    override def empty: Int = 0

    override def append(x: Int, y: Int): Int = x + y
  }

  def sum[S](list: List[S])(implicit monoid: Monoid[S]): S = list.fold(monoid.empty)(monoid.append)

  println(sum(List(1, 2, 3, 4, 5)))

  // The same principle works with objects, too

  case class Car(power: Int)

  implicit case object CarMonoid extends Monoid[Car] {
    override def append(x: Car, y: Car): Car = Car(x.power + y.power)

    override def empty: Car = Car(0)
  }

  println(sum(List(Car(100), Car(200), Car(300))))
}

object CategoryTheoremFunctor extends App {

  sealed trait Functor[F[_]] {
    def fmap[A, B](fa: F[A])(f: A => B): F[B]
  }

  // Sample for Car

  case class Car[S](input: S) extends Functor[Car] {
    override def fmap[A, B](fa: Car[A])(f: A => B): Car[B] = Car(f(fa.input))

    def map[B](f: S => B): Car[B] = fmap(this)(f)
  }

  val intCar: Car[Int] = Car(100)

  val stringCar: Car[String] = intCar.map(_.toString)

  println(stringCar)

  // Natural Transformation (apply from one category to another)

  sealed trait NaturalTransformation[F[_], G[_]] {
    def apply[A](f: F[A]): G[A]
  }

  type ~>[F[_], G[_]] = NaturalTransformation[F, G]

  // Create another category to map from car to ship

  case class Ship[S](input: S)

  implicit case object VehicleMapper extends (Car ~> Ship) {
    override def apply[A](f: Car[A]): Ship[A] = Ship(f.input)
  }

  println(VehicleMapper(stringCar))

  // Another option is to work with implicitness (functions)

  def transform[S](input: Car[S])(implicit nt: Car ~> Ship): Ship[S] = nt(input)

  println(transform(stringCar) + " via implicitness")
}

object CategoryTheoremMonad extends App {

  // Keep in mind you need a functor and a applicative for this

  sealed trait Functor[F[_]] {
    def fmap[A, B](fa: F[A])(f: A => B): F[B]
  }

  sealed trait Monad[M[_]] extends Functor[M] {
    def pure[A](a: A): M[A]

    def bind[A, B](fa: M[A])(f: A => M[B]): M[B]

    override def fmap[A, B](fa: M[A])(f: A => B): M[B] = bind(fa)(a => pure(f(a)))
  }

  case class Car[+S](value: S) extends Monad[Car] {
    override def pure[A](ma: A): Car[A] = Car(ma)

    override def bind[A, B](fa: Car[A])(fn: A => Car[B]): Car[B] = fn(fa.value)

    def flatMap[B](f: S => Car[B]): Car[B] = bind(this)(f)

    def map[B](f: S => B): Car[B] = Car(f(value))
  }

  val intCar: Car[Int] = Car(100)
  val shortCar: Car[Short] = Car(200)
  val byteCar: Car[Byte] = Car(125)

  val result: Car[Int] = for {
    int <- intCar
    short <- shortCar
    byte <- byteCar
  } yield int + short + byte

  println(result)
}
