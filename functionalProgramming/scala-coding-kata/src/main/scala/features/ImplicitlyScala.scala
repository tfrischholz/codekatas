package features

/**
  * Created by Freshwood on 23.04.2017.
  * Implicitly just checks during the compile time if an implicit value from the given type is available
  * Also you can work with the current implicit view
  * http://stackoverflow.com/questions/3855595/what-is-the-scala-identifier-implicitly
  */
object ImplicitlyScala extends App {

  implicit val value = 10

  val test = implicitly[Int]

  println(test)
}
