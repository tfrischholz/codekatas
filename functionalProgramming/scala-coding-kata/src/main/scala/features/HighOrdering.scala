package features

import scala.language.implicitConversions

/**
	* Created by Freshwood on 07.11.2016.
	*/
object HighOrdering extends App {

	val sampleList = (1 to 100).toList

	println(sampleList)

	val filtered = (x: Iterable[Int]) => x filter (_ > 5)

	println(filtered(sampleList))

	// High Ordering has a function as param or as return type!!!

	def myFiltering(f: Int => Boolean): List[Int] = sampleList filter f

	println(myFiltering(filter => filter > 5))

	def test(x: List[Int] => Boolean): Boolean = x(List(20))

	// A absolute high order function
	def filter(input: List[Int]): Int => List[Int] = id => input filter (_ > id)

	val result = filter(sampleList)

	// As you can see this is a function
	println(result)

	// You can resolve this by calling the function
	val realResult = result(50)

	println(realResult)

	case class Test(item: String)

	object HighOrderingTest {

		val first = "first"

		implicit def testToString: Test => String = {
			case Test(x) => x
		}

		implicit def stringToTest: String => Test = x => Test(x)
	}

	def testThatToString: String = {

		import HighOrderingTest._

		Test("Hallo")
	}

	def testStringToThat: Test = {

		import HighOrderingTest._

		"Hallo"
	}

	println(testThatToString)

	println(testStringToThat)

}
