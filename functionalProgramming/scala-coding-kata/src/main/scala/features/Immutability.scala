package features

import scala.collection.SortedMap

/**
	* Created by Freshwood on 25.09.2016.
	*/
object Immutability extends App {

	// Mutable version
	val numToStarsM = {
		var nToStars = SortedMap.empty[Int, String]
		for (i <- 1 to 10) nToStars += i -> "*" * i
		nToStars
	}

	println(numToStarsM)

	// Immutable and right version

	val numToStarsI = SortedMap.empty ++ (1 to 10).map(i => i -> "*" * i)

	val test2 = (1 to 10).foldLeft(SortedMap[Int, String]())((x, y) => x ++ SortedMap(y -> "*" * y))

	println(numToStarsI)

	println(test2)
}
