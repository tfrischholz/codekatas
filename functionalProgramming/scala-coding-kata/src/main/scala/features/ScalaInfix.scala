package features

object ScalaInfix extends App {

  val first: Calculator = Calculator(50)

  val second: Calculator = Calculator(5)

  val result: Calculator = first / second

  val result2: Calculator = first /: second

  println(result)
  println(result2)
}

case class Calculator(number: Int) {
  def /(that: Calculator): Calculator = Calculator(number / that.number)

  // Every fn with its prefixed with : (colon) is right associative
  def /:(that: Calculator): Calculator = Calculator(number / that.number)
}