package features

import scala.util.Random

/**
  * Created by Freshwood on 14.11.2016.
  */
object FunctionalGenerator extends App {

  // A simple example how to proper write functional object oriented code

  trait Generator[+T] {

    self => // An alias for the this!!!

    def generate: T

    def map[S](f: T => S): Generator[S] = new Generator[S] {
      // You could also write Generator.this.generate
      override def generate: S = f(self.generate)
    }

    def flatMap[S](f: T => Generator[S]): Generator[S] = new Generator[S] {
      override def generate: S = f(self.generate).generate
    }
  }

  val integers = new Generator[Int] {
    override def generate: Int = Random.nextInt()
  }

  val longs = new Generator[Long] {
    override def generate: Long = Random.nextLong()
  }

  val booleans = integers map (_ >= 0)

  for (i <- 1 to 100) println(booleans.generate)
}
