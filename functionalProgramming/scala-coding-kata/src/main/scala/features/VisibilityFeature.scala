package features

/**
  * Created by Freshwood on 05.06.2017.
  */
object VisibilityFeature extends App {

  class TestClass {

    // You can only use this field on the same instance
    private[this] val singleInstanceNumber: Int = 0

    private val privateNumber: Int = 10

    protected val protectedNumber: Int = 20

    val publicNumber: Int = 30

    protected[this] def woot: Int = 1000

    //def multiply: Int = new TestClass().singleInstanceNumber * 100 would not work, cause we have a new instance
  }

  class SuperClass extends TestClass {
    val lol = woot
  }

  object TestClass {

    //val notWork: Int = new TestClass().singleInstanceNumber not work

    //val wooty = new TestClass().woot not work only for the same instance

    val test = new TestClass().privateNumber
  }

  val classy = new TestClass()

  println(TestClass.test)
}

/**
  * The companion object pattern is a heavy useful feature
  * You can access in a class / object the private fields of the companion
  * You can create object with the apply() method -> Which act like classes
  * And you can limit access tho the this instance only (Only the creating instance has access to the field)
  */
object CompanionUsage extends App {

  import Area._

  class Area(name: String) {

    private val fuck = "FuckYou"

    private[this] val test = "Test"

    def sayHi = println(hi)

    def fromCompanion = propForClass

    // Is not accessible cause it is marked for only the creating instance
    // Even for its companion object / class it is not accessible
    //val isPrivate = propForClass2
  }

  object Area {

    val area = new Area("Orsch")

    private val propForClass = "PropForClass"

    private[this] val propForClass2 = "PropForClass"

    private def hi = area.fuck

    // Is not accessible cause it is marked for only the creating instance
    // Even for its companion object / class it is not accessible
    //private def testHi = area.test
  }

  object woot {
    val area = new Area("Orsch")

    // Is not accessible
    //private def test = area.fuck
  }

}
