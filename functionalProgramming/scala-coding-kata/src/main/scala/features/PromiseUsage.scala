package features

import config.ActorContext

import scala.concurrent._
import scala.language.postfixOps
import scala.util.{Random, Success, Try}

/**
  * @author Freshwood
  * @since 01.01.2018
  * More info: https://docs.scala-lang.org/overviews/core/futures.html#promises
  * Just make several asynchronous computations and chain it
  */
object PromiseUsage extends App with ActorContext {

  val p: Promise[Int] = Promise()
  val f: Future[Int] = p.future

  f onComplete {
    case Success(x) => println(x)
  }

  val anotherFuture = Future {
    (1 to 10) map Random.nextInt sum
  }

  val future = blocking {
    Thread.sleep(5000)
    val newPromise: p.type = p completeWith anotherFuture
    newPromise.future.map(_ * 100)
  }

  future onComplete {
    case Success(x) => println("Future: " + x)
  }
}
