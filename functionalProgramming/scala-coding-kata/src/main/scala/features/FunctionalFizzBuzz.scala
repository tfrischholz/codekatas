package features

import scala.language.postfixOps

/**
  * Created by Freshwood on 12.12.2016.
  */
object FunctionalFizzBuzz extends App {

  type FizzType = Int => Option[String]

  val fizz: FizzType = x => if (x % 3 == 0) Some("Fizz") else None
  val buzz: FizzType = x => if (x % 5 == 0) Some("Buzz") else None
  val fizzBuzz: FizzType = x => if (fizz(x).isDefined && buzz(x).isDefined) Some("FizzBuzz") else None

  val functions = List(fizz, buzz, fizzBuzz)

  val functional: Int => String = input => functions flatMap { f =>
    f(input)
  } match {
    case Nil => input.toString
    case x :: Nil => x
    case _ :: y => y.last
  }

  val result = (1 to 100) map functional

  println(result)
}
