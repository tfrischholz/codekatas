package features

/**
	* Created by Freshwood on 12.10.2016.
	*/
object ScalaStreamsExtended extends App {

	// First define an infinite Stream

	def from(n: Int): Stream[Int] = n #:: from(n + 1)

	// Natural numbers
	val nats = from(0)

	val multipliedNumbers = nats map (_ * 4)

	// Just a test
	println(multipliedNumbers.take(100).toList)

	def sieve(s: Stream[Int]): Stream[Int] = s.head #:: sieve(s.tail filter (_ % s.head != 0))

	val primes = sieve(from(2))

	println(primes.take(100).toList)
}
