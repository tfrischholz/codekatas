package features

/**
  * @author Freshwood
  * @since 28.08.2017
  */
object EitherType extends App {

  val definition: Either[String, Exception] = Left("Hallo Welt")

  println(definition.left.map(out => out + " Du Ficker").left.get)

  val exception: Either[String, Exception] = Right(
    new IllegalArgumentException("This is a exception case"))

  println(exception.right.get)
}
