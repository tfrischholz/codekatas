package features

import scala.collection.generic.CanBuildFrom

/**
	* Created by Freshwood on 20.09.2016.
	*
	* @see [[scala.collection.generic.CanBuildFrom]]
	* Just a helper for build fast builder factory objects
	*/
object CanBuildFromFeature extends App {

	val test = implicitly[CanBuildFrom[Nothing, Int, List[Int]]]

	val test2 = test()

	test2 += 10
	test2 ++= List(1, 2, 3)
	println(test2)

	val result: List[Int] = test2.result()

	println(result)

	/**
		* Another example
		*/
	def getBuilder = implicitly[CanBuildFrom[Nothing, Int, List[Int]]]

	val superTest = getBuilder().+=(10)

	println(superTest)

}
