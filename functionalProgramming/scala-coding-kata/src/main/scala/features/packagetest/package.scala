package features

/**
  * Created by Freshwood on 05.07.2017.
  * You are just extending the package above to the package in the main object
  */
package object packagetest {

  type PackageTest = Int
  val PackageTest: Int = 100

}
