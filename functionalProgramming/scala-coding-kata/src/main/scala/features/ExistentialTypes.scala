package features

import scala.language.existentials

/**
  * Created by Freshwood on 17.04.2017.
  * This lesson shows what are existential types are ;)
  */
object ExistentialTypes extends App {

  def normalFunc[T](x: Array[T]) = println(x.length)

  normalFunc(Array[String]("Hallo", "Test"))

  normalFunc(Array[Int](1, 2))

  // You see it works. But without Generics it wouldn't work

  def foo(x: Array[Any]) = println(x.length)
  //foo(Array[String]("foo", "bar", "baz")) => would not compile. Cause we have strongly typed Any

  // Now, with existential types
  def foo2(x: Array[T] forSome { type T }) = println(x.length)

  foo2(Array[String]("Hallo", "Test"))

  // “I want an Array, and I don’t care what type of things it contains”

}

/**
  * Another good approach is to use the forSome keyword for type bounds {type}
  */
object ExistentialTypesPractice extends App {

  sealed trait PlaceHolder
  trait A extends PlaceHolder
  trait B extends A
  trait C extends B
  trait D extends C
  trait E extends D

  // Like we all now
  type aType = String

  val fn: aType => aType = f => f + f

  println(fn("Hallo"))

  // We just only want the super type of C maximal until B
  type bound = T forSome { type T >: C <: B }

  val x: bound = new D {}

  println(x)
}
