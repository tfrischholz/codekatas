package features

/**
  * This example just shows what we can do with PF (partial functions)
  * Partial functions a partial implemented functions
  */
object PartialFunctions extends App {

  // Sample partial function
  val pf: PartialFunction[Int, String] = {
    case x: Int if x > 10 => x toString
  }

  println(pf.isDefinedAt(11))

  println(pf.lift(11))

  // PF are good for composing --> See the following...
  val firstMatcher: PartialFunction[Int, Boolean] = {
    case x if x > 10 => true
  }

  val secondMather: PartialFunction[Int, Boolean] = {
    case x if x < 10 => false
  }

  val thirdMatcher: PartialFunction[Int, Boolean] = {
    case x if x == 10 => true
    case _            => false
  }

  val matcher
    : PartialFunction[Int, Boolean] = firstMatcher orElse secondMather orElse thirdMatcher

  println(matcher(9))

  // You can use specific PF matchers inside functions... --> see this
  def isHelloWorld(input: String,
                   matcher: PartialFunction[String, Boolean]): Boolean =
    matcher(input)

  val helloWorldMatcher: PartialFunction[String, Boolean] = {
    case x => x.equals("Hello World")
  }

  println(isHelloWorld("Hello World", helloWorldMatcher))

  // We can also do more stuff with partial applied function (Write own)
  val partialFunction: Int => String = number => {
    val tempResult: Int = number * 100
    tempResult toString
  }

  println(partialFunction(5))

  type FN = (Int, Int) => Int // Would be fn()()()

  val test: FN = (a, b) => a + b

  println(test)
  println(test.tupled((12, 14)))
  println(test.curried(5)(7))

}

object PartialFunctionsExtended extends App {

  // Define our own partial function short type alias
  type ==?[-A, +B] = PartialFunction[A, B]

  val test: ==?[Int, String] = {
    case 1 => "Eins"
  }

  println(test(2))
}

object PartialFunctionSpecialized extends App {

  val test: PartialFunction[Int, String] = {
    case x if x > 2 => x.toString
  }

  println(test(3)) // test(1) would throw error but why collect on sequence work?

  val seq: Seq[Int] = 1 to 100

  // collect only the numbers which is dividable through ten
  val numbers: Seq[Int] = seq collect {
    case x if x % 10 == 0 => x
  }

  println(numbers)

  val runWithFeature = test.runWith(string => string.toInt)

  println(runWithFeature(10))
}
