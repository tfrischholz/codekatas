package features

import scala.language.postfixOps

/**
  * @author Freshwood
  * @since 02.01.2018
  * Shows how to use the scala synthetic functions
  */
object SyntheticFunctions extends App {

  val test = 100

  // eq -> Checks for reference equality -> == is value equality
  println(test eq null)

  // eq -> Checks for reference non equality -> != is value non equality
  println(test ne null)

  // ## -> just shortcut for hashcode -> Does return ascii code
  println(test ##)

  println(test to 5)

  println(test & 10)
}
