package features

/**
  * Created by Freshwood on 01.06.2017.
  * Assert, Require, Assume, Ensuring are control structures
  * This is a nice style for library designer and software architects
  * Note that assert & assume can be removed at compile time
  * with the command line option to scalac : -Xdisable-assertions
  */
object Preconditions extends App {

  def testAssert(input: Int): Int = {
    assert(input > 0)
    input / 10
  }

  def testRequire(input: Int): Int = {
    require(input > 0)
    input / 5
  }

  def testAssume(input: Int): Int = {
    assume(input % 2 == 0)
    input / 2 * 3
  }

  def testEnsuring(input: Int): Int = {
    require(input % 2 == 0, "Only even numbers are allowed")
    input / 3
  } ensuring(value => value > 100, s"Postcondition failed outcome is smaller then 100")

  println(testAssert(10))

  println(testRequire(10))

  println(testAssume(6))

  println(testEnsuring(304))
}
