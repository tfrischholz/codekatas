package features

import java.time.Instant

/**
	* Created by Freshwood on 21.09.2016.
	*/
object ScalaStreams extends App {

	val stream = (1 to 100000000).toStream

	println(stream.take(100).toList)

	//val result = stream.length //=> would cause an infinite loop

	// this not
	lazy val result = stream.length

	println(stream.head)

	def everyFifthElement(input: Stream[Int]): Stream[Int] =
		input filter (_ % 500 == 0)

	val time = Instant.now().toEpochMilli
	println(everyFifthElement(stream).take(100).toList)
	println(Instant.now().toEpochMilli - time)
}
