package features

/**
	* Created by Freshwood on 21.09.2016.
	*/
object ScalaViews extends App {

	val normalList = List(1, 2, 3, 4, 5)

	println(normalList)

	// Now the view
	println(normalList.view)

	// Now the real test!!!

	val arr = (1 to 10).toArray

	println(arr)

	val view = arr.view.slice(2, 5)

	// modify the array
	arr(2) = 42

	view.foreach(println)

	// change the elements in the view
	view(0) = 10

	view(1) = 20

	view(2) = 30

	// the array is affected:
	println(arr.tail)

	/**
		* The benefit of using a view in regards to performance comes with how the view works
		* with transformer methods.
		*/
}
