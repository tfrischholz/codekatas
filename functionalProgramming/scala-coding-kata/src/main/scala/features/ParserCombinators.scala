package features

import scala.util.parsing.combinator._

/**
  * Created by Freshwood on 19.04.2017.
  * Shows example of the scala parser combinator
  * http://outofaxis.io/blog/2016/07/08/13/
  */
object ParserCombinators extends App {

  class JSON extends JavaTokenParsers {
    def member: Parser[(String, Any)] = stringLiteral ~ ":" ~ value ^^ {
      case name ~ ":" ~ value => (name, value)
    }

    def obj: Parser[Map[String, Any]] = "{" ~> repsep(member, ",") <~ "}" ^^ {
      Map() ++ _
    }

    def arr: Parser[List[Any]] = "[" ~> repsep(value, ",") <~ "]"

    def value: Parser[Any] =
      obj | arr |
        stringLiteral |
        floatingPointNumber ^^ (_.toDouble) |
        "null" ^^ (x => null) | "true" ^^ (x => true) | "false" ^^ (x => false)
  }

  object ParseJSON extends JSON {

    def test(): Unit = {
      val test: String = """{"hallo":"Welt", "joe":{"name":"shome"}}"""

      println(parseAll(value, test))
    }
  }

  ParseJSON.test()

  // Next step would be a parser which is self development number + number ;)

  val testString: String = "10 + 50"

  class SimpleStringParser extends RegexParsers {

    def number: Parser[Int] = """\d*""".r ^^ (_.toInt)

    def plusToken: Parser[String] = """\+""".r ^^ (_.toString)

    def parsingTest: Parser[Any] = number ~ plusToken ~ number ^^ { f =>
      f._2 + f._1._1
    }
  }

  object Runner extends SimpleStringParser {

    def test(): Unit = println(parse(parsingTest, testString).getOrElse(10))

  }

  Runner.test()

  // I am proud to learned that ;)
}
