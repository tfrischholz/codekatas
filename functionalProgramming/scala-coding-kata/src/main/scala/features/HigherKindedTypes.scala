package features

import scala.language.higherKinds

/**
  * Created by Freshwood on 18.04.2017.
  * This sample shows the real usage of higher kinded types
  * You will see this for example by the scala import above
  * 99% of the usage is a functor of a Type which takes another type as a type param
  */
object HigherKindedTypes extends App {

  sealed trait Choice[+A]

  case class Yep[A](value: A) extends Choice[A]

  case object Nope extends Choice[Nothing]

  object ChoiceFunctor extends Functor[Choice] {
    def map[A,B](fa: Choice[A])(f: A => B): Choice[B] =
      fa match {
        case Nope   => Nope
        case Yep(a) => Yep(f(a))
      }
  }

  ChoiceFunctor.map(Yep(42))({ x => println(x) }) // 42

}

trait Functor[F[_]] {
  def map[A, B](fa: F[A])(f: A => B): F[B]
}