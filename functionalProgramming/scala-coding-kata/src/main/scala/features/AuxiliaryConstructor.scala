package features

/**
	* Created by Freshwood on 21.10.2016.
	*/
object AuxiliaryConstructor extends App {

	 class Car(carName: String, carType: String) {

		 def this(carName: String) {
			 this(carName, "BMW")
		 }

		 def this() = {
			 this("Mein Auto", "BMW")
		 }

		 def show(): Unit = println(s"Car has the name $carName from the type $carType")
	 }

	val car = new Car("Mein Auto", "BMW")

	val myCar = new Car("Mein Auto")

	val newCar = new Car

	List(car, myCar, newCar) foreach (_ show())

	// Sample for case classes
	case class SuperCar(name: String, carType: String) {
		def show() = println(s"Car has the name $name from the type $carType")
	}

	object SuperCar {
		def apply(name: String): SuperCar = new SuperCar(name, "BMW")

		def apply(): SuperCar = new SuperCar("Mein Auto", "BMW")
	}

	val superCar = SuperCar("Mein Auto", "BMW")

	superCar.show()

	val auxiliarySuperCar = SuperCar()

	auxiliarySuperCar.show()

}
