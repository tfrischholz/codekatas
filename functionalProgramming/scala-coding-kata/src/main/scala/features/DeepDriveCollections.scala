package features

import scala.language.postfixOps

/**
	* Created by Freshwood on 20.09.2016.
	*/
object DeepDriveCollections extends App {

	// Assume a normal collection
	val myList = List(1, 2, 3, 4, 5)

	val list = (0 to 100).toList

	// This is a synchronized list
	println(myList.getClass)

	// This is a parallel list
	println(myList.par.getClass)

	// Info: e.g. GetTraversableOnce is only a generated list which is Linear or Parallel

	val collectTest = list collect {
		case x: Int if x > 90 => x
	}

	println("Collect Test:" + collectTest)

	val flatMapTest = list flatMap (Some(_))

	println(flatMapTest)

	val partitionTest = list partition (_ <= 50)

	// same as
	val takeWhileTest = list span(_ <= 50)

	println(partitionTest._1)
	println(partitionTest._2)

	// grouping
	val groupTest = list groupBy (_ > 10)

	println(groupTest)

	// zipping
	val zipTest = list zip (0 to 10)

	println(zipTest)

	// zip with an index
	val zipIndexTest = list zipWithIndex

	println(zipIndexTest)

	// zip all test
	val zipAllTest = list zipAll('a' to 'z', "fuck", 1337)

	println(zipAllTest)

}
