package features

/**
  * Created by Freshwood on 07.05.2017.
  * In other words, TypeTag provides runtime information about the type
  * while ClassTag provides runtime information about the value
  * (more specifically, information that tells us what is the actual type of the value in question at runtime).
  */
object TypeErasure extends App {

  // Example of the type erasure problem

  object Extractor {
    def extract[T](list: List[Any]) = list.flatMap {
      case element: T => Some(element)
      case _ => None
    }
  }

  val list = List(1, "string1", List(), "string2")
  val result = Extractor.extract[String](list)
  println(result) // List(1, string1, List(), string2)
}

object TypeErasureClassTag extends App {

  import scala.reflect.ClassTag

  object Extractor {
    def extract[T](list: List[Any])(implicit tag: ClassTag[T]) =
      list.flatMap {
        //case (element @ tag(_: T)) => Some(element) -> Compiler stuff
        case element: T => Some(element)
        case _ => None
      }
  }
  val list: List[Any] = List(1, "string1", List(), "string2")
  val result = Extractor.extract[String](list)
  println(result) // List(string1, string2)

  // ----> This won't work
  val list2: List[List[Any]] = List(List(1, 2), List("a", "b"))
  val result2 = Extractor.extract[List[Int]](list)
  println(result2) // List(List(1, 2), List(a, b))

  // ClassTag can only check the first hierarchy of a type. e.g. T => List[?] or T => Set[?]
  // It doesn't check the type in the list
  // To fix this we need type tags
}

object TypeErasureTypeTags extends App {

  // TypeTags have richer type information for example: It checks:
  // List[List[Set[Int]]] or Map[String, List[Int]]

  import scala.reflect.runtime.universe._

  object Recognizer {
    def recognize[T](x: T)(implicit tag: TypeTag[T]): String =
      tag.tpe match {
        case TypeRef(utype, usymbol, args) =>
          List(utype, usymbol, args).mkString("\n")
      }
  }

  val list: List[Int] = List(1, 2)
  val result = Recognizer.recognize(list)
  println(result)
  // prints:
  //   scala.type
  //   type List
  //   List(Int)
}

/**
  * Weak type tags are only useful for abstract types
  * You can not mix type tags with abstract types
  * Weak type tag is a super set of a type tag
  */
object TypeErasureWeakTypeTag extends App {

  import scala.reflect.runtime.universe._

  abstract class SomeClass[T] {

    object Recognizer {
      //def recognize[S](x: S)(implicit tag: TypeTag[S]): String = --> Would not work
      def recognize[S](x: S)(implicit tag: WeakTypeTag[S]): String =
        tag.tpe match {
          case TypeRef(utype, usymbol, args) =>
            List(utype, usymbol, args).mkString("\n")
        }
    }

    val list: List[T]
    val result = Recognizer.recognize(list)
    println(result)
  }

  new SomeClass[Int] { val list = List(1) }
  // prints:
  //   scala.type
  //   type List
  //   List(T)

}