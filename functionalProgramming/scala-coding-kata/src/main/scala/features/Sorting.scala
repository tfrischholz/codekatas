package features

import java.time.Instant

import scala.util.Random

/**
  * Created by Freshwood on 19.11.2016.
  */
object Sorting extends App {

  case class Severity(element: String)

  object Severity {
    val Critical = Severity("Critical")
    val Warning = Severity("Warning")
    val Info = Severity("Info")

    val randomSeverity = (randomNumber: Int) => if (randomNumber > 0 ) Critical else Info
  }

  case class SomeObject(id: Int, severity: Severity, created: Long)


  val testList = (1 to 100) map (SomeObject(_, Severity.randomSeverity(Random.nextInt(10)), Instant.now().toEpochMilli))

  println(testList.sortBy(x => x.severity != Severity.Critical))

  println(testList.sortWith((x, y) => x.id == y.id))
}
