package features

import akka.actor.ActorSystem
import config.ActorContext

import scala.concurrent.{ExecutionContext, Future, Promise}
import scala.util.{Failure, Success, Try}

/**
  * @author Freshwood
  * @since 25.08.2017
  */
object FutureUsage extends App {

  implicit lazy val system: ActorSystem = ActorSystem()

  implicit lazy val ex: ExecutionContext = system.dispatcher

  val future: Future[Int] = Future(100 / 0)

  val recover: Future[Int] = future recover {
    case ex: ArithmeticException =>
      println(s"Exception lol ${ex.getMessage}")
      15
  }

  val recoverWith: Future[Int] = future recoverWith {
    case ex: ArithmeticException =>
      Future.successful {
        println("Exception found")
        100
      }
  }

  val fallback: Future[Int] = future.fallbackTo(Future(1337))

  val transformed =
    fallback.transform(identity, e => new IllegalArgumentException(e.getMessage))

  transformed foreach println
}

object FutureHacking extends App {

  implicit lazy val system: ActorSystem = ActorSystem()

  implicit lazy val ex: ExecutionContext = system.dispatcher

  lazy val future: Future[Int] = Future(throw new IllegalArgumentException("Test"))

  private def recover(pf: PartialFunction[Throwable, Int]): Future[Int] = {
    val p = Promise[Int]()

    future.onComplete(f => p complete (f recover pf))

    p.future
  }

  val recoverFuture = recover {
    case _ => 10
  }

  recoverFuture onComplete (f => println(f.get))

  // What would happen if we have multiple success callbacks

  val testFuture: Future[Int] = Future(100)

  val test1: Unit = testFuture onComplete {
    case Success(x) => println(s"test1 $x")
    case Failure(e) => println(e.getMessage)
  }

  val test2: Unit = testFuture onComplete {
    case Success(x) => println(s"test2 $x")
    case Failure(e) => println(e.getMessage)
  }
}

object EventCallbackWithPromise extends App with ActorContext {

  trait EventStructure[S] {

    def onComplete[A](f: S => A): Unit

    def compute(): Unit
  }

  object EventStructure {

    def apply[S](value: => S): EventStructure[S] = new EventStructure[S] {

      val promise: Promise[S] = Promise[S]()

      override def onComplete[A](f: S => A): Unit = promise.future.onComplete {
        case Success(x) => f(x)
        case Failure(_) => // Swallow
      }

      override def compute(): Unit = promise.complete(Try[S](value))
    }
  }

  val eventStructure = EventStructure(1000)

  eventStructure onComplete (x => println(x * x))

  eventStructure.compute()
}

object EventCallbackWithCustomExecutor extends App {

  trait EventStructure[S] {

    def onComplete[A](f: S => A): Unit

    def compute(): Unit
  }

  object EventStructure {

    def apply[S](value: => S): EventStructure[S] = new EventStructure[S] {

      var seq: Seq[S => _] = Seq()

      val promise: Promise[S] = Promise[S]()

      override def onComplete[A](f: S => A): Unit = seq = seq :+ f

      override def compute(): Unit =
        Try(value).foreach(
          result =>
            // Cool feature instead of 9 secs the duration here is 3 secs
            seq.par.foreach(f => {
              Thread.sleep(3000)
              f(result)
            }))
    }
  }

  val eventStructure = EventStructure(1000)

  eventStructure onComplete (x => println(x * x))

  eventStructure onComplete (x => println(x + x))

  eventStructure onComplete (x => println(x - x))

  eventStructure.compute()
}
