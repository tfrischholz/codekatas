package features

/**
  * @author Freshwood
  * @since 27.08.2017
  */
object CaseObjects extends App {

  sealed trait Entity

  case object TestObject extends Entity {
    def test: String = "Hallo Welt"
  }

  case object TestObject2 extends Entity {
    def woot: String = "Hallo Woot"
  }

  def tester: Entity => String = {
    case x @ TestObject  => x.test
    case x @ TestObject2 => x.woot
    case _               => "Fuck"
  }

  println(tester(TestObject2))
}
