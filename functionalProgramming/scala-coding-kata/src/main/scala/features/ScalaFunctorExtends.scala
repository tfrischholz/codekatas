package features

/**
  * Created by Freshwood on 19.04.2017.
  * This sample should show how we can inherit from functions :)
  */
object ScalaFunctorExtends extends App {

  // Sample Function Int => String

  val sample: Int => String = int => int.toString

  class FunInherit extends (Int => String) {
    override def apply(v1: Int): String = (v1 + 10).toString
  }

  val test: FunInherit = new FunInherit

  val woot = test(11)

  println(woot)
}

object ScalaFunctorExtends2 extends App {

  type Int2String = Int => String

  val function: Int2String = number => (number + 10) toString

  val result = function(100)

  println(result)
}