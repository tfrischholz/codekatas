package features

/**
  * Created by Freshwood on 24.04.2017.
  */
object Lifting extends App {

  val pf: PartialFunction[Int, Boolean] = { case i if i > 0 => i % 2 == 0}

  val pfApplied = pf(10)

  val lifted = pf.lift

  val liftedApplied = lifted(10)

  println(pfApplied)

  println(liftedApplied)

  // More complex tests

  val list = Seq(1, 2, 3)

  println(list)

  // list(10) would throw exception

  val liftedList = list.lift

  println(liftedList(10))

}
