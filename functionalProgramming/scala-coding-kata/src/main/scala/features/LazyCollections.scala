package features

import scala.collection.mutable

/**
  * Created by Freshwood on 23.12.2016.
  */
object LazyCollections extends App {

  def func[A](a: A): A = { println("f"); a }

  val xs = mutable.LinkedList(1, 2, 3, 4, 5)

  val result = xs.map(x => func(x)+1).map(x => func(x)*2).filter(x => func(x) < 10).find(x => func(x) == 6)

  // We see a lot of useless operations
  println(result)

  val lazyResult = xs.iterator.map(x => func(x)+1).map(x => func(x)*2).filter(x => func(x) < 10).find(x => func(x) == 6)

  println(lazyResult)
}
