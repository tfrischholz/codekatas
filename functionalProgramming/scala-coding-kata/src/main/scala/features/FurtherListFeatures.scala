package features

/**
	* Created by Freshwood on 20.10.2016.
	*/
object FurtherListFeatures extends App {

	val sample = (0 to 10).toList

	println(sample)

	val takeElement = sample.takeWhile(_ < 10)

	println(takeElement)

	val dropElement = sample.dropWhile(_ < 0)

	println(dropElement)

	val simpleSum = sample.reduceLeft((x, y) => x + y)

	println(simpleSum)

	val foldSum = sample.foldLeft(1)((x, y) => x + y)

	println(foldSum)
}
