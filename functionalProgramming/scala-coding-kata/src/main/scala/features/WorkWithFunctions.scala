package features

import scala.language.postfixOps

/**
	* Created by Freshwood on 06.11.2016.
	*/
object WorkWithFunctions extends App {

	// Generate a list to work with
	val dataHolder = (1 to 100).toList

	// Define a high order function
	def listFilter(f: Int => List[Int], id: Int) = f(id)

	// The immutable equals filter which produces a function
	val filter = (id: Int) => dataHolder.filter(_ == id)

	println(listFilter(filter, 10))

	// Assume the fizz buzz sample. In this step It should call a function on FizzBuzz to
	// This time pure functional

	def onFizzBuzz(f: Int => Unit, x: Int) = {
		println(s"On Fizz Buzz called $x")
		f(x)
		println(s"On Fizz Buzz ended $x")
	}

	val isEven = (x: Int) => if (x % 2 == 0) {
		println("Yes it is a even number")
	} else {
		println("Not even")
	}

	val fizzBuzz = 1 to 100 foreach (e => println{
		(e % 3, e % 5) match {
			case (0, 0) => onFizzBuzz(isEven, e)
			case (0, _) => "Fizz"
			case (_, 0) => "Buzz"
			case _ => e
		}
	})
}

object WorkWithFunctions2 extends App {

	val x = 10

	val y = 12

	val sum = x + y

	println(sum)

	def mySum(x: Int, y: Int): Int = x + y

	println(mySum(x, y))

	val test = mySum _

	println(test)

	val test2 = test(14, 25)

	println(test2)

	def myFn(x: Int, y: Int): Int => String = _ => toString

}

object WorkWithPartials extends App {

	// Display only even numbers otherwise print Noob

	// source
	val list = (1 to 100) toList

	val even: PartialFunction[Int, String] = {
		case x if x % 2 == 0 => x toString
	}

	val printNoob: PartialFunction[Int, String] = {
		case _ => "Noob"
	}


	//list foreach (number => if (number % 2 == 0) println(number) else println("Noob"))

	val result = list map (even orElse printNoob) foreach println

}

object SomeFunnyFunTest extends App {

	val model: Seq[Int] = 1 to 100

	val filter: Seq[Int] => Int = input => input find (_ == 10) getOrElse 0

	val result = filter(model)

	println(result)

	// But what we need is a high order function... ???

	private def modelFilter(input: Seq[Int]): Int => Seq[Int] = number => input filter (_ > number)

	val fnFilter: Int => Seq[Int] = modelFilter(model)

	println(fnFilter(57))
}