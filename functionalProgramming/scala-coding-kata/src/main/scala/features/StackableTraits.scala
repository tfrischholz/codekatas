package features

/**
  * Created by Freshwood on 24.04.2017.
  * Just adds features to a new created class
  */
object StackableTraits extends App {

  trait Machine {
    def show: Unit
  }

  class MachineImpl extends Machine {
    override def show = println("Machine")
  }

  trait Car {
    def showCar: Unit = println("Car")
  }

  trait Ship {
    def showShip: Unit = println("Ship")
  }

  val machine = new MachineImpl

  machine.show

  val stackable = new MachineImpl with Car with Ship

  stackable.showCar
  stackable.showShip

}
