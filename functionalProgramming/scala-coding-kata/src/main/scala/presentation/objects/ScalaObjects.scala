package presentation.objects

/**
  * @author Freshwood
  * @since 19.07.2018
  */
object ScalaObjectsGeneral extends App {

  sealed trait Entity {
    def id: Int
  }

  sealed trait Vehicle extends Entity {
    def name: String
  }

  case class Car(id: Int, name: String) extends Vehicle

  case class Ship(id: Int, name: String) extends Vehicle

  val car = Car(1, "Car")

  val ship = Ship(2, "Ship")

  def retrieveId(entity: Entity): Int = entity.id

  println(retrieveId(car))
  println(retrieveId(ship))

  // Of course this works with normal classes, too

  class Airplane(val id: Int, val name: String) extends Vehicle

  println(retrieveId(new Airplane(3, "Airplane")))
}

object ScalaObjectsAdvanced extends App {

  // class with companion
  // Keep in mind you can not unapply on case classes
  class Person(val name: String, val age: Int)

  object Person {
    def unapply(arg: Person): Option[String] = Some(s"${arg.name}:${arg.age}")
  }

  val person = new Person("John", 28)

  println {
    person match {
      case x @ Person("John:28") => x.age
      case _                     => "Not found"
    }
  }
}

/**
  * In this example we want to extract different car brands
  */
object ScalaObjectsSample extends App {

  sealed trait Vehicle {
    def name: String
    def created: Int
  }

  class BMW(val name: String, val created: Int) extends Vehicle

  object BMW {
    def unapply(arg: BMW): Option[Boolean] = if (arg.created == 1900) Some(true) else None
  }

  class Mercedes(val name: String, val created: Int) extends Vehicle

  object Mercedes {
    def unapply(arg: Mercedes): Option[Boolean] = if (arg.created == 1900) Some(true) else None
  }

  val testVehicle: Vehicle = new BMW("BMW", 2000)

  println {
    testVehicle match {
      case BMW(x) => x
      case _      => "Someone want to cheat here :)"
    }
  }
}
