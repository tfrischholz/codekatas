package presentation.patterns

/**
  * @author Freshwood
  * @since 19.07.2018
  */
object ScalaPatternsImplicits extends App {

  // Given a simple class we can extend it with implicit classes
  case class Car(power: Int)

  object Implicits {
    implicit def intToString: Int => String = _.toString

    implicit class CarToString(car: Car) {
      def powerToString: String = car.power.toString
    }
  }

  import Implicits._

  val powerString: String = Car(100).power

  val powerString2: String = Car(250).powerToString

  println(powerString)
  println(powerString2)
}

/**
  * With this you can extend type classes or for example implicit classes
  * Or extend functionality with implicit context
  * For example the play json writer / reads
  */
object ScalaPatternsPimpMyLibrary extends App {

  sealed trait Vehicle {
    def name: String
  }

  trait VehicleDisplay[S <: Vehicle] {
    def display: String
  }

  case class Ship(name: String) extends Vehicle

  case class Car(name: String) extends Vehicle

  case class Airplane(name: String) extends Vehicle

  object Implicits {

    implicit object ShipDisplayer extends VehicleDisplay[Ship] {
      override def display: String = "This is a ship"
    }

    implicit object CarDisplayer extends VehicleDisplay[Car] {
      override def display: String = "This is a car"
    }
  }

  import Implicits._

  def showCorrectVehicleName[A <: Vehicle](vehicle: A)(
      implicit display: VehicleDisplay[A]): String =
    vehicle.name + " " + display.display

  println(showCorrectVehicleName(Ship("Ship")))
  println(showCorrectVehicleName(Car("Car")))
  //println(showCorrectVehicleName(Airplane("Airplane"))) would not work cause we have no implicit for it
}

/**
  * With the magnet pattern you can work with a specific type
  * which will be later extracted by implicit functions
  * The magnet is the opposite of the pimp my library pattern
  */
object ScalaPatternsMagnetPattern extends App {

  trait VehicleKind {
    def kind: String
  }

  case class LandVehicle(kind: String) extends VehicleKind
  case class AirVehicle(kind: String) extends VehicleKind

  // Now some real vehicles
  case class Car(name: String)
  case class Airplane(name: String)

  object Magnet {

    implicit def carVehicleKind: Car => VehicleKind = _ => LandVehicle("Land")

    implicit def airVehicleKind: Airplane => VehicleKind = _ => AirVehicle("Air")
  }

  import Magnet._

  def vehicleKind(vehicleKind: VehicleKind): String = vehicleKind.kind

  println(vehicleKind(Car("BMW")))
  println(vehicleKind(Airplane("Lufthansa")))
}

/**
  * The cake pattern should be used when you want to compose different classes
  * You just extend a functionality but not its behaviour this is very important
  */
object ScalaPatternsCakePattern extends App {

  trait Vehicle {
    def name: String
  }

  trait Wheel extends Vehicle {
    final def wheel: String = "Rad"
  }

  trait Engine extends Vehicle {
    def engine: String = "Motor"
  }

  trait VehicleComponent { self: Wheel with Engine =>
    override def name: String = self.wheel + "-" + self.engine
  }

  //val vehicle = new VehicleComponent does not work missing objects

  val concrete: VehicleComponent = new VehicleComponent with Engine with Wheel

  println(concrete.name)
}

/**
  * The loan pattern should be used when you want to abstract the operation from rest
  * Always useful when you have always the same logic and you want to generalize this e.g.
  * def testContext(testCode: DefaultObjects => Any): Unit = {
  *   val testData = createTestData()
  *   try {
  *   testCode(testData) // "loan" the fixture to the test
  *   }
  *   finally clearTestData(testData) // clean up the fixture
  * }
  */
object ScalaPatternsLoanPattern extends App {

  def loan[A](op: => A): A =
    try {
      op
    } catch {
      case _: Throwable => throw new IllegalArgumentException("Could not call operation")
    }

  println(loan(10 / 2))
}
