# Scala Themen

1. Basics wie val, var lazy val def -> initialisierung ~> Schreibweise F[_] ~> G[_]
2. Struktur class, case class, trait, object, case object, sealed classes, abstract, final modifier, Companion objects
3. Funktionen und Schreibweise,  Partielle Funktionen , pattern matching, types -> existential types, fn by val -> fn by name, Currying
4. Infix notation
5. private [this] and magic
6. Collections
7. Option, Either, Validation
8. Functional Patterns -> Cake Pattern, Duck Pattern -> Structural types, Loan Pattern, Magnet Pattern, PimpMyLibrary Pattern, Stackable Traits, Mixins
9. Semigroup, Monoid, Functor, Natural Transformation, Bind -> Monad
10. Optics, Lenses
11. Free Monads (usage with Interpreter)
12. Akka
