package presentation.basics

/**
  * @author Freshwood
  * @since 19.07.2018
  */
object ScalaBasicsInitialization extends App {

  val test = 100

  lazy val test2 = 100

  def defTest = 100

  println(test)
  println(test2)
  println(defTest)
}

object ScalaBasicsFunctions extends App {

  // Normal function
  def square(input: Int): Int = input * input

  // Other style
  val squareFn: Int => Int = i => i * i

  // e.g. a function which returns a function

  def productFn(input: Int): Int => Int = unknown => input * unknown

  // test
  val fn: Int => Int = productFn(100)

  println(fn(50))

  // Functions by val

  def nanoTime: Long = {
    println("Fetching nano time...")
    System.nanoTime()
  }

  def valFn(input: Long): Long = {
    println("In by val")
    input
    println("Calling by val again")
    input
  }

  println(valFn(nanoTime))

  // Functions by ref
  def valRef(input: => Long): Long = {
    println("In by ref")
    input
    println("Calling by ref again")
    input
  }

  println(valRef(nanoTime))

  // This is a powerful feature by val gets always first evaluated
  // By ref will be later (multiple times) evaluated -> you call always a function and not the value
}

object ScalaBasicsPatternMatching extends App {

  val list: List[Int] = (1 to 100).toList

  // Match the last number
  def lastNumber(input: List[Int]): Int = input match {
    case Nil      => 0
    case x :: Nil => x
    case _ :: y   => lastNumber(y)
  }

  println(lastNumber(list))

  // Same work with names e.g. match Tobias
  val names = Seq("Franz", "Helmut", "Regina", "Tobias")

  println {
    names.collect {
      case "Tobias" => "Found"
    }
  }
}
