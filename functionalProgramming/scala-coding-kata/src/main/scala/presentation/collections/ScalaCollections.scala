package presentation.collections

/**
  * @author Freshwood
  * @since 19.07.2018
  * Just a sample and usage of the most scala collection functions
  */
object ScalaCollections extends App {

  lazy val list: Seq[Int] = 1 to 100

  // Reduce left reduces the stream to a concrete value
  println("Reduced left: " + list.reduceLeft((x, y) => x + y))

  // Reduce left option wraps an option around the value -> produces none when the list is empty or none
  println("Reduced left option: " + list.reduceLeftOption((x, y) => x + y))
  // Reduce right does this from the right side (reverse the stream and use reduceleft same here)

  // The same as reducing but this time with a default value (So we don't need a option functionality here)
  println(list.foldLeft(0)((x, y) => x + y))
  // Fold right does this from the right of the list

  // Sliding just slide the sequence with the given elements and the sliding count -> produces multiple sequences
  println(list.sliding(5, 3).toSeq)

  // Just filter the list for all 50 values (in this case we have only one element
  println(list.filter(x => x == 50))

  // Groups all matches to the key value
  println(list.groupBy(x => x * x))

  // Scan left create a sequence of all results in every computation
  println(list.scanLeft(0)((x, y) => x + y))

  // Dropping everything which is smaller then 50 in the list
  println(list.dropWhile(x => x < 50).toList)

  // Taking everything which is smaller then 50 in the list
  // Keep in if the condition is breaking the list is returned (It is as fast way to get subsequence of a list)
  println(list.takeWhile(x => x < 50).toList)

  // Makes a diff from the first and the second list (Removes the the elements from the given list)
  println(list.diff(Seq(7, 8, 9)))

  // Makes a intersection with the second list (cross table style)
  println(list.intersect(Seq(7, 8, 9)))

  // Just cut the list with the given params
  println(list.slice(5, 45).toList)

  // Aggregate is like foldLeft but should be used in parallel (In normal mode combo fn will not be called)
  println(list.par.aggregate(0)((x, y) => x + y, (x, y) => x + y))

  // Should be false cause the first element is <= 1
  println(list.forall(x => x > 1))

  // Stacks the parameter with the underlying list from the beginning one by one
  println(list.zip(Seq(7, 8, 9)))

  // The opposite of zip, Makes a list of the first and in this case of the second element in the sublist)
  println(list.zip(Seq(7, 8, 9)).unzip)

  // Makes a zip for every element in the list, when no element is found in the 2. seq then the default will be set
  println(list.zipAll(Seq(7, 8, 9), 1337, 1338))

  // Just zip the list with an incremental index
  println(list.zipWithIndex)
}
