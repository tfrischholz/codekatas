package presentation.types

import scala.language.existentials

/**
  * @author Freshwood
  * @since 19.07.2018
  */
object ScalaTypesBasics extends App {

  type MyString = String

  type Prod = Int => Int

  def test(input: Int): MyString = input.toString

  println(test(1337))

  val product: Prod = input => input * input

  def template(f: Prod): Prod = variable => f(variable)

  val sum: Prod = template(f => f + f)

  val prod: Prod = template(f => f * f)

  println(sum(50))

  println(prod(50))
}

// A Type is only an alias (not really a hardcoded type)
object ScalaTypesAdvanced extends App {

  // Describe a type which determine a flow e.g. Int => String
  type FUNCTION = Int => String

  sealed trait Transformation[A, B] {
    def apply(input: A): B
  }

  // But we want a flow
  type ~>[A, B] = Transformation[A, B]

  // Also called natural transformation (for primitive types)
  object IntToStringTransformer extends (Int ~> String) {
    override def apply(input: Int): String = input.toString
  }

  println(IntToStringTransformer(1337))
}

object ScalaTypesPartialFunction extends App {

  val pf: PartialFunction[T forSome { type T }, String] = {
    case x => x.toString
  }

  println(pf(100))

  val isEven: PartialFunction[Int, Int] = {
    case x if x % 2 == 0 => x
  }

  println(isEven(2))

  val list: Seq[Int] = 1 to 100

  val evenNumbers = list collect isEven

  println(evenNumbers)

  def functionWithPartial(input: Int)(pf: PartialFunction[Int, Int]): Int = {
    val partial: PartialFunction[Int, Int] = pf.orElse {
      case _ => 0
    }
    partial(input)
  }

  println(functionWithPartial(3)(isEven))
}
