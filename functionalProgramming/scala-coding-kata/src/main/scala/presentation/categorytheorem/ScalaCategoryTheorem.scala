package presentation.categorytheorem

import scala.language.higherKinds

/**
  * @author Freshwood
  * @since 19.07.2018
  */
object ScalaCategoryTheoremSemiGroup extends App {

  trait SemiGroup[S] {
    def append(x1: S, x2: S): S
  }

  object SemiGroup {
    implicit object IntSemiGroup extends SemiGroup[Int] {
      override def append(x1: Int, x2: Int): Int = x1 + x2
    }

    implicit object StringSemiGroup extends SemiGroup[String] {
      override def append(x1: String, x2: String): String = x1 + x2
    }
  }

  def concat[A](x: A, y: A)(implicit sg: SemiGroup[A]): A = sg.append(x, y)

  println(concat(10, 10))
  println(concat("Hallo ", "Welt"))
}

object ScalaCategoryTheoremMonoid extends App {

  trait Monoid[S] {

    def unit: S

    def append(x1: S, x2: S): S
  }

  object Monoid {
    implicit object IntMonoid extends Monoid[Int] {
      override def append(x1: Int, x2: Int): Int = x1 + x2

      override def unit: Int = 0
    }

    implicit object StringMonoid extends Monoid[String] {
      override def append(x1: String, x2: String): String = x1 + x2

      override def unit: String = ""
    }
  }

  def fold[A](input: Seq[A])(implicit m: Monoid[A]): A = input.foldLeft(m.unit)(m.append)

  println(fold(Seq(34, 30, 10)))

  println(fold(Seq("Das", "ist", "ein", "schöner", "Satz")))
}

/**
  * A functor make it possible to map the value in another value e.g.:
  * F[A] -> F[B]
  */
object ScalaCategoryTheoremFunctor extends App {

  trait Functor[F[_]] {
    def fmap[A, B](input: F[A])(f: A => B): F[B]
  }

  case class Car[S](data: S) extends Functor[Car] {

    override def fmap[A, B](input: Car[A])(f: A => B): Car[B] = Car(f(input.data))

    def map[B](f: S => B): Car[B] = this.fmap(this)(f)
  }

  val car: Car[Int] = Car(100)

  val mappedCar: Car[String] = car.map(input => input.toString + "_mapped")

  println(mappedCar.data)
}

/**
  * A natural transformation makes it possible to map the object to another object e.g.:
  * F[A] -> G[A]
  */
object ScalaCategoryTheoremNaturalTransformation extends App {

  trait NaturalTransformation[F[_], G[_]] {
    def apply[A](input: F[A]): G[A]
  }

  object NaturalTransformation {
    type ~>[F[_], G[_]] = NaturalTransformation[F, G]
  }

  import NaturalTransformation._

  case class Car[S](data: S)

  case class Ship[S](data: S)

  object TransformCarToShip extends (Car ~> Ship) {
    override def apply[A](input: Car[A]): Ship[A] = Ship(input.data)
  }

  val car: Car[Int] = Car(100)

  val ship: Ship[Int] = TransformCarToShip(car)

  println(ship)
}

object ScalaCategoryTheoremMonad extends App {

  trait Monad[M[_]] {

    def unit[A](x: M[A]): M[A]

    def bind[A, B](input: M[A])(f: A => M[B]): M[B]
  }

  case class Car[S](data: S) extends Monad[Car] {
    override def unit[A](x: Car[A]): Car[A] = x

    override def bind[A, B](input: Car[A])(f: A => Car[B]): Car[B] = f(input.data)

    def flatMap[B](f: S => Car[B]): Car[B] = bind(this)(f)
  }

  val car: Car[Int] = Car(100)

  val stringCar: Car[String] = car.flatMap(f => Car(f.toString + "_flatMapped"))

  println(stringCar)
}
