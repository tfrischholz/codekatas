package sample

/**
  * Created by Freshwood on 11.05.2016.
  */
object GenericSample extends App {

  // Test my own stack ;)
  val stack = new MyStack[Any]

  stack.push("Hallo")
  stack.push("Welt")
  stack.push(10)

  println(stack)
  println(stack.pop)
}
