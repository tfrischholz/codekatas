package sample

/**
  * Created by Freshwood on 12.05.2016.
  */
class MyStack[T] {

  var elements: List[T] = Nil

  def push(element: T) = elements = element :: elements

  def top: T = elements.head

  def pop: List[T] = elements.tail

  override def toString = elements.toString()
}


