package sample

/**
  * Created by Freshwood on 02.05.2016.
  */

object PracticeMergeMapWithList extends App {

  val valueMap: Map[String, Int] = Map("Speed" -> 1000, "Power" -> 10, "Times" -> 3)

  val metaMap: Map[String, String] = Map("pom1" -> "Speed", "pom2" -> "Power", "pom3" -> "Times")

  val testList = List(1, 2, 3, 4, 5, 6)

  val sum = (x:List[Any]) => x ++ x

  def collectExample(list: List[Int]): Unit = {
    val funcy = list.collect {
      case x:Int => println(s"Hallo Liste $x")
    }
  }


  def functionTest(list: List[Any], funky: => Boolean): List[Any] = {
       if (funky) sum(list) else List()
  }

  def getNumbersByPredicate(list: List[Any])( predicate: => Boolean): List[Any] = {

    if(predicate) list else List()
  }

  def getTestMap(mapValues: Map[String, Int], mapMeta: Map[String, String]): Map[String, String] = {

    // First step intersect the keys
    val keysFound = mapMeta.map(_.swap).keySet.intersect(mapValues.keySet)

    metaMap
  }

  // In collect you have always a default partial function | In this example for nil last element
  println(collectExample(testList))

  testList.foreach(i => println(s"Hallo Liste $i"))
}
