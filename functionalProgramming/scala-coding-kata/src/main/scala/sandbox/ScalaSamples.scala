package sandbox

import scala.language.higherKinds

object ScalaSamples extends App {

  val list: Seq[Int] = 1 to 100

  list.map(println)

  trait Monad[M[_]] {

    def pure[A](a: A): M[A]

    def flatMap[A, B](a: M[A])(fn: A => M[B]): M[B]

  }

  case class Car[T](input: T) extends Monad[Car] {
    override def pure[A](a: A): Car[A] = Car(a)

    override def flatMap[A, B](a: Car[A])(fn: A => Car[B]): Car[B] = fn(a.input)

    //def map[A, B](a: Car[A])(fn: A => B): Car[B] = Car(fn(a.input))
  }

  val cars = Seq(Car(100), Car(200), Car(300))

  val result = for {
    c <- cars
    out = c.input * 2
  } yield out

  println(result)
}

object PartialUnification extends App {

  object Test {
    def meh[M[_], A](x: M[A]): M[A] = x
    println {
      meh { (x: Int) =>
        x
      }
    } // should solve ?M = [X] X => X and ?A = Int ...
  }

  val woot = Test
}
