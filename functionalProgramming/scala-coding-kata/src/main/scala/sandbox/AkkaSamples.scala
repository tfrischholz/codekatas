package sandbox

import akka.actor.{Actor, ActorRef, Props}
import akka.http.scaladsl.Http
import akka.http.scaladsl.server.Route
import akka.{Done, NotUsed}
import akka.stream.scaladsl.{Flow, RunnableGraph, Sink, Source}
import config.ActorContext
import akka.http.scaladsl.server.Directives._
import scala.concurrent.Future

object AkkaSimpleSample extends App with ActorContext {

  class SampleActor extends Actor {
    override def receive: Receive = {
      case x => println(x)
    }
  }

  object SampleActor {
    def props: Props = Props[SampleActor]
  }

  lazy val actorRef: ActorRef = system.actorOf(SampleActor.props)

  (1 to 100) foreach (element => {
    println(s"sending $element to actor: ${actorRef.path}")
    actorRef ! element
  })
}

object AkkaStreamSample extends App with ActorContext {

  val source: Source[Int, NotUsed] =
    Source.fromIterator(() => (1 to 100).toIterator)

  val flow: Flow[Int, Int, NotUsed] = Flow[Int].map(input => input * 2)

  val sink: Sink[Int, Future[Done]] = Sink.foreach(element => println(element))

  val graph: RunnableGraph[NotUsed] = source via flow to sink

  graph.run()
}

object AkkaHttpSample extends App with ActorContext {

  lazy val route: Route = complete("Hello World")

  Http().bindAndHandle(route, "localhost", 8080)

}
