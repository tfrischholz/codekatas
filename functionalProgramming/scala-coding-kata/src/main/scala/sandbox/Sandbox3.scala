package sandbox

/**
	* Created by Freshwood on 30.07.2016.
	*/
object Sandbox3 extends App {

	val sillyString = "Hallo Welt"

	// Throws Out of Bounds exception. C# is the same!!!
	val result = sillyString.substring(0, 100)

	println(result)

}
