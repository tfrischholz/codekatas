package sandbox

/**
  * This sample should simply show the power of functional programming
  * We want to mimik the MVC design pattern with the power of functional programming
  * MVC == M => C <= V
  * Given a spefic model which will be enriched by a controller will be provided as view (result)
  */
object MVCFunctionalFlowExample extends App {

  // First given a specific Model
  val model: Model[Int] = Model(100)

  val controller: Controller[Int] = Controller(model)

  val view: View[String] = controller.process(value => value.toString + " Hello World")

  println(view)
}

case class Model[A](value: A)

case class Controller[A](input: Model[A]) {
  def process[B](f: A => B): View[B] = View(f(input.value))
}

case class View[A](input: A)