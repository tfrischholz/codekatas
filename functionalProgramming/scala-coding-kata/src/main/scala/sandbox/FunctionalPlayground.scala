package sandbox

import scala.language.{existentials, higherKinds, postfixOps, reflectiveCalls}

/**
  * Created by Freshwood on 04.05.2017.
  */
object FunctionalPlayground extends App {

  type Monoid[A] = A => A

  def ownCompose[A, B, C](a: B => C)(b: A => B): A => C = input => a(b(input))

  def ownAndThen[A](a: Monoid[A])(b: Monoid[A]): Monoid[A] = input => b(a(input))

  def reduce[A](f: Seq[Monoid[A]]): Monoid[A] = f.reduceLeft(ownAndThen(_)(_))

  def folded[A](f: Seq[Monoid[A]]): Monoid[A] = input => f.foldLeft(input) {
    case (x, y) => y(x)
  }

  def foldWithPredicate[A](list: Seq[Monoid[A]])(predicate: A => Boolean): Monoid[A] = input => {
    list.foldLeft(input) {
      case (x, y) if predicate(x) => y(x)
      case (x, _) => x
    }
  }

  def divide(int: Int): Int = int / 2

  def multiply(int: Int): Int = int * 2

  def sum(int: Int): Int = int + int

  val composed: Monoid[Int] = ownCompose(divide)(multiply)

  val chunked: Monoid[Int] = ownAndThen(divide)(multiply)

  val reduced: Monoid[Int] = reduce(Seq(divide, multiply, sum, sum, sum))

  val foldedList: Monoid[Int] = folded(Seq(divide, multiply, sum, sum, sum))

  // On curried methods we have to take the underscore, because scala can not find the apply method
  val foldedWithAPredicate: Monoid[Int] = foldWithPredicate(Seq(divide _, multiply _, sum _))(_ % 2 == 0)

  println(composed(10))

  println(chunked(10))

  println(reduced(10))

  println(foldedList(10))

  println(foldedWithAPredicate(10))
}

object AllTogetherTypeAliases extends App {

  // Function declaration
  val sum: Int => Int => Int = x => y => x + y
  val factor: Int => Int => Int = x => y => x * y

  println(sum(10)(10))
  println(factor(10)(10))

  def what(input: String): Unit = println(input)

  def lol(function: {def what(input: String): Unit}) = function.what("Hallo Welt")

  what("Hallo Welt")

  object Woot {

    def what(input: String): Unit = println("Duck Typing")
  }

  lol(Woot)
}

// Higher Ordering / First class functions

object HighOrderingTestApp extends App {

  type Function[S] = S => S

  type Consumer[S] = S => Unit

  val test: Function[Int] = x => x * x

  val test2: Consumer[Int] = println

  test2(100)

  trait Functor[S[_]] {
    def fmap[A, B](f: A => B): S[A] => S[B]
  }

  case class MayBeType[T](input: T) extends Functor[MayBeType] {
    override def fmap[A, B](f: (A) => B): MayBeType[A] => MayBeType[B] = value => MayBeType(f(value.input))
  }

  val maybe: MayBeType[Int] = MayBeType(100)

  val maybeString: MayBeType[Int] => MayBeType[String] = maybe.fmap(f => f.toString)

  val myy: MayBeType[Int] = MayBeType(100)

  println(maybeString(myy))
}

object OwnFlatteningApp extends App {

  type Result = Int

  type WorkWith = Int => Int

  trait Flat {
    def flatten(input: WorkWith): Result
  }

  case class WorkerClass(value: Int) extends Flat {
    override def flatten(input: WorkWith): Result = input(value)
  }

  val fn: WorkWith = input => input + 100

  val instance: WorkerClass = WorkerClass(100)

  println(instance.value)

  println(instance.flatten(fn))

  // Currying test
  val fnn: Int => Int => Int = { input1 =>
    val temp = input1 * input1
    input2 => temp * input2
  }

  println(fnn(10)(10))

  val workWithFn: Int => Int = fnn(8)

  println(workWithFn(7))
  println(workWithFn(3))
}

/**
  * Just working with generic functions and make everything with it
  */
object GenericFunctions extends App {

  type Gen[S] = S => S

  // Now, we try to run a sequence of functions
  def sum: Gen[Int] = input => input + input

  def reduce[A](list: Seq[Gen[A]]): Gen[A] = list reduceLeft { (x, y) =>
    out => y(x(out))
  }

  val fnList: Seq[Gen[Int]] = Seq(sum, sum, sum, sum)

  val reduced = reduce(fnList)

  println(reduced(5))

  val numbers: Seq[Int] = 1 to 100

  val result: Int = numbers reduceLeft {
    (x, y) => println(s"X: $x")
      println(s"Y: $y")
      x + y
  }

  println(result)
}

object SandboxComposition extends App {

  type Monoid[S] = S => S

  val first: Monoid[Int] = input => input * 100

  val second: Monoid[Int] = input => input / 10

  def ownAndThen[A](a: Monoid[A], b: Monoid[A]): Monoid[A] = fn => b(a(fn))

  val owned: Monoid[Int] = ownAndThen(first, second)

  val owned2: Monoid[Int] = ownAndThen(second, first)

  println(owned(3))
  println(owned2(10))
}


object FNComposeExtreme extends App {

  type SimpleType = Int => Int

  val first: SimpleType = input => input * 2

  val second: SimpleType = input => input * 3

  val third: SimpleType = input => input * 4

  val combined = first andThen second andThen third

  println(combined.compose(combined)(10))
}

object TypeLambdasTest extends App {

  type NormalType[A] = A => A

  type PojoType[A] = NormalType[({type T = Map[Int, A]})#T]

  private def test: PojoType[String] = fn => fn + (10 -> "Welt")

  val fn: PojoType[String] = test

  println(fn(Map(1 -> "Hallo")))

}