package sandbox

import scala.util.Random

/**
  * @author Freshwood
  * @since 12.01.2018
  */
object AuthenticationHelper extends App with PassMonoid {

  def crypt(user: User): User =
    user.copy(pass = buildPassword(user.salt.getOrElse(salt), user.pass))

  def salt: String = Random.nextString(10)

  def buildPassword(salt: String, password: String): String =
    encrypt(encrypt(salt) + encrypt(password))

  private def encrypt: String => String = _.reverse
}

private[sandbox] case class User(salt: Option[String], pass: String)

private[sandbox] trait Monoid[A] {
  def identity: A
  def append(x: A, y: A): A
}

private[sandbox] trait PassMonoid extends Monoid[String] {
  override def append(x: String, y: String): String = x + y

  override def identity: String = Random.nextString(10)

  private def encrypt: String => String = _.reverse
}
