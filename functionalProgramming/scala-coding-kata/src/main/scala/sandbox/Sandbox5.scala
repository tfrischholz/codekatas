package sandbox

import sandbox.PhantomTypeUsage.Builder.BuildItFirst

import scala.annotation.implicitNotFound
import scala.language.{higherKinds, reflectiveCalls}

/**
  * @author Freshwood
  * @since 09.09.2018
  */
object Sandbox5 extends App {

  def all: String = {
    "Hello World"
  }

  def fn(f: => String): (String, String) = f -> f

  println(fn(all))
}

/**
  * Here you can see how an unapply work
  */
object Processor extends App {

  case class User(name: String, age: Int)

  object UserProcessor {
    def unapply(user: User): Option[User] = if (user.age > 20) Some(user) else None
  }

  val wrongUser: User = User("Hans", 15)

  wrongUser match {
    case UserProcessor(u) => println(u)
    case _                => throw new IllegalArgumentException("Wrong user provided")
  }
}

/**
  * This sample show how to use a functor the right way
  */
object FunctorUsage extends App {

  trait Functor[F[_]] {
    def fmap[A, B](fn: F[A])(f: A => B): F[B]
  }

  case class Dog[S](input: S) extends Functor[Dog] {
    override def fmap[A, B](fn: Dog[A])(f: A => B): Dog[B] = Dog(f(fn.input))
    def map[A](f: S => A): Dog[A] = fmap(this)(f)
  }

  val test: Dog[Int] = Dog(122)

  val anotherDog: Dog[String] = test.map(_.toString)

  println(test)

  println(anotherDog)
}

/**
  * In this sample we are use a monad with functor
  */
object MonadUsage extends App {

  trait Functor[F[_]] {
    def fmap[A, B](fn: F[A])(f: A => B): F[B]
  }

  trait Monad[M[_]] extends Functor[M] {
    def pure[A](a: A): M[A]
    def mflatMap[A, B](fn: M[A])(f: A => M[B]): M[B]
  }

  case class Cat[S](value: S) extends Monad[Cat] {
    override def pure[A](a: A): Cat[A] = Cat(a)

    override def mflatMap[A, B](fn: Cat[A])(f: A => Cat[B]): Cat[B] = f(fn.value)

    override def fmap[A, B](fn: Cat[A])(f: A => B): Cat[B] = Cat(f(fn.value))

    def map[A](f: S => A): Cat[A] = fmap(this)(f)

    def flatMap[A](f: S => Cat[A]): Cat[A] = mflatMap(this)(f)
  }

  val testCat: Cat[Int] = Cat(300)

  val flatMapCat: Cat[String] = testCat.flatMap(value => Cat(value.toString))

  println(testCat)

  println(flatMapCat)
}

object PhantomTypeUsage extends App {

  /**
    * First of all we have to define some types
    */
  sealed trait BuilderState
  sealed trait BuilderInitialized extends BuilderState
  sealed trait BuilderNotInitialized extends BuilderState

  class Builder[S <: BuilderState] {
    val prop: Option[String] = None

    def setProp(value: String): Builder[BuilderInitialized] = new Builder[BuilderInitialized] {
      override val prop: Option[String] = Some(value)
    }

    def build(implicit evidence: BuildItFirst[S]): String = prop.get
  }

  object Builder {

    @implicitNotFound("You have to set a property first")
    type BuildItFirst[S] = S =:= BuilderInitialized

    def apply(): Builder[BuilderNotInitialized] = new Builder[BuilderNotInitialized]
  }

  val result = Builder().setProp("Hello").build

  println(result) // Work

  //val wrongResult = Builder().build // Would not work -> says: You have to set a property first (your custom message)
}

/**
  * Example shows how to use type lambdas
  */
object TypeLambdaUsage extends App {

  case class Vehicle[S](property: S)

  type functor[T] = Vehicle[T]

  type functorMap[A, B] = functor[({ type T = Map[A, B] })#T]

  val test: functorMap[Int, String] = Vehicle[Map[Int, String]](Map(100 -> "Hello World"))

  println(test)
}
