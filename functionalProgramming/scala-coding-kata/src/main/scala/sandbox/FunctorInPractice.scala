package sandbox

import scala.language.higherKinds

/**
  * Created by Freshwood on 31.05.2017.
  * Just a sample which should clearly show a functor sample
  */
object FunctorInPractice extends App {

  // Just a basic monoid example
  type Monoid[S] = S => S

  type Enriched[S] = Monoid[S] => S

  def enricher[A](input: A, fn: Monoid[A]): Enriched[A] = {
    val result = fn(input)
    enrich =>
      {
        Thread.sleep(1000)
        enrich(result)
      }
  }

  val multiply: Monoid[Int] = test => test * test

  val sum: Monoid[Int] = test => test + test

  val test = enricher(100, multiply)

  println(test)

  println(test(sum))

  // Just a composing sample

  val concat: Monoid[Int] = sum andThen multiply

  val composed: Monoid[Int] = sum compose multiply

  println(concat(70))

  println(composed(5) ensuring (input => input % 5 == 0))

  trait Functor[F[_]] {
    def fmap[A, B](f: A => B): F[A] => F[B]

    def apply[A, B](input: F[A])(fn: F[A => B]): F[B]

    def flatMap[A, B](input: F[A])(f: A => F[B]): F[B]
  }
}

/**
  * Created by TFrischholz on 22.06.2017.
  */
object Samples extends App {

  val mayBe: Container[String] = Container("Hallo")

  val mapped: Container[String] = mayBe map (_ + " Welt")

  val flatMapped: Container[String] = mapped flatMap (obj =>
    Container("Woot " + obj))

  println(mayBe)
  println(mapped)
  println(flatMapped)

  val result = for {
    a <- mayBe
    b <- mapped
    c <- flatMapped
  } yield a + b + c

  println(result)
}

object EvenSample extends App {

  val possibleEven = EvenEnriched(100)
  val test = EvenEnriched(25)
  val possibleNoneEven = EvenEnriched(51)

  println(possibleEven)
  println(possibleNoneEven)

  val result = for {
    a <- EvenEnriched(10)
    b <- EvenEnriched(16)
    c <- EvenEnriched(18)
  } yield a + b + c

  println(result)
}

trait GenericFunctor[F[_]] {
  def fmap[A, B](f: A => B): F[A] => F[B]
}

trait ApplicativeFunctor[F[_]] {
  def apply[A, B](input: F[A])(f: F[A => B]): F[B]
}

trait MonadFunctor[F[_]] {
  def bind[A, B](input: F[A])(f: A => F[B]): F[B]
}

trait Functor[F[_]]
    extends GenericFunctor[F]
    with ApplicativeFunctor[F]
    with MonadFunctor[F] {

  type S

  def identity[A](a: A): A = a

  def map[B](f: S => B): F[B]
}

case class Container[T](value: T) extends Functor[Container] {

  override type S = T

  override def apply[A, B](input: Container[A])(
      f: Container[(A) => B]): Container[B] = {
    val outcome = f.value.apply(input.value)
    new Container[B](outcome)
  }

  override def map[B](f: (T) => B): Container[B] = Container(f(value))

  override def bind[A, B](input: Container[A])(
      f: (A) => Container[B]): Container[B] =
    f(input.value)

  override def fmap[A, B](f: (A) => B): Container[A] => Container[B] =
    input => Container(f(input.value))

  def flatMap[B](f: T => Container[T]): Container[T] = f(value)
}

sealed trait EvenEnriched[+S] {
  def isEven: Boolean

  def get: S

  @inline final def flatMap[B](f: S => EvenEnriched[B]): EvenEnriched[B] =
    if (isEven) f(get) else NoneEven

  @inline final def map[B](f: S => B): EvenEnriched[B] =
    if (isEven) Even(f(get)) else NoneEven
}

case object NoneEven extends EvenEnriched[Nothing] {
  override def isEven: Boolean = false

  override def get: Nothing = throw new NoSuchElementException("NoneEven.get")
}

final case class Even[T](input: T) extends EvenEnriched[T] {
  override def isEven: Boolean = true

  override def get: T = input
}

object EvenEnriched {
  def apply[A](x: A): EvenEnriched[A] = x match {
    case i: Int if i % 2 == 0 => Even(x)
    case _                    => NoneEven
  }
}
