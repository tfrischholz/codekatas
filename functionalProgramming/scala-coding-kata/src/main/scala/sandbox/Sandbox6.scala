package sandbox

/**
  * @author Freshwood
  * @since 12.11.2018
  */
object Sandbox6 extends App {

  var jobName: String = _

  val test: Option[String] = Option(jobName)

  println(test)
}
