package sandbox

/**
  * Created by Freshwood on 29.11.2016.
  */
object Sandbox4 extends App {

  val list = (1 to 100).toList

  def high(list: List[Int]): Int => List[Int] = someInt => list filter(_ > someInt)

  val output = high(list)

  println(output(10))
}
