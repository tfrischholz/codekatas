package sandbox

/**
  * Here are some sandbox classes ;)
  */
object Sandbox extends App {

  implicit class SpaceString(str: String) {
    val addSpace = str + " "
  }

  val toUpper = (x: String) => x.toUpperCase

  val toLower = (x: String) => x.toLowerCase

  val decision = (x: String) => x match {
    case y if y.startsWith("H") => toUpper(x)
    case y if y.startsWith("W") => toLower(x)
    case _ => x
  }


  val addMultipliedString = (x: String, y: String) => (z: Int) => (x + y).addSpace * z

  println(addMultipliedString("Hallo ", "Welt")(19))

}
