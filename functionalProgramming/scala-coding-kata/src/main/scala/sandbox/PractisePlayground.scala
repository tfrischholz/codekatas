package sandbox

import scala.collection.immutable.Stack

/**
  * Created by Freshwood on 10.07.2017.
  * Just build a tree with function as values
  * 1. Tree with unlimited childs
  * 2. Function addNode and removeNode
  * 3. Should consume untyped values
  * 4. Introduce a play function to traverse the tree (just print out the values)
  */
object PractisePlayground extends App {

  case class Tree[S](data: S, childs: List[Tree[S]] = List.empty) {

    def addNode(node: Tree[S]): Tree[S] = Tree(data, node :: childs)

    def removeNode(): Tree[S] = Tree(data, childs.tail)

    def play(): Unit = {
      println(data)
      def traverse[B](nodes: List[Tree[S]]): List[Tree[S]] = nodes match {
        case Nil => List.empty
        case x :: Nil =>
          println(x.data)
          traverse(x.childs)
        case x :: y =>
          println(x.data)
          traverse(y)
          traverse(x.childs)
      }
      traverse(childs)
    }
  }

  val instance = Tree("Hallo")

  val addedNode =
    instance.addNode(Tree(" Welt", List(Tree("Fuck"), Tree("Woot"))))

  val anotherNode = addedNode.addNode(
    Tree("Fuck_you", List(Tree("Boris"), Tree("Thomas"), Tree("Sebastian"))))

  anotherNode.play()
}

/**
  * Just implement an immutable stack
  */
object StackExample extends App {

  trait Stack[T] {
    def isEmpty: Boolean
    def push(element: T): Stack[T]
    def pop: Stack[T]
    def print(): Unit
  }

  case class OwnStack[S](elements: List[S]) extends Stack[S] {
    override def isEmpty = elements.isEmpty

    override def push(element: S) = OwnStack(element :: elements)

    override def pop: Stack[S] = OwnStack(elements.tail)

    override def print() = elements foreach println
  }

  val test = OwnStack(List("Hallo", "Welt"))

  test.print()

  val result = test.pop

  result.print()

  val woot = Stack[String]("Hallo")
}

object TypeSafeBuilder extends App {

  sealed trait Build
  class CanBuilded extends Build
  class CanNotBuild extends Build

  case class Builder[S <: Build](data: String) {

    def withProp(d: String)(
        implicit evidence: S =:= CanNotBuild): Builder[CanBuilded] =
      Builder[CanBuilded](data + "---" + d)

    def build(implicit evidence: S =:= CanBuilded): Builder[CanNotBuild] =
      new Builder(data + " Welt")
  }

  object Builder {
    def apply(): Builder[CanNotBuild] = Builder[CanNotBuild]("")
  }

  val test = Builder().withProp("Fuck you").build

  println(test.data)
}
