package config

import akka.actor.ActorSystem
import akka.stream.{ActorMaterializer, Materializer}

import scala.concurrent.ExecutionContext

/**
  * @author Freshwood
  * @since 01.01.2018
  * Make sure we have a fancy actor context for learning purpose
  */
trait ActorContext {

  implicit lazy val system: ActorSystem = ActorSystem("scala-coding-kata")

  implicit lazy val materializer: Materializer = ActorMaterializer()

  implicit lazy val ec: ExecutionContext = system.dispatcher
}
