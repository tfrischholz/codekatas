package kata

object CodingKata05Reverse extends App{

  val listValues: List[Int] = List(1, 1, 2, 3, 5, 8, 8, 9)

  def reverse(listValues: List[Int]): List[Int] = {

    if (listValues.tail.nonEmpty)
    {
      reverse(listValues.tail) :+ listValues.head
    }
    else
    {
      List(listValues.head)
    }
  }

  def reverseMatch(listValues: List[Int]): List[Int] = listValues match {
    case Nil => listValues
    case x :: Nil => listValues
    case x :: y => reverse(y) :+ x

  }

  println(reverseMatch(listValues))
}
