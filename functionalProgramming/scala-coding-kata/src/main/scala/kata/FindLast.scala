package kata

import scala.annotation.tailrec

/**
  * Created by Freshwood on 29.05.2017.
  * scala> last(List(1, 1, 2, 3, 5, 8))
  * res0: Int = 8
  */
object FindLast extends App {

  val list = List(1, 1, 2, 3, 5, 8)

  @tailrec
  private def last(input: List[Int]): Int = input match {
    case x :: Nil => x
    case _ :: y => last(y)
    case Nil => 0
  }

  println(last(list))
}

/**
  * scala> nth(2, List(1, 1, 2, 3, 5, 8))
  * res0: Int = 2
  */
object SpecificElementInList extends App {

  val list = List(1, 1, 2, 3, 5, 8)

  private def nth(element: Int, input: List[Int]): Int = (element, input) match {
    case (number, x :: _) if number == 0 => x
    case (number , _ :: y) if number != 0 => nth(element - 1, y)
    case (_, Nil) => 0
  }

  println(nth(5, list))
}