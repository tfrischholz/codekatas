package kata

object CodingKata02FindSecondLast extends App{

  val listValues: List[Int] = List(1, 1, 2, 3, 5, 8)
  val listValueWithOneElement: List[Int] = List(2)
  val listValueWithTwoElements: List[Int] = List(10, 0)
  val emptyListValue: List[Int] = List()
  val nilListValue: List[Int] = Nil

  def secondLastMatchingList(listValues: List[Int]): Int = listValues match {

    case Nil => 0
    case x :: _ :: Nil => x
    case _ :: y => secondLastMatchingList(y)
  }

  println(secondLastMatchingList(listValues))
  println(secondLastMatchingList(listValueWithOneElement))
  println(secondLastMatchingList(listValueWithTwoElements))
  println(secondLastMatchingList(emptyListValue))
  println(secondLastMatchingList(nilListValue))
}
