package kata

object CodingKata04Length extends App{

  val listValues: List[Int] = List(1, 1, 2, 3, 5, 8, 8, 9)

  def length(listValues: List[Int]): Int = {
    if (listValues != null && listValues.nonEmpty) {
      length(listValues.tail) + 1
    } else {
      0
    }
  }
  def lengthMatch(listValues: List[Int]): Int = listValues match {
    case null => 0
    case Nil => 0
    case _ :: Nil => 1
    case x :: y => lengthMatch(y) + 1
  }

  println(length(listValues))
  println(lengthMatch(listValues))
}
