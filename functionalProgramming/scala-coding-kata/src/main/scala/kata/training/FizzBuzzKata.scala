package kata.training

/**
  * @author Freshwood
  * @since 08.07.2018
  * Numbers from 1 to 100 -> 3 is Fizz, 5 is Buzz, 15 is Fizz Buzz
  */
object FizzBuzzKata extends App {

  private val fizz: String = "Fizz"
  private val buzz: String = "Buzz"
  private val fizzBuzz: String = fizz + buzz

  private val numbers: Seq[Int] = 1 to 100

  def isFizz: PartialFunction[Int, String] = {
    case x if x % 3 == 0 => fizz
  }

  def isBuzz: PartialFunction[Int, String] = {
    case x if x % 5 == 0 => buzz
  }

  def isFizzBuzz: PartialFunction[Int, String] = {
    case x if x % 15 == 0 => fizzBuzz
  }

  private val pf: PartialFunction[Int, String] = isFizzBuzz orElse isFizz orElse isBuzz orElse {
    case x => x.toString
  }

  numbers map pf foreach println
}
