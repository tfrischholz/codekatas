package kata

object CodingKata07FlattenList extends App{

  val unflattedList: List[Any] = List(List(1, 1), 2, List(3, List(5, 8)))

  val flattedList: List[Any] = List(5, 5, 1, 2, 10, 9)

  val emptyList: List[Any] = List()

  val nilList: List[Any] = Nil

  def unflatList(unflattedList: List[Any]): List[Any] = unflattedList match {

    /**
      * Special case when we have null values :)
      */
    case null => List()

    /**
      * Return nothing when we have nothing ;)
      */
    case Nil => Nil

    /**
      * Check if we have any type of a list (I don't know how to make it better)
      * Then recursive unflat the previous typed list and append unflatted tail
      */
    case (head: List[Any]) :: tail => unflatList(head) ::: unflatList(tail)

    /**
      * Almost the same like above. This time without a list type
      * Handle the condition with many list items
      */
    case head :: tail => head :: unflatList(tail)
  }

    println(unflatList(unflattedList))
    println(unflatList(flattedList))
    println(unflatList(emptyList))
    println(unflatList(nilList))
    println(unflatList(null))
}
