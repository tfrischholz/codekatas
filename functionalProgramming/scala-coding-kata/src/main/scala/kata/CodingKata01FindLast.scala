package kata

object CodingKata01FindLast extends App {

  val listValues: List[Int] = List(1, 1, 2, 3, 5, 8)

  def last(listValues: List[Int]): Int = {

    if (listValues.tail.nonEmpty) {
      last(listValues.tail)
    } else {
      listValues.head
    }
  }

  def lastMatchingList(listValues: List[Int]): Int = listValues match {
    case Nil => 0
    case x :: Nil => x
    case _ :: y => lastMatchingList(y)

  }

  println(last(listValues))
  println(lastMatchingList(listValues))
}
