package kata

object CodingKata08DuplicatesInList extends App{

  val listWithDuplicateEntries: List[Any] = List('a, 'a, 'a, 'a, 'b, 'c, 'c, 'a, 'a, 'd, 'e, 'e, 'e, 'e)

  /**
    * Uncompress the give list
    * @param valueList uncompressed list
    * @return A compressed list
    */
  def compress(valueList: List[Any]): List[Any] = {
    if (valueList.nonEmpty && valueList.tail.nonEmpty) {

      if (valueList.head == compress(valueList.tail).head) {
        compress(valueList.tail)
      } else {
          valueList.head +: compress(valueList.tail)
      }
    } else {
         valueList
    }
  }

  /**
    * Pattern with logic after matching
    * @param valueList uncompressed list
    * @return A compressed list
    */
  def compressWithPattern1(valueList: List[Any]): List[Any] = valueList match {

    case Nil => Nil

    case x :: Nil => valueList

    case x :: y => if (x == y.head) compressWithPattern1(y) else x +: compressWithPattern1(y)
  }

  /**
    * Pattern with logic in the pattern
    * @param valueList valueList uncompressed list
    * @return A compressed list
    */
  def compressWithPattern2(valueList: List[Any]): List[Any] = valueList match {

    case Nil => Nil

    case x :: Nil => valueList

    case x :: y if x == y.head => compressWithPattern2(y)

    case x :: y => x +: compressWithPattern2(y)
  }

    println(compress(listWithDuplicateEntries))
    println(compressWithPattern1(listWithDuplicateEntries))
    println(compressWithPattern2(listWithDuplicateEntries))
}
