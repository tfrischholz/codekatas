package kata

object CodingKata06Palindrom extends App{

  val palindromList: List[Int] = List(1, 2, 3, 2, 1)

  val nonPalindromList: List[Int] = List(1, 1, 2, 3, 5, 8, 8, 9)

  def reverse(listValues: List[Int]): List[Int] = {

    if (listValues.tail.nonEmpty)
    {
      reverse(listValues.tail) :+ listValues.head
    }
    else
    {
      List(listValues.head)
    }
  }

  def reverseMatch(listValues: List[Int]): List[Int] = listValues match {
    case Nil => listValues
    case x :: Nil => listValues
    case x :: y => reverse(y) :+ x
  }

  def isPalindrom(listValues: List[Int]): Boolean = listValues match {

    case Nil => false
    case x :: Nil => false
    case x :: y => reverseMatch(listValues) == listValues
  }


  println(isPalindrom(palindromList))
  println(isPalindrom(nonPalindromList))
}
