package kata

object CodingKata03FindNth extends App{

  val listValues: List[Int] = List(1, 1, 2, 3, 5, 8)

  def nth(xth: Int, listValues: List[Int]): Int = {

    if(xth == 0) {
      listValues.head
    }
    else {
      nth(xth-1, listValues.tail)
    }
  }

  def nthMatch(xth: Int, listValues: List[Int]): Int = (xth, listValues) match {

    case(0, n :: _) => n
    case(_, _) => nth(xth -1, listValues.tail)
  }

  println(nth(3, listValues))
  println(nthMatch(2, listValues))
}
