package kata

/**
  * P09 (**) Pack consecutive duplicates of list elements into sublists.
  If a list contains repeated elements they should be placed in separate sublists.
  Example:

  scala> pack(List('a, 'a, 'a, 'a, 'b, 'c, 'c, 'a, 'a, 'd, 'e, 'e, 'e, 'e))
  res0: List[List[Symbol]] = List(List('a, 'a, 'a, 'a), List('b), List('c, 'c), List('a, 'a), List('d), List('e, 'e, 'e, 'e))
  */
object CodingKata09PackList extends App {

  val sample = List('a, 'a, 'a, 'a, 'b, 'c, 'c, 'a, 'a, 'd, 'e, 'e, 'e, 'e)

  // Only a partial implementation
  def pack(xs: List[Symbol]): List[List[Symbol]] = xs match {
    case Nil => List()
    case x :: y :: z if x == y => pack(z)
    case x :: y :: z if x != y => pack(z)
  }



  println(pack(sample))
}
