package kata;

import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * @author Freshwood
 * @since 08.07.2018
 */
public class FizzBuzzKata {

  public static String fizzBuzz(Integer input) {
    String retVal = input.toString();

    if (input % 3 == 0) {
      retVal = "Fizz";
    }

    if (input % 5 == 0) {
      retVal = "Buzz";
    }

    if (input % 15 == 0) {
      retVal = "FizzBuzz";
    }

    return retVal;
  }

  public static void main(String[] args) {
    Stream<String> stringStream = IntStream.rangeClosed(1, 100).mapToObj(FizzBuzzKata::fizzBuzz);

    stringStream.forEach(System.out::println);
  }
}
