package streamapi;


import java.util.Arrays;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;


/**
 * Created by TFrischholz on 17.05.2017.
 */
public class StreamApiTest {

	private final static Function<Integer, Integer> f = (x) -> x * x;

	private final static BiFunction<Integer, Integer, String> str = (x, y) -> String.valueOf(x * y);

	private final static Consumer<? super Object> println = System.out::println;

	private final static Predicate<Integer> nullCheck = (x) -> x == 0;

	private final static Predicate<Integer> evenCheck = (x) -> x % 2 == 0;

	private final static Supplier<String> supplier = () -> "Halloo Muahahaha";

	public static void main(String[] args) {
		Integer test = f.apply(10);
		String result = str.apply(10, 10);
		Boolean check = nullCheck.test(1);
		Boolean nullOrEven = nullCheck.and(evenCheck).test(0);
		String supplied = supplier.get();

		println.accept(test);
		println.accept(result);
		println.accept("Hallo Welt");
		println.accept(check);
		println.accept(nullOrEven);
		println.accept(supplied);

		functionTest(100, fn -> fn + "_Hallo");
		consumerTest(System.out::println);
		predicateTest(Arrays.asList("1", "2", "3"), newCheck -> newCheck.equals("2"));
		supplierTest(() -> "Test Output");
		simpleStreamExample();
	}

	private static void predicateTest(List<String> list, Predicate<String> checker) {
		list.stream().filter(checker).forEach(System.out::println);
	}

	// Loan pattern
	private static void consumerTest(Consumer function) {
		try {
			function.accept(100 / 0);
		} catch (Exception ex) {
			System.out.println(ex.getLocalizedMessage());
		}
	}

	private static void functionTest(Object value, Function fn) {
		System.out.println(fn.apply(value));
	}

	private static void supplierTest(Supplier arg) {
		System.out.println(arg.get());
	}

	private static void simpleStreamExample() {
		// Just make a Fizz Buzz example here
		Predicate<Integer> isFizz = x -> x % 3 == 0;
		Predicate<Integer> isBuzz = x -> x % 5 == 0;
		Predicate<Integer> isFizzBuzz = x -> x % 15 == 0;
		BiConsumer<Boolean, Integer> lastCheck = (x, y) -> {
			if (x) {
				System.out.println(y);
			}
		};

		List<Integer> list = IntStream.range(1, 101).boxed().collect(Collectors.toList());

		list.stream().filter(isFizz).collect(Collectors.toList()).forEach(System.out::println);

		//list.forEach(System.out::println);
	}

}
