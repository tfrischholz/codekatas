/**
 * The Java FizzBuzz Kata
 */
public class FizzBuzzTest {

	public static void main(String[] args) {
		// Start Fizz Buzz Kata :)
		for (Integer i = 1; i <= 100; i++) {

			if (isFizz(i) && isBuzz(i)) {
				System.out.println("FizzBuzz");
			}

			if (isBuzz(i)) {
				System.out.println("Buzz");
			}

			if (isFizz(i)) {
				System.out.println("Fizz");
			}

			System.out.println(i);
		}
	}

	private static Boolean isFizz(Integer number) {
		return number % 3 == 0;
	}

	private static Boolean isBuzz(Integer number) {
		return number % 5 == 0;
	}
}
