package practice.generics;

/**
 * Created by Freshwood on 03.05.2016.
 */
public class GenericClass<T> {

    private T t;

    public void setObject(T object) {
        this.t = object;
    }

    public T getObject() {
       return t;
    }

}
