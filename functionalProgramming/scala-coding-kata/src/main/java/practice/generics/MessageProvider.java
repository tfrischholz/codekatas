package practice.generics;

/**
 * Created by Freshwood on 03.05.2016.
 */
public class MessageProvider<T> implements IMessageProvider<T> {

    @Override
    public T getObject(T value) {
        return value;
    }
}
