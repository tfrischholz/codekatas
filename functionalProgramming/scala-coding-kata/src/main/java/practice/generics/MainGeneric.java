package practice.generics;

/**
 * Created by Freshwood on 03.05.2016.
 */
public class MainGeneric {

    public static void main(String[] args) {
        IMessageProvider<Auto> messageProvider = new MessageProvider<>();

        Auto car = messageProvider.getObject(new Auto());

        System.out.println(car.getName());

        GenericClass<Auto> genericClass = new GenericClass<>();

        genericClass.setObject(new Auto());

        System.out.println(genericClass.getObject().getName());
    }
}
