package practice.generics;

/**
 * {@link IMessageProvider}
 * @param <T> generic given type yo
 */
public interface IMessageProvider<T> {

    T getObject(T value);

}
