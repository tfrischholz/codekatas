package practice.inheritance;

/**
 * @author Freshwood
 * @since 09.07.2018
 */
public class InheritanceMain {

  public static void main(String[] args) {
    Car car = new Car();

    car.sayHello();
  }
}
