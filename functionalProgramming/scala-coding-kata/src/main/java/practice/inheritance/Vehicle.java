package practice.inheritance;

/**
 * @author Freshwood
 * @since 09.07.2018
 */
public class Vehicle {

  protected Vehicle() {
    System.out.println("I am a Vehicle ");
  }
}
