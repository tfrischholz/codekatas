package practice.inheritance;

/**
 * @author Freshwood
 * @since 09.07.2018
 */
public class Car extends Vehicle {

  public void sayHello() {
    System.out.println("I am a Car!");
  }
}
