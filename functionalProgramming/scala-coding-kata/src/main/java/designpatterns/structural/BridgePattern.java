package designpatterns.structural;

/**
 * @author Freshwood
 * @since 03.07.2018
 * This pattern involves an interface which acts as a bridge which makes
 * the functionality of concrete classes independent from interface implementer classes.
 * Both types of classes can be altered structurally without affecting each other.
 */
public class BridgePattern {

  public static void main(String[] args) {
    Shape red = new Circle(new RedCircle());
    Shape green = new Circle(new GreenCircle());
    red.draw();
    green.draw();
  }
}

interface DrawAPI {
  void drawCircle();
}

class RedCircle implements DrawAPI {

  @Override
  public void drawCircle() {
    System.out.println("Print Red circle");
  }
}

class GreenCircle implements DrawAPI {

  @Override
  public void drawCircle() {
    System.out.println("Print Green circle");
  }
}

abstract class Shape {

  protected DrawAPI drawAPI;

  protected Shape(DrawAPI drawAPI) {
    this.drawAPI = drawAPI;
  }

  public abstract void draw();
}

class Circle extends Shape {

  protected Circle(DrawAPI drawAPI) {
    super(drawAPI);
  }

  @Override
  public void draw() {
    this.drawAPI.drawCircle();
  }
}
