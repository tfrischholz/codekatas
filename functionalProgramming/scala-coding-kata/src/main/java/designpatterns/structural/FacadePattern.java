package designpatterns.structural;

/**
 * @author Freshwood
 * @since 03.07.2018
 */
public class FacadePattern {

  public static void main(String[] args) {
    VehicleFacade facade = new VehicleFacade();

    facade.bellCar();
    facade.bellShip();
    facade.bellAirplane();
  }
}

interface Vehicle {
  void bell();
}

class Car implements Vehicle {

  @Override
  public void bell() {
    System.out.println("Car");
  }
}

class Ship implements Vehicle {

  @Override
  public void bell() {
    System.out.println("Ship");
  }
}

class Airplane implements Vehicle {

  @Override
  public void bell() {
    System.out.println("Airplane");
  }
}

interface VehicleDefinition {
  void bellCar();

  void bellShip();

  void bellAirplane();
}

/**
 * Keep in mind that the concrete implementation is always private
 * Someone which is using this facade does not know which implementation is inside
 * We are communicating only with the strong interface definition
 */
class VehicleFacade implements VehicleDefinition {

  private Car car = new Car();

  private Ship ship = new Ship();

  private Airplane airplane = new Airplane();

  public void bellCar() {
    car.bell();
  }

  public void bellShip() {
    ship.bell();
  }

  public void bellAirplane() {
    airplane.bell();
  }
}

