package designpatterns.structural;

/**
 * @author Freshwood
 * @since 03.07.2018
 * This pattern creates a decorator class which wraps the original class and provides additional functionality
 * keeping class methods signature intact.
 */
public class DecoratorPattern {

  public static void main(String[] args) {
    AShape circle = new Elipsis();
    AShape redCircle = new RedShapeDecorator(new Elipsis());
    AShape redRectangle = new RedShapeDecorator(new Rectangle());

    System.out.println("Elipsis with normal border");
    circle.draw();

    System.out.println("\nCircle of red border");
    redCircle.draw();

    System.out.println("\nRectangle of red border");
    redRectangle.draw();
  }
}

interface AShape {
  void draw();
}

class Rectangle implements AShape {

  @Override
  public void draw() {
    System.out.println("Shape: Rectangle");
  }
}

class Elipsis implements AShape {

  @Override
  public void draw() {
    System.out.println("Shape: Elipsis");
  }
}

abstract class ShapeDecorator implements AShape {
  protected AShape decoratedShape;

  public ShapeDecorator(AShape decoratedShape){
    this.decoratedShape = decoratedShape;
  }

  public void draw(){
    decoratedShape.draw();
  }
}

class RedShapeDecorator extends ShapeDecorator {

  public RedShapeDecorator(AShape decoratedShape) {
    super(decoratedShape);
  }

  @Override
  public void draw() {
    super.draw();
    setRedBorder(decoratedShape);
  }

  private void setRedBorder(AShape decoratedShape){
    System.out.println("Border Color: Red " + decoratedShape.getClass().getSimpleName());
  }
}
