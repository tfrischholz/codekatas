package designpatterns.creational;

/**
 * @author Freshwood
 * @since 26.06.2018
 */
public class SingletonPattern {

  private static SingletonPattern ourInstance = new SingletonPattern();

  public static SingletonPattern getInstance() {
    return ourInstance;
  }

  private SingletonPattern() {
  }
}
