package designpatterns.creational;

/**
 * @author Freshwood
 * @since 26.06.2018
 */
public class AbstractFactoryPattern {

  public static void main(String[] args) throws IllegalAccessException {
    ColorFactory factory = new FactoryProducer().getColor();

    System.out.println(factory.getColor("Red").getName());
  }
}

interface Color {
  String getName();
}

class Red implements Color {

  @Override
  public String getName() {
    return "Red";
  }
}

class Green implements Color {

  @Override
  public String getName() {
    return "Green";
  }
}

class Blue implements Color {

  @Override
  public String getName() {
    return "Blue";
  }
}

class ColorFactory {

  /**
   * Returns a shape object on the specified given shape type
   */
  Color getColor(String colorType) throws IllegalAccessException {
    switch (colorType) {
      case "Red":
        return new Red();
      case "Green":
        return new Green();
      case "Blue":
        return new Blue();
      default:
        throw new IllegalAccessException("The specified type " + colorType + " was not found");
    }
  }
}

abstract class AbstractFactory {
  abstract ShapeFactory getShape();
  abstract ColorFactory getColor();
}

class FactoryProducer extends AbstractFactory {

  @Override
  ShapeFactory getShape() {
    return new ShapeFactory();
  }

  @Override
  ColorFactory getColor() {
    return new ColorFactory();
  }
}
