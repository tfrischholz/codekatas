package designpatterns.creational;

/**
 * A dead simple java factory implementation
 */
public class FactoryPattern {

	public static void main(String[] args) throws IllegalAccessException {
	  ShapeFactory factory = new ShapeFactory();

    Shape circle = factory.getShape("Circle");

    System.out.println(circle.getName());
	}
}

interface Shape {
  String getName();

}
class Circle implements Shape {

  @Override
  public String getName() {
    return "Circle";
  }
}

class Square implements Shape {

  @Override
  public String getName() {
    return "Square";
  }
}

class Rectangle implements Shape {

  @Override
  public String getName() {
    return "Rectangle";
  }
}

class ShapeFactory {

  /**
   * Returns a shape object on the specified given shape type
   */
  Shape getShape(String shapeType) throws IllegalAccessException {
    switch (shapeType) {
      case "Circle":
        return new Circle();
      case "Rectangle":
        return new Rectangle();
      case "Square":
        return new Square();
      default:
          throw new IllegalAccessException("The specified type " + shapeType + " was not found");
    }
  }
}
