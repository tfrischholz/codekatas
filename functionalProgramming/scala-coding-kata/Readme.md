# Repo von Liebschi, Tobi + R�mer

## Scala Design Patterns

* Functor (check)
* Applicative Functor (check)
* Monoid (check)
* Monad (check)
* Memoization (check)
* Cake Pattern (check)
* Magnet Pattern (check)
* Pimp my Library Design Pattern (e.g. Implicit class ) (check)
* Duck Typing (check)
* Lens Design Pattern (check => Is a case class extension -> massive manipulation)
// Not really a scala or functional pattern
* Type Lambdas -> not really a design pattern (check)