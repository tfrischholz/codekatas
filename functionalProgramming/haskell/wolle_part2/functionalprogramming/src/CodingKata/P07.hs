module CodingKata.P07 (NestedList(Val, List), flatten) where

data NestedList a = Val a | List [NestedList a]

flatten :: [NestedList a] -> [a]

flatten [(Val a)] = [a]
flatten [List a] = flatten a
flatten (Val a : y) = [a] ++ flatten y
flatten (List a : y) = flatten a ++ flatten y