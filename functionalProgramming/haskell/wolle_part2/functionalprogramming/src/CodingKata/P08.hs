module CodingKata.P08 (compress) where

compress :: Eq a => [a] -> [a]

compress [x] = [x]
compress [x, y]
    | x == y = [x]
    | otherwise = x : y : []
compress (x : y : xs)
    | x == y = compress (x : xs)
    | otherwise = x : compress (y : xs)