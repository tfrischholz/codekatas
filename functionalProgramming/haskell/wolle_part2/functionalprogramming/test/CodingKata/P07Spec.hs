module CodingKata.P07Spec (main, spec) where

import Test.Hspec
import CodingKata.P07

main :: IO ()
main = hspec spec

spec :: Spec
spec = do
  describe "Flatten a nested list structure." $ do
    it "with single element" $ do
      flatten [(Val 1)] `shouldBe` [1]

    it "with 2 levels list" $ do
      flatten [List[List[Val 2]]] `shouldBe` [2]

    it "with 5 levels list" $ do
      flatten [List[List[List[List[List[Val 2]]]]]] `shouldBe` [2]

    it "with test sample" $ do
      flatten [List[Val 1, Val 1], Val 2, List[Val 3, List[Val 5, Val 8]]] `shouldBe` [1, 1, 2, 3, 5, 8]