-- Task 1 --
last1 :: [a] -> a
last1 x
    | null x = error "fehler"
--last1 [] = error "fehler"
last1 (x:[]) = x
last1 (x:xs) = last1 xs

-- Task 2 --
nextToLast :: [a] -> a
nextToLast x
    | length x >= 2 = x!!((length x) - 2)
    | True = error "fehler"

-- Task 3 --
findNth :: Int -> [a] -> a
findNth k x
    | null  x = error "fehler"
    | k < (length x - 1) = error "index zu gross"
    | True = x!!k 

-- Task 4 --
length1 :: [a] -> Int
length1 x
   | null x = error "fehler"
length1 (x:[]) = 1
length1 (x:xs) = 1 + length1 xs

-- Task 5 --
reverse1 :: [a] -> [a]
reverse1 ([]) = []
reverse1 (x:[]) = [x]
reverse1 (x:xs) = reverse1 xs ++ [x]

-- Task 6 --
palindrom :: (Show a, Eq a) => [a] -> Bool
palindrom x -- 1,2,3,2,1
   | length x `mod` 2 == 0 = error "fehler"
palindrom x = null [idx | idx <- [0.. l-1] , hd!!idx /= tail!!idx] --show l ++ " " ++ show tail ++ " " ++ show hd
    where l = (length x `quot` 2) -- 2
          tail = reverse1 $ drop (l+1) x -- 2,1 -> 1,2
          hd = take (l) x -- 1,2

-- Task 7 --
-- correct data definition:
data AL = Any Int | AnyList [AL] deriving (Show)

-- Task 8 --
noConseqDupl :: Eq a => [a] -> [a]
noConseqDupl (x:xs)
    | null xs = [x]  
	| x /= head xs = x : noConseqDupl xs
    | x == head xs = noConseqDupl xs

-- Task 9 --
--conseq2List :: Eq a => [a] -> [a]
--conseq2List (x:xs)
--    | null xs = [x]  
--    | x /= head xs = x : conseq2List xs
--    | x == head xs = noConseqDupl xs
