# encoding: utf-8
#
# Flos Lösung zum Functional-Programming-Kata
# https://bitbucket.org/ckittelmann/codekatas/src/8a5fe0ec3d0ae11d8b31e78497949c67fb421a52/functionalProgramming/readme.txt
# Fragen gerne an florian.duetsch@nix-wie-weg.de

# Meine Lösung hat noch einige Ecken und Kanten, u. a.
# * ist nicht jede Funktion end-rekursiv formuliert.
# * Mit einer empty?- oder last?-Funktion ließen sich einige Bedingungen klarer
#   formulieren.
# * ...

# Ein Eintrag in der verketteten Liste
class Entry < Struct.new(:left, :right)
  def to_s
    return left.to_s unless right
    "#{left}, #{right}"
  end

  def to_a
    return [left] unless right
    [left] + right.to_a
  end
end

def List(*args)
  args = Array(args)
  return nil if args.empty?
  Entry.new(args.first, List(*args[1..-1]))
end

def head(list)
  list.left
end

def tail(list)
  list.right
end

def last(list)
  if tail(list)
    last(tail(list))
  else
    head(list)
  end
end

# http://learnyouahaskell.com/starting-out
def init(list)
  if list.right.right
    Entry.new list.left, init(list.right)
  else
    Entry.new list.left, nil
  end
end

def penultimate(list)
  if tail(tail(list))
    penultimate(tail(list))
  else
    list.left
  end
end

def nth(index, list)
  # Effektiv: Poppe index mal, dann head
  if index == 0
    head(list)
  else
    nth(index - 1, tail(list))
  end
end

def length(list)
  return 1 unless tail(list)
  1 + length(tail(list))
end

def reverse(list)
  return head(list) unless tail(list)
  Entry.new(reverse(tail(list)), head(list))
end

def palindrome(list)
  return false if head(list) != last(list)
  return true unless list.right
  palindrome(init(list).right)
end

def append l1, l2
  Entry.new l1.left, (l1.right ? append(tail(l1), l2) : l2)
end

def flatten(list)
  return nil unless list
  if head(list).is_a? Entry
    append(
      flatten(head(list)),
      flatten(tail(list))
    )
  elsif !tail(list)
    list
  else
    Entry.new head(list), flatten(tail(list))
  end
end

def compress(list)
  return list unless list.right

  e = compress(tail(list))
  if head(list) == head(e)
    e
  else
    Entry.new head(list), e
  end
end

puts last(List(1, 1, 2, 3, 5, 8))
puts init(List(1, 1, 2, 3, 5, 8))
puts penultimate(List(1, 1, 2, 3, 5, 8))
puts nth(2, List(1, 1, 2, 3, 5, 8))
puts length(List(1, 1, 2, 3, 5, 8))
puts reverse(List(1, 1, 2, 3, 5, 8))
puts palindrome(List(1, 2, 3, 2, 1))
puts palindrome(List(1, 2, 3, 3, 1))
puts append(List(1, 2, 3), List(4, 5, 6))
puts flatten(List(List(1, 1), 2, List(3, List(5, 8))))
puts compress(List('a', 'a', 'a', 'a', 'b', 'c', 'c', 'a', 'a',
                   'd', 'e', 'e', 'e', 'e'))
