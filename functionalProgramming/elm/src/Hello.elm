module Hello where

-- Erstes Element
first xs =
  case xs of
    [] -> Nothing
    x :: xs -> Just x

-- Alles außer erstem Element
tail xs =
  case xs of
    [] -> Nothing
    x :: xs -> Just xs

-- Letztes Element
last xs =
  case xs of
    [] -> Nothing
    x :: [] -> Just x
    x :: xs -> last xs

-- Alles außer dem letzten Element
init xs =
  case xs of
    [] -> []
    x :: [] -> []
    x :: y :: [] -> [x]
    x :: xs2 -> x :: init xs2

--------------------------------------------------------------------------------

penultimate xs =
  case xs of
    [] -> Nothing
    x :: _ :: [] -> Just x
    _ :: xs2 -> penultimate xs2

nth pos xs =
  if pos == 0 then
    first xs
  else
    case xs of
      x :: xs2 -> nth (pos - 1) xs2
      [] -> Nothing

length xs =
  case xs of
    [] -> 0
    x :: xs2 -> (length xs2) + 1

reverse xs =
  case xs of
    [] -> []
    x :: [] -> x :: []
    x :: xs2 -> (reverse xs2) ++ [x]

isPalindrome xs =
  case xs of
    [] -> True
    [_] -> True
    xs ->
      let
        safeTail xs = Maybe.withDefault [] (tail xs)
        inner xs = init (safeTail xs)
        innerIsPalindrome xs2 = isPalindrome (inner xs2)
      in
        first xs == last xs && innerIsPalindrome xs

isPalindrome' xs = xs == reverse xs
