module Tests where

import ElmTest exposing (..)
import String
import Hello exposing (..)


assertEqualSafe x y =
  assertEqual x (Maybe.withDefault 1000 y)

all : Test
all =
  suite "Hello"
    [
      test "first"
        <| assertEqualSafe 1
        <| first [1,1,2,3,5,8],
      test "last 1"
        <| assertEqualSafe 8
        <| last [1,1,2,3,5,8],
      test "last 2"
        <| assertEqualSafe 5
        <| last [1,1,2,3,5,8,5],
      test "penultimate"
        <| assertEqualSafe 5
        <| penultimate [1, 1, 2, 3, 5, 8],
      test "nth"
        <| assertEqualSafe 2
        <| nth 2 [1, 1, 2, 3, 5, 8],
      test "length"
        <| assertEqual 6
        <| length [1, 1, 2, 3, 5, 8],
      test "reverse"
        <| assertEqual [8,5,3,2,1,1]
        <| reverse [1,1,2,3,5,8],
      test "init"
        <| assertEqual [1,1,2,3,5]
        <| init [1,1,2,3,5,8],
      test "isPalindrome"
        <| assert (isPalindrome [1,2,3,2,1]),
      test "isPalindrome 2"
        <| assert (not (isPalindrome [1,2,3,4,1])),
      test "isPalindrome'"
        <| assert (isPalindrome' [1,2,3,2,1]),
      test "isPalindrome' 2"
        <| assert (not (isPalindrome' [1,2,3,4,1]))
      -- test "flatten"
      --   <| assertEqual [1, 1, 2, 3, 5, 8]
      --   <| flatten [[1,1],2,[3,[5,8]]]
    ]
