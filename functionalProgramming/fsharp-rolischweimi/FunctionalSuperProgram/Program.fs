﻿[<EntryPoint>]
let main argv = 
    
    // EXAMPLE for patterns let test = 1 :: (1 :: (2 :: (3 :: (5 :: (8 :: [])))))

    let list1 = [ 1; 1; 2; 3; 5; 8 ]
    printfn "List: %A" list1
    
    // P01 (*) Find the last element of a list.
    let rec last = function
    | head :: [] -> head
    | head :: tail -> last tail
    | _ -> failwith "Empty list."
    
    printfn "Last: %i" (last(list1))
    
    // P02 (*) Find the last but one element of a list.
    let rec penultimate = function
    | matching :: _ :: [] -> matching
    | head :: tail -> penultimate tail
    | _ -> failwith "Empty list."
    
    printfn "Last: %i" (penultimate(list1))

    // P03 (*) Find the Kth element of a list.
    let rec nth(index:int, list:List<int>) =
      if index = 0 then list.Head
      else nth((index - 1), list.Tail)

    printfn "nth: %i" (nth(2, list1))

    //P04 (*) Find the number of elements of a list.
    let rec length(counter:int, list:List<int>) =
      if list.IsEmpty then counter
      else length((counter + 1), list.Tail)

    printfn "length: %i" (length(0, list1))

    printfn "f# syntax is super duper nicht!"

    0 // Exitcode
